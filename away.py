import discord
from discord.ext import commands
from .utils import checks
from .utils.dataIO import fileIO
import logging
import asyncio
from datetime import datetime, timedelta
import pytz
import pyrfc3339 
from __main__ import send_cmd_help
import os

log = logging.getLogger("red.away")
if os.path.exists("data/istest"):
    log.setLevel(logging.DEBUG)
    log.debug('Logging: Debug')
else:
    log.setLevel(logging.ERROR)
    log.error('Logging: Error')
    
class Away:
    def __init__(self, bot):
        """Initializes Away."""
        self.bot = bot
        self.data = fileIO("data/away/data.json", "load")
        self.default_data = {
            'return_message' : '',
            'away_message' : '',
            'away_time' : None,
            'away_reason' : ''
        }
        self.away_message = 'away.'
        self.return_message = 'back.'
        asyncio.ensure_future( self.full_flag())
        
    @commands.group(name='awayset',no_pm=True,pass_context=True)
    async def awayset(self, ctx):
        """Modify Away settings"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx) 
            
    @awayset.command(name='return',no_pm=True, pass_context=True)
    async def set_return(self, ctx, *message : str):
        """Sets what you want for your return message
        
        [p]awayset return <message>
        
        Such that when returning, the following will show:
        
        YourName is now <message>"""
        message = ' '.join(ctx.message.clean_content.split()[2:])
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        if ctx.message.author.id not in self.data[ctx.message.channel.server.id]:
            self.data[ctx.message.channel.server.id][ctx.message.author.id] = {
            'return_message' : '',
            'away_message' : '',
            'away_time' : None,
            'away_reason' : ''
        }
        if not message:
            self.data[ctx.message.channel.server.id][ctx.message.author.id]['return_message'] = ''
            await self.bot.say('Reset message to default.')
        else:
            self.data[ctx.message.channel.server.id][ctx.message.author.id]['return_message'] = message
            await self.bot.say('Set message to: {author.display_name} is now {message}'.format(author = ctx.message.author, message = message))
        fileIO("data/away/data.json", "save", self.data)
        
    @awayset.command(name='away',no_pm=True, pass_context=True)
    async def set_away(self, ctx, *message : str):
        """Sets what you want for your away message
        
        [p]awayset away <message>
        
        Such that when setting away, the following will show:
        
        YourName is now <message>"""
        message = ' '.join(ctx.message.clean_content.split()[2:])
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        if ctx.message.author.id not in self.data[ctx.message.channel.server.id]:
            self.data[ctx.message.channel.server.id][ctx.message.author.id] = {
            'return_message' : '',
            'away_message' : '',
            'away_time' : None,
            'away_reason' : ''
        }
        if not message:
            self.data[ctx.message.channel.server.id][ctx.message.author.id]['away_message'] = ''
            await self.bot.say('Reset message to default.')
        else:
            self.data[ctx.message.channel.server.id][ctx.message.author.id]['away_message'] = message
            await self.bot.say('Set message to: {author.display_name} is now {message}'.format(author = ctx.message.author, message = message))
        fileIO("data/away/data.json", "save", self.data)
        
    @awayset.command(name='default_return',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def set_server_return(self, ctx, *message : str):
        """Sets what you want for the default return message
        
        [p]awayset default_return <message>
        
        Such that when returning, the following will show:
        
        YourName is now <message>"""
        message = ' '.join(ctx.message.clean_content.split()[2:])
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        if not message:
            del self.data[ctx.message.channel.server.id]['default_return']
            await self.bot.say('Reset message to default.')
        else:
            self.data[ctx.message.channel.server.id]['default_return'] = message
            await self.bot.say('Set message to: {author.display_name} is now {message}'.format(author = ctx.message.author, message = message))
        fileIO("data/away/data.json", "save", self.data)
        
    @awayset.command(name='default_away',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def set_server_away(self, ctx, *message : str):
        """Sets what you want for the default away message
        
        [p]awayset default_away <message>
        
        Such that when setting away, the following will show:
        
        YourName is now <message>"""
        message = ' '.join(ctx.message.clean_content.split()[2:])
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        if not message:
            del self.data[ctx.message.channel.server.id]['default_away']
            await self.bot.say('Reset message to default.')
        else:
            self.data[ctx.message.channel.server.id]['default_away'] = message
            await self.bot.say('Set message to: {author.display_name} is now {message}'.format(author = ctx.message.author, message = message))
        fileIO("data/away/data.json", "save", self.data)
        
    @awayset.command(name='role',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def set_role(self, ctx, *role_name : str):
        """Sets a role to add to people who are away
        
        [p]awayset role <Role_name>"""
        role_name = ' '.join(ctx.message.clean_content.split()[2:])
        if not role_name:
            if 'role' in self.data[ctx.message.channel.server.id]:
                del self.data[ctx.message.channel.server.id]['role']
            return await self.bot.say("Removed role!")
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        try:
           roles = ctx.message.channel.server.roles
        except AttributeError:
           return await self.bot.say("This server has no roles!")
        role = discord.utils.get(roles, name=role_name)
        if role is None:
            return await self.bot.say("Couldn't find role: `" + role_name + "`")
        await self.bot.say("Set role to: " + role.name)
        self.data[ctx.message.channel.server.id]['role'] = role.id
        fileIO("data/away/data.json", "save", self.data)
    
    @awayset.command(name='enable',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def enable(self, ctx):
        """Toggles the away feature on a server
        
        [p]awayset enable"""
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        if 'enabled' not in self.data[ctx.message.channel.server.id]:
            self.data[ctx.message.channel.server.id]['enabled'] = False
        self.data[ctx.message.channel.server.id]['enabled'] = not self.data[ctx.message.channel.server.id]['enabled']
        await self.bot.say("Working on this server: " + str(self.data[ctx.message.channel.server.id]['enabled']))
        fileIO("data/away/data.json", "save", self.data)
    
    #@awayset.command(name='test',no_pm=True, pass_context=True)
    #@checks.admin_or_permissions(manage_server=True)
    async def test(self, ctx):
        await self.listener(ctx.message)
    
    async def full_flag(self):
        for server in self.bot.servers:
            for member in server.members:
                await self.adjust_member(member)
            
    async def remove_setting(self,member):
        server = member.server
        if server.id not in self.data:
            return
        if member.id not in self.data[server.id]:
            return
        del self.data[server.id][member.id]
        fileIO("data/away/data.json", "save", self.data)
            
    async def adjust_member(self,member):
        server = member.server
        try:
            if server.id not in self.data:
                self.data[server.id] = {}
                fileIO("data/away/data.json", "save", self.data)
            if member.id not in self.data[server.id]:
                self.data[server.id][member.id] = {
                    'return_message' : '',
                    'away_message' : '',
                    'away_time' : None,
                    'away_reason' : ''
                }
                fileIO("data/away/data.json", "save", self.data)
            if not self.data[server.id][member.id]['away_time'] and member.status != discord.Status.online:
                log.debug("Member: " + member.name + " On server: " + server.name)
                self.data[server.id][member.id]['away_time'] = pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
                log.debug("Set time to: " + self.data[server.id][member.id]['away_time'])
                fileIO("data/away/data.json", "save", self.data)
                if 'role' in self.data[server.id]:
                    try:
                       roles = server.roles
                    except AttributeError:
                       log.error("This server has no roles... what even?")
                       return
                    role = discord.utils.get(roles, id=self.data[server.id]['role'])
                    if role is None:
                        del self.data[server.id]['role']
                        fileIO("data/away/data.json", "save", self.data)
                    else:
                        try:
                            await self.bot.add_roles(member, role)
                        except discord.Forbidden:
                            log.error("Bot doesn't have permission to modify that role!")
                return
            if not self.data[server.id][member.id]['away_reason'] and self.data[server.id][member.id]['away_time'] and member.status == discord.Status.online:
                log.debug("Member: " + member.name + " On server: " + server.name)
                log.debug("Time was set to: " + self.data[server.id][member.id]['away_time'])
                self.data[server.id][member.id]['away_time'] = ''
                self.data[server.id][member.id]['away_reason'] = ''
                fileIO("data/away/data.json", "save", self.data)
                if 'role' in self.data[server.id]:
                    log.debug('Found role: ' + self.data[server.id]['role'])
                    try:
                       roles = server.roles
                    except AttributeError:
                       log.error("This server has no roles... what even?")
                       return
                    role = discord.utils.get(roles, id=self.data[server.id]['role'])
                    log.debug('Found role: ' + str(role.id))
                    if role is None:
                        log.error("Role no longer exists on server, deleting from settings.")
                        del self.data[server.id]['role']
                        fileIO("data/away/data.json", "save", self.data)
                    else:
                        try:
                            await self.bot.remove_roles(member, role)
                        except discord.Forbidden:
                            log.error("Bot doesn't have permission to modify that role!")
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
    
    async def member_update(self, pre_member, post_member):
        try:
            if post_member.status == pre_member.status:
                return
            if post_member.bot:
                return
            member = post_member
            server = post_member.server
            if server.id not in self.data:
                self.data[server.id] = {}
                fileIO("data/away/data.json", "save", self.data)
            if member.id not in self.data[server.id]:
                self.data[server.id][member.id] = {
                    'return_message' : '',
                    'away_message' : '',
                    'away_time' : None,
                    'away_reason' : ''
                }
                fileIO("data/away/data.json", "save", self.data)
            log.debug(pre_member.status)
            log.debug(post_member.status)
            log.debug(self.data[post_member.server.id][post_member.id]['away_time'])
            if not self.data[post_member.server.id][post_member.id]['away_time'] and pre_member.status == discord.Status.online:
                log.debug("User to be marked away")
                log.debug("Member: " + member.name + " On server: " + server.name)
                self.data[server.id][member.id]['away_time'] = pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
                log.debug("Set time to: " + self.data[server.id][member.id]['away_time'])
                fileIO("data/away/data.json", "save", self.data)
                if 'role' in self.data[server.id]:
                    try:
                       roles = server.roles
                    except AttributeError:
                       log.error("This server has no roles... what even?")
                       return
                    role = discord.utils.get(roles, id=self.data[server.id]['role'])
                    if role is None:
                        del self.data[server.id]['role']
                        fileIO("data/away/data.json", "save", self.data)
                    else:
                        try:
                            await self.bot.add_roles(member, role)
                        except discord.Forbidden:
                            log.error("Bot doesn't have permission to modify that role!")
                return
            if self.data[post_member.server.id][post_member.id]['away_time'] and post_member.status == discord.Status.online and not self.data[post_member.server.id][post_member.id]['away_reason']:
                log.debug("Member: " + member.name + " On server: " + server.name)
                log.debug("Time was set to: " + self.data[server.id][member.id]['away_time'])
                self.data[server.id][member.id]['away_time'] = ''
                self.data[server.id][member.id]['away_reason'] = ''
                fileIO("data/away/data.json", "save", self.data)
                if 'role' in self.data[server.id]:
                    log.debug('Found role: ' + self.data[server.id]['role'])
                    try:
                       roles = server.roles
                    except AttributeError:
                       log.error("This server has no roles... what even?")
                       return
                    role = discord.utils.get(roles, id=self.data[server.id]['role'])
                    log.debug('Found role: ' + str(role.id))
                    if role is None:
                        log.error("Role no longer exists on server, deleting from settings.")
                        del self.data[server.id]['role']
                        fileIO("data/away/data.json", "save", self.data)
                    else:
                        try:
                            await self.bot.remove_roles(member, role)
                        except discord.Forbidden:
                            log.error("Bot doesn't have permission to modify that role!")
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
        
    async def listener(self, message):
        try:
            if message.author.bot:
                log.debug('First abort')
                return
            if not message.mentions:
                log.debug('Second abort')
                return
            if message.server is None:
                log.debug('Third abort')
                return
            if message.server.id not in self.data:
                log.debug('Fourth abort')
                return
            if not self.is_enabled(message.server):
                log.debug('Fifth abort')
                return
            log.debug('Trying to process')
            for member in message.mentions:
                log.debug('Member: ' + member.id)
                if member.id in self.data[message.server.id]:
                    if self.data[message.server.id][member.id]['away_time']:
                        time = how_long_ago(pyrfc3339.parse(self.data[message.server.id][member.id]['away_time']))
                        if self.data[message.server.id][message.author.id]['away_reason']:
                            away_message = '\nReason: ' + self.data[message.server.id][member.id]['away_reason']
                        else:
                            if member.status != discord.Status.online:
                                if member.status == discord.Status.dnd:
                                    away_message = "\nUser is marked: `Do Not Disturb`"
                                elif member.status == discord.Status.idle:
                                    away_message = "\nUser is: `Idle`"
                                elif member.status == discord.Status.offline:
                                    away_message = "\nUser is: `Offline`"
                                else:
                                    away_message = "\nUser is: `" + str(member.status) + "`"
                            else:
                                away_message = ''
                        msg = 'User `{member.display_name}` has been away for {time}.{away_message}'.format(member=member, away_message=away_message, time=time)
                        return await self.bot.send_message(message.channel, msg)
                for server in self.data.keys():
                    server = self.bot.get_server(server)
                    if server is None:
                        continue
                    if server.id not in self.data:
                        continue
                    if member.id not in self.data[server.id]:
                        continue
                    if self.data[server.id][member.id]['away_time']:
                        time = how_long_ago(pyrfc3339.parse(self.data[server.id][member.id]['away_time']))
                        msg = 'User `{member.display_name}` has been away on another server for {time}.'.format(member=member, time=time)
                        return await self.bot.send_message(message.channel, msg)
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
                    
    @commands.command(name='away', no_pm=True, pass_context=True)
    async def _away(self, ctx, *reason : str):
        if not self.is_enabled(ctx.message.channel.server):
            return await self.bot.send_message(ctx.message.author,"This feature is not enabled on {}".format(ctx.message.channel.server.name))
        if ctx.message.channel.server.id not in self.data:
            self.data[ctx.message.channel.server.id] = {}
        if ctx.message.author.id not in self.data[ctx.message.channel.server.id]:
            self.data[ctx.message.channel.server.id][ctx.message.author.id] = {
                'return_message' : '',
                'away_message' : '',
                'away_time' : None,
                'away_reason' : ''
            }
        if not self.data[ctx.message.channel.server.id][ctx.message.author.id]['away_time'] or reason:
            for server in self.bot.servers:
                if server.id == ctx.message.channel.server.id:
                    native_server = True
                else:
                    native_server = False
                member = server.get_member(ctx.message.author.id)
                if member is None:
                    continue
                if server.id not in self.data:
                    self.data[server.id] = {}
                    fileIO("data/away/data.json", "save", self.data)
                if member.id not in self.data[server.id]:
                    self.data[server.id][member.id] = {
                        'return_message' : '',
                        'away_message' : '',
                        'away_time' : None,
                        'away_reason' : ''
                    }
                    fileIO("data/away/data.json", "save", self.data)
                if native_server:
                    message = self.data[server.id][member.id]['away_message']
                    if not message:
                        if 'default_away' in self.data[server.id]:
                            message = self.data[server.id]['default_away']
                        else:
                            message = self.away_message
                    await self.bot.say('{author.display_name} is now {message}'.format(author = member, message = message))
                self.data[server.id][member.id]['away_time'] = pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
                if reason:
                    self.data[server.id][member.id]['away_reason'] = ' '.join(ctx.message.clean_content.split()[1:])
                fileIO("data/away/data.json", "save", self.data)
                if 'role' in self.data[server.id]:
                    try:
                       roles = server.roles
                    except AttributeError:
                       log.error("This server has no roles... what even?")
                       return
                    role = discord.utils.get(roles, id=self.data[server.id]['role'])
                    if role is None:
                        del self.data[server.id]['role']
                        fileIO("data/away/data.json", "save", self.data)
                    else:
                        try:
                            await self.bot.add_roles(member, role)
                        except discord.Forbidden:
                            log.error("Bot doesn't have permission to modify that role!")
        else:
            for server in self.bot.servers:
                if server.id == ctx.message.channel.server.id:
                    native_server = True
                else:
                    native_server = False
                member = server.get_member(ctx.message.author.id)
                if member is None:
                    continue
                if server.id not in self.data:
                    self.data[server.id] = {}
                    fileIO("data/away/data.json", "save", self.data)
                if member.id not in self.data[server.id]:
                    self.data[server.id][member.id] = {
                        'return_message' : '',
                        'away_message' : '',
                        'away_time' : None,
                        'away_reason' : ''
                    }
                    fileIO("data/away/data.json", "save", self.data)
                if native_server:
                    message = self.data[server.id][member.id]['return_message']
                    if not message:
                        if 'default_return' in self.data[server.id]:
                            message = self.data[server.id]['default_return']
                        else:
                            message = self.return_message
                    time = how_long_ago(pyrfc3339.parse(self.data[server.id][member.id]['away_time']))
                    await self.bot.say('{author.display_name} is now {message} (away for {time})'.format(author = member, message = message, time = time))
                self.data[server.id][member.id]['away_time'] = ''
                self.data[server.id][member.id]['away_reason'] = ''
                fileIO("data/away/data.json", "save", self.data)
                if 'role' in self.data[server.id]:
                    log.debug('Found role: ' + self.data[server.id]['role'])
                    try:
                       roles = server.roles
                    except AttributeError:
                       log.error("This server has no roles... what even?")
                       return
                    role = discord.utils.get(roles, id=self.data[server.id]['role'])
                    log.debug('Found role: ' + str(role.id))
                    if role is None:
                        log.error("Role no longer exists on server, deleting from settings.")
                        del self.data[server.id]['role']
                        fileIO("data/away/data.json", "save", self.data)
                    else:
                        try:
                            await self.bot.remove_roles(member, role)
                        except discord.Forbidden:
                            log.error("Bot doesn't have permission to modify that role!")
                            
    def is_enabled(self, server):
        try:
            if 'enabled' in self.data[server.id] and self.data[server.id]['enabled']:
                return True
        except:
            pass
        return False
    
def how_long_ago(old_datetime : datetime = None):
    if old_datetime is None:
        raise ValueError
    duration = datetime.utcnow().replace(tzinfo=pytz.utc) - old_datetime
    if int(duration.days / 365):
        if int(duration.days / 365) == 1:
            return str(int(duration.days / 365)) + ' year'
        else:
            return str(int(duration.days / 365)) + ' years'
    if duration.days:
        if int(duration.days) == 1:
            return str(int(duration.days)) + ' day'
        else:
            return str(int(duration.days)) + ' days'
    if int(duration.seconds / 3600):
        if int(duration.seconds / 3600) == 1:
            return str(int(duration.seconds / 3600)) + ' hour'
        else:
            return str(int(duration.seconds / 3600)) + ' hours'
    if int(duration.seconds / 60):
        if int(duration.seconds / 60) == 1:
            return str(int(duration.seconds / 60)) + ' minute'
        else:
            return str(int(duration.seconds / 60)) + ' minutes'
    return 'under a minute'
    
#following should only need to do anything once. They ensure the right files/folders are created        
def check_folder():
    if not os.path.exists("data/away"):
        print("Creating data/away folder...")
        os.makedirs("data/away")
        
def check_files():
    if not fileIO("data/away/data.json", "check"):
        print("Creating default data.json...")
        fileIO("data/away/data.json", "save", {}) 
    
def setup(bot):
    check_folder()
    check_files()
    n = Away(bot)
    bot.add_listener(n.listener, 'on_message')
    bot.add_listener(n.member_update, 'on_member_update')
    bot.add_listener(n.remove_setting, 'on_member_remove')
    bot.add_listener(n.adjust_member, 'on_member_join')
    bot.add_cog(n)