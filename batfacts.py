import discord
import feedparser
from discord.ext import commands
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
from urllib import parse
import os
import asyncio
import logging
from datetime import datetime, timedelta
import pyrfc3339 

try:   # check if feedparser exists
    import feedparser 
except ImportError:
    feedparser = None

log = logging.getLogger("red.batfacts")
log.setLevel(logging.ERROR)
    
class Batfacts:
    def __init__(self, bot):
        self.bot = bot
        self.data = fileIO("data/batfacts/data.json", "load")
        self.sub_task = bot.loop.create_task(self.batfacts_service())
        self.backup = {}

    @commands.group(name='batfacts',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _batfacts(self, ctx):
        """Manages Bat Facts"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)
            
    @_batfacts.command(name='refresh', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes)
        
        [p]batfacts refresh <time>"""
        server = ctx.message.server
        if refresh_time < 5:
            await self.bot.say("You can only enter a time over 15 minutes.")
            return
        self.data['refresh'] = refresh_time
        await self.bot.say( "Refresh time set to %i minutes." % (refresh_time))
        fileIO("data/batfacts/data.json", "save", self.data)

    @_batfacts.command(name='channel', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _set_channel(self, ctx, channel : discord.Channel):
        """Set the channel to put batfacts in
        
        [p]batfacts channel #<channel>"""
        server = ctx.message.server
        if 'channels' not in self.data:
            self.data['channels'] = {}
        self.data['channels'][server.id] = channel.id
        fileIO("data/batfacts/data.json", "save", self.data)
        await self.bot.say("Set Bat Facts channel to #%s." % ( channel.name ))

    @_batfacts.command(name='test', pass_context=True)
    async def _test(self, ctx):
        await self.spam_looper()        
        
    async def batfacts_service(self):
        """Trigger loop"""
#        return
        await self.bot.wait_until_ready()
        try:
            log.debug( "Entering try block, starting loop." )
            while True:
                log.info( "Bat Facts: Checking with youtube..." )
                try:
                    temp_backup = await self.spam_looper()
                    if temp_backup is not None:
                        self.backup = temp_backup
                except asyncio.CancelledError:
                    raise
                except:
                    self.data = self.backup
                    fileIO("data/batfacts/data.json", "save", self.data)
                    for file_name in os.listdir("data/batfacts/"):
                        if file_name == "data.json":
                            continue
                        if file_name == "last_feed.json":
                            continue
                        try:
                            log.error( "Deleting: {}".format(file_name) )
                            os.remove("data/batfacts/" + file_name)
                        except exception.OSError:
                            log.error( "Cannot delete!" )
                    log.error( "Loop error." )
                    pass
                log.info( "Bat Facts: Task done, sleeping." )
                if type(self.data['refresh']) is int:
                    if self.data['refresh'] < 5:
                        self.data['refresh'] = 5
                        fileIO("data/batfacts/data.json", "save", self.data)
                else:
                    self.data['refresh'] = 5
                    fileIO("data/batfacts/data.json", "save", self.data)
                await asyncio.sleep(self.data['refresh'] * 60)
                print("Batfacts service tick.")
        except asyncio.CancelledError:
            pass

    def __unload(self):
        self.sub_task.cancel()
        pass
        
    async def spam_looper(self):
        if not self.data:
            log.debug( "Program data empty? Bail out!" )
            return None
        if 'channels' not in self.data:
            log.debug( "Channels not configured. Bail out!" )
            return None
        for server, channel in self.data['channels'].items():
            server = find_server(self, server)
            if server is None:
                continue
            channel = find_channel(self, server, channel)
            log.debug( "Looking for a channel to spam, found: {}".format(channel.name) )
            if channel is None:
                continue
            feed = feedparser.parse( 'https://www.youtube.com/feeds/videos.xml?channel_id=UCwyl6DjeJTF1rECrfsx9Rjw' )
            log.debug( "Got the following from feedparser: {}".format(feed) )
            if not feed:
                return None
            if 'items' not in feed:
                return None
            if 'latest' not in self.data:
                self.data['latest'] = pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
                fileIO("data/batfacts/data.json", "save", self.data)
                continue
            latest_time = self.data['latest']
            new_latest_time = latest_time
            fileIO("data/batfacts/last_feed.json", "save", feed)
            for i in range(len(feed['items'])):
                if not feed['items'][i]['published'] > latest_time:
                    continue
                if feed['items'][i]['published'] > new_latest_time:
                    new_latest_time = feed['items'][i]['published']
                if 'RIP' in feed['items'][i]['summary']:
                    asyncio.ensure_future(self.bot.send_message( channel, "Bat Facts: WARNING — Potential Sad Bat Fact Detected! <{}>".format(feed['items'][i]['link'])))
                else:
                    asyncio.ensure_future(self.bot.send_message( channel, "Bat Facts: {}".format(feed['items'][i]['link'])))
            self.data['latest'] = new_latest_time
            fileIO("data/batfacts/data.json", "save", self.data)
        return self.data
            
def find_channel(self, server, channel_id):
    return discord.utils.get(self.bot.get_all_channels(), server__name=server.name, id=channel_id)
    
def find_server(self, server):
    return self.bot.get_server(server)
            
def check_folder():
    if not os.path.exists("data/batfacts"):
        print("Creating data/batfacts folder...")
        os.makedirs("data/batfacts")

def check_files():
    data_default = {"refresh":15, 'facts' : []}
    if not fileIO("data/batfacts/data.json", "check"):
        feed = feedparser.parse( 'https://www.youtube.com/feeds/videos.xml?channel_id=UCwyl6DjeJTF1rECrfsx9Rjw' )
        for i in range(len(feed['items'])):
            data_default['facts'].append(feed['items'][i]['link'])
        print("Creating default batfacts data.json...")
        fileIO("data/batfacts/data.json", "save", data_default)
    else:
        data = fileIO("data/batfacts/data.json", "load")

def setup(bot):
    check_folder()
    check_files()
    if feedparser is None:
        raise RuntimeError("You need to run `pip install feedparser`")
    bot.add_cog(Batfacts(bot))