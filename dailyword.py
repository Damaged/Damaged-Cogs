import discord
from discord.ext import commands
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
import os
import sys
import re
import asyncio
import requests
import logging
import html2text
from bs4 import BeautifulSoup
from pyembed.core import PyEmbed
from functools import partial
from urllib.parse import unquote

log = logging.getLogger("red.dailyword")
if os.path.exists("data/istest"):
    log.setLevel(logging.DEBUG)
    log.debug('Debug Logging On')
else:
    log.setLevel(logging.ERROR)
    log.error('Error Logging On')
    
class DailyWord:
    def __init__(self, bot):
        self.bot = bot
        self.data = fileIO("data/dailyword/data.json", "load")
        self.sub_task = bot.loop.create_task(self.dailyword_service())

    @commands.group(name='dailyword',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _feedsub(self, ctx):
        """Manages dailyword"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)
            
    @_feedsub.command(name='refresh', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes)
        
        [p]dailyword refresh <time>"""
        server = ctx.message.server
        if refresh_time < 1:
            await self.bot.say("You can only enter a time 1 hour and over.")
            return
        self.data['refresh'] = refresh_time
        await self.bot.say( "Refresh time set to %i hours." % (refresh_time))
        fileIO("data/dailyword/data.json", "save", self.data)

    @_feedsub.command(name='add', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _add(self, ctx, channel : discord.Channel = None):
        """Add a channel to spam
        
        [p]dailyword add #<channel>"""
        server = ctx.message.server
        if channel is None:
            await send_cmd_help(ctx)
            return
        if server.id not in self.data['subs']:
            self.data['subs'][server.id] = {}
        if channel.id in self.data['subs'][server.id].keys():
            return await self.bot.say('That channel is already subscribed!')
        self.data['subs'][server.id][channel.id] = True
        fileIO("data/dailyword/data.json", "save", self.data)
        await self.bot.say("Added {channel}.".format(channel = channel.mention))
        
    @_feedsub.command(name='del', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _del(self, ctx, channel : discord.Channel = None):
        """Del a channel
        
        [p]dailyword del #<channel>"""
        server = ctx.message.server
        if channel is None:
            await send_cmd_help(ctx)
            return
        if server.id not in self.data['subs']:
            return await self.bot.say('No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.say('Not subbed on this channel!')
        del self.data['subs'][server.id][channel.id]
        fileIO("data/dailyword/data.json", "save", self.data)
        await self.bot.say("Done.")
        
    @_feedsub.command(name='test', pass_context=True)
    @checks.admin_or_permissions(manage_server=True) 
    async def _test(self, ctx):
        log.setLevel(logging.DEBUG)
        self.data = fileIO("data/dailyword/data.json", "load")
        self.data['last_word'] = ""
        fileIO("data/dailyword/data.json", "save", self.data)
        await self.spam_looper()  
        if os.path.exists("data/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.ERROR)
            log.error('Error Logging On')      
        
    async def dailyword_service(self):
        """Trigger loop"""
        if os.path.exists("data/istest"):
            return #comment out when not testing
        await self.bot.wait_until_ready()
        try:
            log.debug( "Entering try block, starting loop." )
            while True:
                try:
                    await self.spam_looper()
                except:
                    pass
                log.info( "dailyword: Task done, sleeping." )
                await asyncio.sleep(int(self.data['refresh']) * 60 * 60)
                log.info("dailyword service tick.")
        except asyncio.CancelledError:
            pass
        
    def __unload(self):
        self.sub_task.cancel()
        pass
        
    async def spam_looper(self):
        if not self.data:
            log.debug( "Program data empty? Bail out!" )
            return None
        loop = asyncio.get_event_loop()
        response = loop.run_in_executor(None, partial(requests.request,"GET",'https://www.merriam-webster.com/word-of-the-day'))
        website = await response
        if website.status_code != requests.codes.ok:
            log.error('Page not found: ' + str(website.status_code))
        soup = BeautifulSoup(website.text, 'html.parser')
        #log.debug(soup.prettify())
        site_data = soup.find('head')
        if site_data is None:
            return
        log.debug(site_data.prettify())
        datum = {
            'word' : '',
            'link' : '',
            'image' : '',
            'meaning' : [],
            'date' : ''
        }
        try:
            datum['word'] = site_data.find('meta', property='og:title').get('content').rsplit(':',1)[1].strip()
            datum['date'] = '-'.join(site_data.find('meta', property='og:url').get('content').rsplit('-',3)[-3:][::-1])
            datum['link'] = site_data.find('meta', property='og:url').get('content')
            datum['image'] = site_data.find('meta',property='og:image').get('content')
            datum['meaning'] = soup.find('div', class_='word-attributes').prettify()
            datum['meaning'] += soup.find('div', class_='wod-definition-container').p.prettify()
            for siblings in soup.find('div', class_='wod-definition-container').p.next_siblings:
                if str(siblings).startswith('<p><strong>'):
                    datum['meaning'] += siblings.prettify()
            #datum['meaning'] += soup.find('div', class_='did-you-know-wrapper').find('div', class_='left-content-box').prettify()
            #site_data.find('meta',property='og:description').get('content')
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True
            datum['meaning'] = text_maker.handle(datum['meaning'])
            log.debug(datum)
            log.info("I found a word: " + datum['word'])
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            raise 
        if 'last_word' not in self.data:
            self.data['last_word'] = ''
            fileIO("data/dailyword/data.json", "save", self.data)
        if datum['word'] == self.data['last_word']:
            log.debug("But I already spammed it :(")
            return
        self.data['last_word'] = datum['word']
        fileIO("data/dailyword/data.json", "save", self.data)
        embed = discord.Embed()
        #embed.set_image(url=datum['image'])
        embed.url = datum['link']
        embed.title = 'Word of the day: ' + datum['word']
        embed.description = datum['meaning']
        embed.set_footer(text=datum['date'])
        for server in self.data['subs'].keys():
            server = self.bot.get_server(server)
            if server is None:
                continue
            log.debug('Found server: ' + server.name)
            for channel, data in self.data['subs'][server.id].items():
                channel = self.bot.get_channel(channel)
                if channel is None:
                    continue
                log.debug( "Looking for a channel to spam, found: {}".format(channel.name) )
                asyncio.ensure_future(self.bot.send_message( channel, content = 'Word of the Day: ' + datum['word'] + '\n<' + datum['link'] + '>', embed=embed))
                        
def check_folder():
    if not os.path.exists("data/dailyword"):
        print("Creating data/dailyword folder...")
        os.makedirs("data/dailyword")

def check_files():
    data_default = {"refresh":1, 'subs' : {}}
    if not fileIO("data/dailyword/data.json", "check") or not fileIO("data/dailyword/data.json", "load"):
        print("Creating default dailyword data.json...")
        fileIO("data/dailyword/data.json", "save", data_default)
    else:
        data = fileIO("data/dailyword/data.json", "load")

def setup(bot):
    check_folder()
    check_files()
    bot.add_cog(DailyWord(bot))