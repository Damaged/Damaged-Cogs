import discord
from redbot.core import Config, checks, commands, data_manager
import aiohttp
import os
import asyncio
import re
import json
from functools import partial
from datetime import datetime, timedelta
import pyrfc3339 
import logging
import hashlib
import sys
from .imagesafe import Imagesafe, ImageEmbed, ImagesafeSettings
from bs4 import BeautifulSoup
import copy
log = logging.getLogger("red.derpisub")
log.setLevel(logging.ERROR)

class DiscordLog:
    def __init__(self, urgency : str = 'Error', source : str = 'Unknown'):
        self.log_message = '{urgency} - {source}:\n'.format(urgency=urgency, source=source)
        self.urgency = urgency
        self.source = source
        
    def log(self, message : str = None):
        if message is None:
            raise ValueError
        self.log_message += "{message}\n".format(message=message)
    
    def get(self):
        return self.log_message

class Derpisub(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.bot_path = str(data_manager.cog_data_path(cog_instance=self))
        if os.path.exists(self.bot_path + "/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.ERROR)
            log.error('Error Logging On')
        self.config = Config.get_conf(self, identifier=0x6465727069737562)
        default_global = {
            "refresh" : 1,
            "last_id" : '',
            "log_channel" : None,
            "tags" : {},
            "featured" : { 'users' : [], 'channels' : []},
            "filter" : '100073',
            "latest_featured" : ''
        }
        default_user = {
            'blacklist' : []
        }
        default_channel = {
            'blacklist' : []
        }
        default_guild = {
            'blacklist' : []
        }
        self.config.register_global(**default_global)
        self.config.register_user(**default_user)
        self.config.register_channel(**default_channel)
        self.config.register_guild(**default_guild)
        self.sub_task = bot.loop.create_task(self.derpisub_service())  
        self.rate = {}
        self.imagesafe = Imagesafe(None)
        self.regex_tagurl = re.compile(r'https?://(?:www\.)?(?:derpibooru|trixiebooru)\.org/tags/(.+)', flags=re.IGNORECASE)  
        self.regex_filterurl = re.compile(r'https?://(?:www\.)?(?:derpibooru|trixiebooru)\.org/filters/(\d+)', flags=re.IGNORECASE)   
        self.idle = False

    async def on_message(self,message):
        if message.author.bot:
            return
        tag_pattern = self.regex_tagurl.search(message.clean_content)
        filter_pattern = self.regex_filterurl.search(message.clean_content)
        if tag_pattern is None and filter_pattern is None:
            return
        if tag_pattern is not None and filter_pattern is not None:
            return
        await self.add_emoji(message,filter_pattern is not None)

    async def on_raw_reaction_add(self, msg):
        try:
            if not msg:
                return
            channel = self.bot.get_channel(msg.channel_id)
            message = await channel.get_message(msg.message_id)
            reaction = None
            for datum in message.reactions:
                if isinstance(datum.emoji, str):
                    if msg.emoji.is_custom_emoji():
                        continue
                    if datum.emoji == msg.emoji.name:
                        reaction = datum
                        break
                else:
                    if msg.emoji.is_unicode_emoji():
                        continue
                    if datum.emoji.id == msg.emoji.id:
                        reaction = datum
                        break
            if reaction is None:
                return
            if msg.guild_id is not None:
                guild = self.bot.get_guild(msg.guild_id)
                member = guild.get_member(msg.user_id)
            else:
                member = await self.bot.get_user_info(msg.user_id)
            await self.reaction(reaction,member)
            return
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)

    async def reaction(self, reaction, member):
        try:
            if member.bot:
                log.debug("First abort")
                return
            if reaction.custom_emoji:
                log.debug("Second abort")
                return
    #        if not reaction.message.author.bot:
    #            log.debug("Third abort")
    #            return
            message = reaction.message
            channel = message.channel
            log.debug(reaction.emoji)
            looper = True
            sent_message = None
            while looper:
                looper = False
                tag_url = self.regex_tagurl.search(message.clean_content)
                tag = None
                if tag_url is not None:
                    tag_data = await self.imagesafe.get_data(tag_url.group(0) + '.json')
                    if tag_data is None or 'tag' not in tag_data or 'name' not in tag_data['tag']:
                        return
                    tag = tag_data['tag']['name']
                filter_url = self.regex_filterurl.search(message.clean_content)
                filter_id = None
                if filter_url is not None:
                    filter_id = filter_url.group(1)
                if not filter_id and not tag:
                    break
                if reaction.emoji == "ℹ": #Info emoji
                    if not member.permissions_in(message.channel).manage_messages:
                        break
                    await message.add_reaction("➕")
                    await message.add_reaction("➖")
                    if isinstance(channel, discord.TextChannel):
                        await message.add_reaction("🇧")
                        await message.add_reaction("🅱")
                    break
                if reaction.emoji == "🌟": #star
                    if filter_id:
                        if not member.permissions_in(message.channel).manage_messages:
                            break
                        log.debug(filter_id)
                        await self.config.filter.set(filter_id)
                        await channel.send('Set filter to: ' + filter_url.group(0))
                    elif tag and tag == 'featured image':
                        async with self.config.featured() as featured:
                            if member.id not in featured['users']:
                                featured['users'].append(member.id)
                                await member.send('Added subscription to featured images!')
                                break
                            else:
                                await member.send('You\'re already subscribed to featured images!')
                                break
                    elif tag:
                        async with self.config.tags() as tags:
                            async with self.config.user(member).blacklist() as blacklist:
                                if tag in blacklist:
                                    blacklist.remove(tag)
                                    sent_message = await member.send('Removed from blacklist:`' + tag + '` ' + tag_url.group(0))
                                    break
                            if tag not in tags:
                                tags[tag] = {'users' : [], 'channels' : []}
                            if member.id not in tags[tag]['users']:
                                tags[tag]['users'].append(member.id)
                                sent_message = await member.send('Added subscription to tag: `' + tag + '` ' + tag_url.group(0))
                                break
                            sent_message = await member.send('You are already subscribed to tag: `' + tag + '` ' + tag_url.group(0))
                    else:
                        log.debug("Actionable URL found")
                    break
                if reaction.emoji == "✴": #black star
                    if tag and tag == 'featured image':
                        async with self.config.featured() as featured:
                            if member.id in featured['users']:
                                featured['users'].remove(member.id)
                                await member.send('Removed subscription from featured images!')
                                break
                            else:
                                await member.send('You aren\'t subscribed to featured images!')
                                break
                    elif tag:
                        async with self.config.tags() as tags:
                            if tag not in tags:
                                tags[tag] = {'users' : [], 'channels' : []}
                            if member.id in tags[tag]['users']:
                                tags[tag]['users'].remove(member.id)
                                if not tags[tag]['channels'] and not tags[tag]['users']:
                                    del tags[tag]
                                sent_message = await member.send('Removed subscription to tag: `' + tag + '` ' + tag_url.group(0))
                                break
                            else:
                                async with self.config.user(member).blacklist() as blacklist:
                                    if tag not in blacklist:
                                        blacklist.append(tag)
                                        sent_message = await member.send('Added to blacklist:`' + tag + '` ' + tag_url.group(0))
                                        break
                            sent_message = await member.send('You aren\'t subscribed to tag and it\'s in your blacklist: `' + tag + '` ' + tag_url.group(0))
                    else:
                        log.debug("Actionable URL found")
                    break
                if reaction.emoji == "➕": #Plus
                    if not member.permissions_in(message.channel).manage_messages:
                        break
                    if tag and tag == 'featured image':
                        async with self.config.featured() as featured:
                            if channel.id not in featured['channels']:
                                featured['channels'].append(channel.id)
                                await channel.send('Added subscription to featured images!')
                                break
                            else:
                                await channel.send('Channel already subscribed to featured images!')
                                break
                    elif tag:
                        async with self.config.channel(channel).blacklist() as blacklist:
                            if tag in blacklist:
                                blacklist.remove(tag)
                                await channel.send('Removed tag `{tag}` from blacklist in {ment}'.format(ment=channel.mention,tag=tag))
                                break
                        async with self.config.tags() as tags:
                            if tag not in tags:
                                tags[tag] = {'users' : [], 'channels' : []}
                            if channel.id not in tags[tag]['channels']:
                                tags[tag]['channels'].append(channel.id)
                                await channel.send('Added subscription to tag `{tag}` in {ment}'.format(ment=channel.mention,tag=tag))
                                break
                            await channel.send('Already subscribed!')
                    break
                if reaction.emoji == "➖": #Minus
                    if not member.permissions_in(message.channel).manage_messages:
                        break
                    if tag and tag == 'featured image':
                        async with self.config.featured() as featured:
                            if channel.id in featured['channels']:
                                featured['channels'].remove(channel.id)
                                await channel.send('Removed subscription from featured images!')
                                break
                            else:
                                await channel.send('Channel isn\'t subscribed to featured images!')
                                break
                    if tag:
                        async with self.config.tags() as tags:
                            if tag not in tags:
                                tags[tag] = {'users' : [], 'channels' : []}
                            if channel.id in tags[tag]['channels']:
                                tags[tag]['channels'].remove(channel.id)
                                await channel.send('Removed subscription to tag `{tag}` in {ment}'.format(ment=channel.mention,tag=tag))
                                if not tags[tag]['channels'] and not tags[tag]['users']:
                                    del tags[tag]
                                break
                            else:
                                async with self.config.channel(channel).blacklist() as blacklist:
                                    if tag not in blacklist:
                                        blacklist.append(tag)
                                        await channel.send('Added tag `{tag}` to blacklist in {ment}'.format(ment=channel.mention,tag=tag))
                                        break
                            await channel.send('Not subscribed!')
                    break
                if isinstance(channel, discord.TextChannel):
                    if reaction.emoji == "🇧": #Blue B
                        if not member.permissions_in(message.channel).manage_messages:
                            break
                        if tag:
                            async with self.config.guild(channel.guild).blacklist() as blacklist:
                                if tag in blacklist:
                                    blacklist.remove(tag)
                                    await channel.send('Removed tag `{tag}` from server blacklist.'.format(tag=tag))
                                else:
                                    await channel.send('Not in server blacklist!')
                        break
                    if reaction.emoji == "🅱": #Red B
                        if not member.permissions_in(message.channel).manage_messages:
                            log.debug('not admin')
                            break
                        if tag:
                            log.debug(tag)
                            async with self.config.guild(channel.guild).blacklist() as blacklist:
                                log.debug(blacklist)
                                if tag not in blacklist:
                                    blacklist.append(tag)
                                    await channel.send('Added tag `{tag}` to server blacklist.'.format(tag=tag))
                                else:
                                    await channel.send('Already in server blacklist!')
                        break
                break
            if sent_message is not None:
                await self.add_emoji(sent_message)
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
                
    @commands.group(name='derpisub')
    @checks.admin_or_permissions(manage_guild=True)
    async def derpisub(self, ctx):
        """Manages subscription to a story on derpibooru"""

    @derpisub.command(name='refresh')
    @checks.is_owner()
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes): [p]derpisub refresh <time in minutes>"""
        if refresh_time < 0:
            await ctx.send("Invalid number!")
            return
        await self.config.refresh.set(refresh_time)
        if refresh_time:
            await ctx.send("Refresh time set to %i minutes." % (refresh_time))
        else:
            await ctx.send("Derpisub disabled!")

    @derpisub.command(name='check')
    @checks.admin_or_permissions(manage_guild=True)
    async def _check(self,ctx):
        if self.sub_task.done():
            await self.log_to_channel("Can't find subroutine")
            await asyncio.sleep(60)
            if self.sub_task.done():
                await self.log_to_channel("Still can't find subroutine")
                try:
                    output = self.sub_task.result()
                except asyncio.CancelledError:
                    return
                except asyncio.InvalidStateError:
                    return
                except:
                    await self.log_to_channel("Subroutine Crashed - Restarting")
                    e = sys.exc_info()[0]
                    log.error( "Error: %s" % e )
                    log.error( sys.exc_info())
                    log.exception(e)
                await self.log_to_channel("Subroutine not running - Restarting")
                self.sub_task = self.bot.loop.create_task(self.derpisub_service())  
            else:
                await self.log_to_channel("Derpisub was started by something else.")
        else:
            await self.log_to_channel("Derpisub running okay.")

    @derpisub.command(name='log')
    @checks.is_owner()
    @commands.guild_only()
    async def _log_channel(self, ctx, spam_channel : discord.TextChannel):        
        """Set the log channel for the cog
        
        [p]derpisub log <channel name>"""
        if not spam_channel:
            return await ctx.send( "Channel not found." )
        await self.config.log_channel.set(spam_channel.id)
        await ctx.send( "Channel to spam logs set to {}.".format(spam_channel.mention))
 
    async def derpisub_service(self):
        """Trigger loop"""
        if os.path.exists(self.bot_path + "/istest"):
            await self.log_to_channel("Derpisub running on debug! Exiting service.")
            return
        await self.log_to_channel("Starting derpisub service.")
        await self.bot.wait_until_ready()
        minutes = 0
        try:
            while True:
                try:
                    refresh = await self.config.refresh()
                    if refresh:
                        log.info("Derpisub service tick")
                        await self.spam_looper()
                    else:
                        refresh = 1
                    await asyncio.sleep(refresh * 60)
                    minutes += refresh
                    if minutes > 60:
                        minutes = 0
                    if minutes == 0:
                        await self.spam_featured()
                except asyncio.CancelledError:
                    raise
                except:
                    e = sys.exc_info()[0]
                    log.error( "Error: %s" % e )
                    log.error( sys.exc_info())
                    log.exception(e)
                    await asyncio.sleep(120)
        except asyncio.CancelledError:
            await self.log_to_channel("Exiting derpisub service.")

    @derpisub.command(name='test', pass_context=True)
    async def _test(self, ctx, story : str=''):      
        log.setLevel(logging.DEBUG)
        await self.spam_looper()
        if os.path.exists("data/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging: On')
        else:
            log.setLevel(logging.INFO)
            log.error('Info Logging: On')
                
    async def spam_looper(self):
        try:
            log.info('Starting spam loop')
            channel_log = DiscordLog('INFO', 'Derpisub')
            channel_log.log('```Starting spam loop```')
            filter = await self.config.filter()
            if filter is None:
                filter = '56027'
            async with self.config.tags() as tags:
                log.debug(tags)
                for tag, subs in list(tags.items()):
                    log.debug(tag)
                    log.debug(subs)
                    if subs['users']:
                        continue
                    if subs['channels']:
                        continue
                    del tags[tag]
                if not tags:
                    if not self.idle:
                        log.info('Going idle...')
                        self.idle = True
                    return
                else:
                    if self.idle:
                        log.info('Going active!')
                        self.idle = False
                        await self.config.last_id.set(None)
            #sanity check on latest_update_time
            last_id = str(await self.config.last_id())
            if not last_id: 
                url = 'https://derpibooru.org/search.json?q=*&filter_id={filter}&sf=id&sd=desc'.format(filter=filter)
                image_data = await self.imagesafe.get_data(url = url)
                log.debug(image_data)
                if image_data is None or 'search' not in image_data or not image_data['search']:
                    log.error('Derpi seems broke!')
                    return
                last_id = image_data['search'][0]['id']
                await self.config.last_id.set(last_id)
                log.info('Last update not found, set to: ' + str(last_id))
                return None
            log.debug('Last update found: ' + last_id)
            channel_log.log('Last update found: `' + last_id + '`')
            channel_log.log('Current time: `' + pyrfc3339.generate(datetime.utcnow(),accept_naive=True) + '`')
            #loop builds the array of stories if there is over 100 pending
            users_spammed = []
            channels_spammed = []
            images_found = []
            total_images = 0
            targets_spammed = []
            while True:
                images = []
                highest_id = str(last_id)
                url = 'https://derpibooru.org/search.json?q=id.gt:{oldest},created_at.lt:5 minute ago&filter_id={filter}&sf=id&sd=asc'.format(oldest=last_id,filter=filter)
                log.debug(url)
                image_data = await self.imagesafe.get_data(url = url)
                if image_data is None:
                    break
                if 'search' in image_data:
                    if len(image_data['search']) == 0:
                        log.debug('No more images')
                        break
                    images = image_data['search']                
                total_images += len(images)
                log.debug(len(images))
                #is there anything to process?
                tags = await self.config.tags()
                log.debug(tags)
                if images:
                    for image in images: #loops for days (and images)
                        if 'id' not in image:
                            continue
                        log.debug(image['id'])
                        pre_spam = 'New Image: '
                        #image_tags = list(map(str.strip(),image['tags'].split(',')))
                        image_tags = [x.strip(' ') for x in image['tags'].split(',')]
                        log.debug(image_tags)
                        if tags:
                            hits = list(set(image_tags) & set(tags.keys()))
                            log.debug(hits)
                            if hits: #is anything subscribed to this story?
                                settings = ImagesafeSettings()
                                try:
                                    await self.imagesafe.add_derpibooru(image, settings = settings)
                                    settings.primary = False
                                    i = 0
                                    processed_links = []
                                    if not settings.image_data and settings.primary_image:
                                        log.debug(settings.primary_image)
                                        settings.image_data = await self.imagesafe.get_image_data(settings.primary_image)
                                    while settings.embed.links:
                                        links = settings.embed.links.pop(0)
                                        await asyncio.sleep(1)
                                        log.debug('searching for extra data from: ' + links)
                                        if links in processed_links:
                                            continue
                                        processed_links.append(links)
                                        if links.startswith('file:'):
                                            links = links[5:]
                                            await self.imagesafe.extract_filename(file_name = links, settings = settings)
                                        elif links.startswith('hash:'):
                                            links = links[5:]
                                            await self.imagesafe.extract_from_hash(links, settings = settings)
                                        else:
                                            await self.imagesafe.extract_embed(links, settings = settings)
                                except asyncio.CancelledError:
                                    raise
                                except:
                                    e = sys.exc_info()[0]
                                    log.error( "Error: %s" % e )
                                    
                                    log.error( sys.exc_info())
                                    log.exception(e)
                                    continue
                                output_embed = settings.embed.embed
                                user_embed = copy.deepcopy(output_embed)
                                user_embed.add_field(name="Source provided by", value="[SourceBot](https://www.patreon.com/SourceBot)")
                                sent_message = []
                                for tag in hits:
                                    if 'channels' in tags[tag]:#a channel is subscribed
                                        for channel in tags[tag]['channels']:
                                            try:
                                                channel = self.bot.get_channel(channel)
                                                if channel is None:
                                                    continue
                                                if not channel.is_nsfw() and settings.embed.is_explicit:
                                                    continue
                                                async with self.config.channel(channel).blacklist() as blacklist:
                                                    if list(set(blacklist) & set(image_tags)):
                                                        continue
                                                async with self.config.guild(channel.guild).blacklist() as blacklist:
                                                    if list(set(blacklist) & set(image_tags)):
                                                        continue
                                                if '{channel} - {image_id}'.format(channel = channel.id, image_id = image['id']) in targets_spammed:
                                                    continue
                                                sent_message.append( await channel.send(content=pre_spam + '<{}>'.format(output_embed.url), embed=output_embed) )
                                                targets_spammed.append('{channel} - {image_id}'.format(channel = channel.id, image_id = image['id']))
                                                channels_spammed.append('{channel.id} - {channel.name}'.format(channel=channel))
                                                images_found.append('{image[id]}'.format(image=image))
                                            except asyncio.CancelledError:
                                                raise
                                            except:
                                                e = sys.exc_info()[0]
                                                log.error( "Error: %s" % e )
                                                
                                                log.error( sys.exc_info())
                                                log.exception(e)
                                    if 'users' in tags[tag]:#a user is subscribed
                                        for user in tags[tag]['users']:
                                            try:
                                                user = self.bot.get_user(user)
                                                if user is None:
                                                    continue
                                                async with self.config.user(user).blacklist() as blacklist:
                                                    if list(set(blacklist) & set(image_tags)):
                                                        continue
                                                if '{user} - {image_id}'.format(user = user.id, image_id = image['id']) in targets_spammed:
                                                    continue
                                                sent_message.append( await user.send(content=pre_spam + '<{}>'.format(user_embed.url), embed=user_embed) )
                                                targets_spammed.append('{user} - {image_id}'.format(user = user.id, image_id = image['id']))
                                                users_spammed.append('{user.id} - {user.name}'.format(user=user))
                                                images_found.append('{image[id]}'.format(image=image))
                                            except asyncio.CancelledError:
                                                raise
                                            except:
                                                e = sys.exc_info()[0]
                                                log.error( "Error: %s" % e )
                                                
                                                log.error( sys.exc_info())
                                                log.exception(e)
                        if int(last_id) < int(image['id']):
                            last_id = str(image['id'])
                            await self.config.last_id.set(last_id)
            channel_log.log(str(total_images) + ' images to process.')
            if users_spammed:
                channel_log.log('Users spammed: \n' + '\n'.join(list(set(users_spammed))))
            if channels_spammed:
                channel_log.log('Channels spammed: \n' + '\n'.join(list(set(channels_spammed))))
            if images_found:
                channel_log.log('Images spammed: \n' + '\n'.join(list(set(images_found))))
            channel_log.log('```End spam loop```')
            await self.log_to_channel(channel_log.get())

        except asyncio.CancelledError:
            raise
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
            
    async def get_featured(self):
        try:
            website = await self.imagesafe.get_website(url = 'https://derpibooru.org')
            if not website:
                return None
            soup = BeautifulSoup(website.website)
            featured_image_soup = soup.find('div', class_='media-box__content--featured').find('div',class_='image-container')
            image_id = str(featured_image_soup.get('data-image-id'))
        except asyncio.CancelledError:
            raise
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
            return None
        return image_id

    #@commands.command(name='featured')
    async def spam_featured(self, ctx = None):
        image_id = await self.get_featured()
        if not image_id:
            log.error('Couldn\'t find featured image id')
            return
        latest = await self.config.latest_featured()
        image_id = str(image_id)
        if latest:
            if latest == image_id:
                log.debug('latest image')
                return
        await self.config.latest_featured.set(image_id)
        settings = ImagesafeSettings()
        image = await self.imagesafe.get_derpi_data(image_id, settings = settings)
        log.debug(image)
        try:
            await self.imagesafe.add_derpibooru(image, settings = settings)
            settings.primary = False
            i = 0
            processed_links = []
            if not settings.image_data and settings.primary_image:
                log.debug(settings.primary_image)
                settings.image_data = await self.imagesafe.get_image_data(settings.primary_image)
            while settings.embed.links:
                links = settings.embed.links.pop(0)
                await asyncio.sleep(1)
                log.debug('searching for extra data from: ' + links)
                if links in processed_links:
                    continue
                processed_links.append(links)
                if links.startswith('file:'):
                    links = links[5:]
                    await self.imagesafe.extract_filename(file_name = links, settings = settings)
                elif links.startswith('hash:'):
                    links = links[5:]
                    await self.imagesafe.extract_from_hash(links, settings = settings)
                else:
                    await self.imagesafe.extract_embed(links, settings = settings)
            output_embed = settings.embed.embed
        except asyncio.CancelledError:
            raise
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
            return
        image_tags = [x.strip(' ') for x in image['tags'].split(',')]
        featured = await self.config.featured()
        pre_spam = 'Featured Image: '
        for channel in featured['channels']:
            channel = self.bot.get_channel(channel)
            if channel is None:
                continue
            if not channel.is_nsfw() and settings.embed.is_explicit:
                continue
            async with self.config.channel(channel).blacklist() as blacklist:
                if list(set(blacklist) & set(image_tags)):
                    continue
            async with self.config.guild(channel.guild).blacklist() as blacklist:
                if list(set(blacklist) & set(image_tags)):
                    continue
            await channel.send(content=pre_spam + '<{}>'.format(output_embed.url), embed=output_embed)
        output_embed.add_field(name="Source provided by", value="[SourceBot](https://www.patreon.com/SourceBot)")
        for user in featured['users']:
            user = self.bot.get_user(user)
            if user is None:
                continue
            async with self.config.user(user).blacklist() as blacklist:
                if list(set(blacklist) & set(image_tags)):
                    continue
            await user.send(content=pre_spam + '<{}>'.format(output_embed.url), embed=output_embed)

    async def add_emoji(self,message, is_filter : bool = False):
        await message.add_reaction("🌟")
        if is_filter:
            return
        await message.add_reaction("✴")
        if message.channel.guild:
            await message.add_reaction("ℹ")

    async def log_to_channel(self, message : str = None):
        try:
            current_time = datetime.utcnow()
            if message is None:
                return
            channel = await self.config.log_channel()
            channel = self.bot.get_channel(channel)
            if channel is None:
                return
            await channel.send(message)
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            raise
    
    def __unload(self):
        self.sub_task.cancel()
      
def date_to_z(date : str = None):
    if date is None:
        raise ValueError
    return pyrfc3339.generate(pyrfc3339.parse(date),utc=True)
