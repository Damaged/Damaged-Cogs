import discord
from .utils.dataIO import fileIO
import asyncio
import multidict
from aiohttp import web
from discord.ext import commands
from __main__ import send_cmd_help
from .utils import checks
import ssl
import signal, os
from urllib.parse import parse_qs, urlencode, parse_qsl, urlparse
import uuid
import requests
import json
from functools import partial
import logging

log = logging.getLogger("red.fimficauth")
log.setLevel(logging.DEBUG)

routes = web.RouteTableDef()

authorization_base_url = 'https://www.fimfiction.net/authorize-app'
redirect_uri =  'https://discords.net'
authorization_url = authorization_base_url + '?state={1}&response_type=code&redirect_uri=' + redirect_uri + '&client_id={0}'
token_url = 'https://www.fimfiction.net/api/v2/token'

@routes.get('/')
def do_GET(request):
    post_data = parse_qs(request.query_string)
    fileIO("data/fimficauth/webinfo.json", "save", post_data)
    if 'state' in post_data:
        if os.path.exists("data/fimficauth/token-{}.json".format(post_data['state'][0])):
            token_file = fileIO("data/fimficauth/token-{}.json".format(post_data['state'][0]),"load")
            if 'code' in post_data:
                token_file['auth_code'] = post_data['code'][0]
                fileIO("data/fimficauth/token-{}.json".format(post_data['state'][0]),"save", token_file)
    return web.Response(text="Auth Success")

class FimficAuth:
    def __init__(self, bot):
        self.bot = bot
        self.user_data = fileIO("data/fimficauth/users.json", "load")
        self.runner = None
        

    
    @commands.group(name='',no_pm=False, pass_context=True)
    async def fimficauth(self, ctx):
        """Manages authentication with fimfiction.net"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx) 

    @commands.command(name='authme',no_pm=False, pass_context=True)
    async def _start_auth(self, ctx, scope : str=None):
        client = fileIO("data/fimficauth/key.json", "load")
        if not client['client_key'] or not client['secret_key']:
            return await self.bot.say('I am not fully configured yet!')
        await self.bot.say('Initiating Fimfiction Auth')
        token = str(uuid.uuid4())
        fileIO("data/fimficauth/token-{}.json".format(token), "save", { 'user_id' : ctx.message.author.id, 'token' : token, 'auth_code' : False } )
        if scope is not None:
            scope = '&scope=' + scope
        else:
            scope = ''
        await self.bot.send_message(ctx.message.author, 'Please click the following link to be redirected to Fimfiction. {}'.format(authorization_url.format(client['client_key'],token) + scope))
        i = 60
        while i > 0:
            i -= 1
            await asyncio.sleep(10)
            token_file = fileIO("data/fimficauth/token-{}.json".format(token), "load")
            if token_file['auth_code']:
                break
        if not token_file['auth_code']:
            if i <= 0:
                return await self.bot.send_message(ctx.message.author, "Your auth failed! You took too long and it timed out.")
            else:
                return await self.bot.send_message(ctx.message.author, "Your auth failed! Something strange happened, you will probably need to contact an admin (bot admin).")
        payload = "client_id=" + client['client_key'] + "&client_secret=" + client['secret_key'] +  "&grant_type=authorization_code&code=" + token_file['auth_code'] + "&redirect_uri=" + redirect_uri
        headers = {
            'content-type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache"
            }

        await self.bot.send_message(ctx.message.author,payload)
        try:
            loop = asyncio.get_event_loop()
            response = loop.run_in_executor(None, partial(requests.request,"POST", token_url, data=payload, headers=headers))
            website = await response
            website = website.json()
        except:
            await self.bot.send_message(ctx.message.author,website)
            return await self.bot.send_message(ctx.message.author,"Sorry, something went wrong!")
        if 'user' not in website:
            await self.bot.send_message(ctx.message.author,token_url)
            await self.bot.send_message(ctx.message.author,payload)
            await self.bot.send_message(ctx.message.author,website)
            return await self.bot.send_message(ctx.message.author,"Sorry, something went really wrong!")
        scope = website['scope'].split()
        website['scope'] = scope
        self.user_data[ctx.message.author.id] = website
        fileIO("data/fimficauth/users.json", "save", self.user_data)
        await self.bot.send_message(ctx.message.author,"You are added!")
        
    @fimficauth.command(name='starthttp',no_pm=True, pass_context=True)
    @checks.is_owner()
    async def _start_serve(self, ctx):
        if self.runner is not None:
            return await self.bot.say('Server already running!')
        if ctx is not None:
            await self.bot.say('Starting httpd...')
            
        sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        sslcontext.load_cert_chain('data/cert.pem', 'data/key.pem')
        
        
        app = web.Application()
        app.add_routes(routes)
        self.runner = web.AppRunner(app)
        await self.runner.setup()
        site = web.TCPSite(self.runner, '0.0.0.0', 443, ssl_context=sslcontext)
        await site.start()
       
    @fimficauth.command(name='stophttp',no_pm=True, pass_context=True)
    @checks.is_owner()
    async def _stop_serve(self, ctx):
        if self.runner is not None:
            await self.runner.cleanup()
            self.runner = None
            if ctx is not None:
                await self.bot.say('Stopped httpd...')
            
    @fimficauth.command(name='key', pass_context=True, no_pm=False)
    @checks.is_owner()
    async def _key(self, ctx, client_key, secret_key):        
        """Sets the cog's client and secret API keys (must be obtained from fimfic
        
        
        [p]fimficsub key <client key> <secret key>"""
        if not client_key or not secret_key:
            return await self.bot.say("No keys given!")
        # Replace this next line with a proper key-validation once endpoints are completed
        fileIO("data/fimficauth/key.json", "save", { 'client_key' : client_key, 'secret_key' : secret_key } )
        await self.bot.say("Keys are set!")

    def __unload(self):
        if self.runner is not None:
            asyncio.ensure_future(self.runner.cleanup())
        

       
def check_folder():
    if not os.path.exists("data/fimficauth"):
        print("Creating data/fimficauth folder...")
        os.makedirs("data/fimficauth")
        
def check_files():
    data_default = {}
    if not fileIO("data/fimficauth/key.json", "check"):
        print("Creating default fimficauth key.json...")
        fileIO("data/fimficauth/key.json", "save", {'client_key' : '', 'secret_key' : ''})
    if not fileIO("data/fimficauth/users.json", "check"):
        print("Creating default fimficauth users.json...")
        fileIO("data/fimficauth/users.json", "save", data_default)
                    
def setup(bot):
    check_folder()
    check_files()
    bot.add_cog(FimficAuth(bot))