import discord
from redbot.core import Config, checks, commands, data_manager
from urllib.parse import urlparse, quote_plus, urlunparse, unquote
import aiohttp
import os
import asyncio
import re
import json
from functools import partial
from datetime import datetime, timedelta
import pyrfc3339 
import logging
from collections import namedtuple
import hashlib
import html2text
from bs4 import BeautifulSoup
from pyembed.core import PyEmbed
import sys
from zipfile import ZipFile
import zlib
from .imagesafe import Imagesafe, ImageEmbed, ImagesafeSettings
log = logging.getLogger("red.fimficsub")
log.setLevel(logging.ERROR)
user_agent = 'USE A SANE USER AGENT'

class DiscordLog:
    def __init__(self, urgency : str = 'Error', source : str = 'Unknown'):
        self.log_message = '{urgency} - {source}:\n'.format(urgency=urgency, source=source)
        self.urgency = urgency
        self.source = source
        
    def log(self, message : str = None):
        if message is None:
            raise ValueError
        self.log_message += "{message}\n".format(message=message)
    
    def get(self):
        return self.log_message

class Fimficsub(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.bot_path = str(data_manager.cog_data_path(cog_instance=self))
        if os.path.exists(self.bot_path + "/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.ERROR)
            log.error('Error Logging On')
        self.config = Config.get_conf(self, identifier=0x46696d666963737562)
        default_global = {
            "refresh" : 1,
            "latest_update_time" : '',
            "log_channel" : None,
            "authors" : {},
            "stories" : {}
        }
        self.config.register_global(**default_global)
        if os.path.exists(self.bot_path + "/metadata.json"):
            with open(self.bot_path + "/metadata.json") as json_file:
                self.metadata = json.load(json_file)
        else:
            self.metadata = {}
            with open(self.bot_path + '/metadata.json', 'w') as outfile:
                json.dump({},outfile,indent=4,ensure_ascii=False)
        self.sub_task = bot.loop.create_task(self.fimficsub_service())  
        self.rate = {}
        self.imagesafe = Imagesafe(None)
        self.regex_fimficurl = re.compile(r'https?://(?:www\.)?fimfiction\.net/story/([0-9]+)(?:/\S+)', flags=re.IGNORECASE)
        self.regex_fimficuserurl = re.compile(r'https?://(?:www\.)?fimfiction\.net/user/([0-9]+)(?:/\S+)', flags=re.IGNORECASE)
        self.current_hour = -1
        self.idle = False


    async def on_raw_reaction_add(self, msg):
        try:
            if not msg:
                return
            channel = self.bot.get_channel(msg.channel_id)
            message = await channel.get_message(msg.message_id)
            reaction = None
            for datum in message.reactions:
                if isinstance(datum.emoji, str):
                    if msg.emoji.is_custom_emoji():
                        continue
                    if datum.emoji == msg.emoji.name:
                        reaction = datum
                        break
                else:
                    if msg.emoji.is_unicode_emoji():
                        continue
                    if datum.emoji.id == msg.emoji.id:
                        reaction = datum
                        break
            if reaction is None:
                return
            if msg.guild_id is not None:
                guild = self.bot.get_guild(msg.guild_id)
                member = guild.get_member(msg.user_id)
            else:
                member = await self.bot.get_user_info(msg.user_id)
            await self.reaction(reaction,member)
            return
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)

    async def reaction(self, reaction, member):
        try:
            if member.bot:
                log.debug("First abort")
                return
            if reaction.custom_emoji:
                log.debug("Second abort")
                return
    #        if not reaction.message.author.bot:
    #            log.debug("Third abort")
    #            return
            message = reaction.message
            channel = message.channel
            log.debug(reaction.emoji)
            looper = True
            while looper:
                looper = False
                url = ''
                if message.embeds:
                    if message.embeds[0].url is not None:
                        url = message.embeds[0].url
                if not url:
                    url = message.clean_content[1:-1]
                story_id = self.regex_fimficurl.search(url)
                if story_id is not None:
                    story_id = story_id.group(1)
                author_id = self.regex_fimficuserurl.search(url)
                if author_id is not None:
                    author_id = author_id.group(1)
                if story_id:
                    log.debug('I found story: ' + story_id)
                    settings = ImagesafeSettings()
                    story = await self.imagesafe.get_story(story_id, settings = settings)
                    await self.imagesafe.add_fimfiction(story, settings = settings)
                    settings.embed.type = 'story'
                    output = settings.embed.embed
                    author_id = story['author'][0]['id']
                else:
                    if not author_id:
                        break
                if reaction.emoji == "ℹ": #Info emoji
                    if not member.permissions_in(message.channel).manage_messages:
                        break
                    if author_id is not None or story_id is not None:
                        await message.add_reaction("➕")
                        await message.add_reaction("➖")
                    else:
                        log.debug("No Story found")
                    break
                if reaction.emoji == "🌟": #star
                    if story_id:
                        async with self.config.stories() as stories:
                            if story_id not in stories:
                                stories[story_id] = {'users' : [], 'channels' : []}
                            if member.id not in stories[story_id]['users']:
                                stories[story_id]['users'].append(member.id)
                                sent_message = await member.send('Added subscription to story: ' + '<{}>'.format(url), embed=output)
                            else:
                                sent_message = await member.send('You are already subscribed to story: ' + '<{}>'.format(url), embed=output)
                        await self.add_emoji(sent_message)
                    else:
                        log.debug("No Story found")
                    break
                if reaction.emoji == "✴": #black star
                    if story_id:
                        async with self.config.stories() as stories:
                            if story_id not in stories:
                                stories[story_id] = {'users' : [], 'channels' : []}
                            if member.id in stories[story_id]['users']:
                                stories[story_id]['users'].remove(member.id)
                                sent_message = await member.send('Removed subscription to story: ' + '<{}>'.format(url), embed=output)
                            else:
                                sent_message = await member.send('You aren\'t subscribed to story: ' + '<{}>'.format(url), embed=output)
                        await self.add_emoji(sent_message)
                    else:
                        log.debug("No Story found")
                    break
                if reaction.emoji == "🇦": #Blue A
                    if author_id is not None:
                        settings = ImagesafeSettings()
                        author = await self.imagesafe.get_user(author_id, settings = settings)
                        if author is not None:
                            log.debug(author)
                            self.imagesafe.add_fimfiction_user(author, settings = settings)
                            output = settings.embed.embed
                        async with self.config.authors() as authors:
                            if author_id not in authors:
                                authors[author_id] = {'users' : [], 'channels' : []}
                            if member.id not in authors[author_id]['users']:
                                authors[author_id]['users'].append(member.id)
                                sent_message = await member.send('Added subscription to author: ' + "https://www.fimfiction.net/user/{0}/{1}".format(author['id'],quote_plus(author['name'])),embed=output)
                            else:
                                sent_message = await member.send('You are already subscribed to author: ' + "<https://www.fimfiction.net/user/{0}/{1}>".format(author['id'],quote_plus(author['name'])),embed=output)
                        await self.add_emoji(sent_message,story=False)
                    else:
                        log.debug("No Story found")
                    break
                if reaction.emoji == "🅰": #Red A
                    if author_id is not None:
                        settings = ImagesafeSettings()
                        author = await self.imagesafe.get_user(author_id, settings = settings)
                        if author is not None:
                            log.debug(author)
                            embed = ImageEmbed()
                            self.imagesafe.add_fimfiction_user(author, settings = settings)
                            output = settings.embed.embed
                        async with self.config.authors() as authors:
                            if author_id not in authors:
                                authors[author_id] = {'users' : [], 'channels' : []}
                            if member.id in authors[author_id]['users']:
                                authors[author_id]['users'].remove(member.id)
                                sent_message = await member.send('Removed subscription to author: ' + "https://www.fimfiction.net/user/{0}/{1}".format(author['id'],quote_plus(author['name'])),embed=output)
                            else:
                                sent_message = await member.send('You aren\'t subscribed to author: ' + "<https://www.fimfiction.net/user/{0}/{1}>".format(author['id'],quote_plus(author['name'])),embed=output)
                        await self.add_emoji(sent_message,story=False)
                    else:
                        log.debug("No Story found")
                    break
                if reaction.emoji == "➕": #Plus
                    if not checks.admin_or_permissions(manage_messages=True):
                        break
                    if story_id:
                        async with self.config.stories() as stories:
                            if story_id not in stories:
                                stories[story_id] = {'users' : [], 'channels' : []}
                            if channel.id not in stories[story_id]['channels']:
                                stories[story_id]['channels'].append(channel.id)
                                await channel.send('Added subscription to story in {}'.format(channel.mention))
                            else:
                                await channel.send('Already subscribed!')
                        break
                    if author_id is not None:
                        async with self.config.authors() as authors:
                            if author_id not in authors:
                                authors[author_id] = {'users' : [], 'channels' : []}
                            if channel.id not in authors[author_id]['channels']:
                                authors[author_id]['channels'].append(channel.id)
                                await channel.send('Added subscription to author in {}'.format(channel.mention))
                            else:
                                await channel.send('Already subscribed!')
                    break
                if reaction.emoji == "➖": #Minus
                    if not checks.admin_or_permissions(manage_messages=True):
                        break
                    if story_id:
                        async with self.config.stories() as stories:
                            if story_id not in stories:
                                stories[story_id] = {'users' : [], 'channels' : []}
                            if channel.id in stories[story_id]['channels']:
                                stories[story_id]['channels'].remove(channel.id)
                                await channel.send('Removed subscription to story in {}'.format(channel.mention))
                            else:
                                await channel.send('Not subscribed!')
                        break
                    if author_id is not None:
                        async with self.config.authors() as authors:
                            if author_id not in authors:
                                authors[author_id] = {'users' : [], 'channels' : []}
                            if channel.id in authors[author_id]['channels']:
                                authors[author_id]['channels'].remove(channel.id)
                                await channel.send('Removed subscription to author in {}'.format(channel.mention))
                            else:
                                await channel.send('Not subscribed!')
                    break
                break
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
                
    @commands.group(name='fimficsub')
    @checks.admin_or_permissions(manage_guild=True)
    async def fimficsub(self, ctx):
        """Manages subscription to a story on fimfiction"""

    @fimficsub.command(name='refresh')
    @checks.is_owner()
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes): [p]fimficsub refresh <time in minutes>"""
        if refresh_time < 0:
            await ctx.send("Invalid number!")
            return
        await self.config.refresh.set(refresh_time)
        if refresh_time:
            await ctx.send("Refresh time set to %i minutes." % (refresh_time))
        else:
            await ctx.send("Fimficsub disabled!")

    @fimficsub.command(name='check')
    @checks.admin_or_permissions(manage_guild=True)
    async def _check(self,ctx):
        if self.sub_task.done():
            await self.log_to_channel("Can't find subroutine")
            await asyncio.sleep(60)
            if self.sub_task.done():
                await self.log_to_channel("Still can't find subroutine")
                try:
                    output = self.sub_task.result()
                except asyncio.CancelledError:
                    return
                except asyncio.InvalidStateError:
                    return
                except:
                    await self.log_to_channel("Subroutine Crashed - Restarting")
                    e = sys.exc_info()[0]
                    log.error( "Error: %s" % e )
                    log.error( sys.exc_info())
                    log.exception(e)
                await self.log_to_channel("Subroutine not running - Restarting")
                self.sub_task = self.bot.loop.create_task(self.fimficsub_service())  
            else:
                await self.log_to_channel("Fimficsub was started by something else.")
        else:
            await self.log_to_channel("Fimficsub running okay.")

    @fimficsub.command(name='log')
    @checks.is_owner()
    @commands.guild_only()
    async def _log_channel(self, ctx, spam_channel : discord.TextChannel):        
        """Set the log channel for the cog
        
        [p]fimficsub log <channel name>"""
        if not spam_channel:
            return await ctx.send( "Channel not found." )
        await self.config.log_channel.set(spam_channel.id)
        await ctx.send( "Channel to spam logs set to {}.".format(spam_channel.mention))
 
    @commands.command(name='fimficstats')
    async def _sitestats(self, ctx):
        if os.path.exists(self.bot_path + "/fimficdata.json"):
            with open(self.bot_path + "/fimficdata.json") as json_file:
                data_array = json.load(json_file)
        else:
            data_array = {}

        if 'last_access' not in data_array:
            data_array['last_access'] = ''
        update = False
        log.debug( data_array['last_access'])
        log.debug( pyrfc3339.generate(datetime.utcnow(),accept_naive=True) )
        if data_array['last_access'] < pyrfc3339.generate(datetime.utcnow(),accept_naive=True):
            headers = {'user-agent' : user_agent}
            async with aiohttp.ClientSession() as session:
                log.debug('I has a session!')
                async with session.request("GET", 'https://www.fimfiction.net/statistics', headers=headers) as resp: 
                    try:
                        resp.raise_for_status()      
                    except:
                        log.error('Bad status: ' + str(resp.status))
                        return
                    soup = BeautifulSoup(await resp.text(), 'html.parser')
            with open(self.bot_path + '/fimficdata.json', 'w') as outfile:
                data_array = { 
                    'last_access' : pyrfc3339.generate(datetime.utcnow() + timedelta(days=1), accept_naive=True),
                    'stories_approved' : None,
                    'blogs_posted' : None,
                    'users_joined' : None
                }
                data_array['stories_approved'] = json.loads(soup.find('div',attrs={'class' : 'content_box','data-element' : 'storiesApproved'}).get('data-data'))
                data_array['blogs_posted'] = json.loads(soup.find('div',attrs={'class' : 'content_box','data-element' : 'blogsPosted'}).get('data-data'))
                data_array['users_joined'] = json.loads(soup.find('div',attrs={'class' : 'content_box','data-element' : 'usersJoined'}).get('data-data'))
                json.dump(data_array,outfile,indent=4,ensure_ascii=False)
            update = True
        if update or not ( os.path.exists(self.bot_path + '/users_joined_monthly.csv') and os.path.exists(self.bot_path + '/users_joined_daily.csv') and os.path.exists(self.bot_path + '/blogs_posted_monthly.csv') and os.path.exists(self.bot_path + '/stories_approved_daily.csv') and os.path.exists(self.bot_path + '/stories_approved_monthly.csv') and os.path.exists(self.bot_path + '/stories_approved_daily.csv') ):
            daily_data = ['Date,Everyone,Teen,Mature\n']
            monthly_data = ['Date,Everyone,Teen,Mature\n']
            everyone = 0
            teen = 0
            mature = 0
            month_year = data_array['stories_approved'][0]['date'].rsplit('-', 1)[0]
            for datum in data_array['stories_approved']:
                daily_output = datum['date'] + ','
                day = datum['date'].split('-')[-1]
                if day == '01':
                    monthly_data.append(month_year + ',' + str(everyone) + ',' + str(teen) + ',' + str(mature) + '\n')
                    everyone = 0
                    teen = 0
                    mature = 0
                month_year = datum['date'].rsplit('-', 1)[0]
                if '244' in datum:
                    everyone += datum['244']
                    daily_output += str(datum['244']) + ','
                else:
                    daily_output += ','
                if '245' in datum:
                    teen += datum['245']
                    daily_output += str(datum['245']) + ','
                else:
                    daily_output += ','
                if '246' in datum:
                    mature += datum['246']
                    daily_output += str(datum['246']) + '\n'
                else:
                    daily_output += '\n'
                daily_data.append(daily_output)
            if os.path.exists(self.bot_path + '/stories_approved_daily.csv'):
                os.remove(self.bot_path + '/stories_approved_daily.csv')
            if os.path.exists(self.bot_path + '/stories_approved_monthly.csv'):
                os.remove(self.bot_path + '/stories_approved_monthly.csv')
            daily_file = open(self.bot_path + '/stories_approved_daily.csv','w')
            daily_file.writelines(daily_data)
            daily_file.close()
            monthly_file = open(self.bot_path + '/stories_approved_monthly.csv','w')
            monthly_file.writelines(monthly_data)
            monthly_file.close()
            
            
            daily_data = ['Date,Daily,Total\n']
            monthly_data = ['Date,Monthly,Total\n']
            monthly = 0
            month_year = data_array['blogs_posted'][0]['date'].rsplit('-', 1)[0]
            for datum in data_array['blogs_posted']:
                daily_output = datum['date'] + ','
                day = datum['date'].split('-')[-1]
                if day == '01':
                    monthly_data.append(month_year + ',' + str(monthly) + ',' + str(datum['aggregate'] - datum['daily']) + '\n')
                    monthly = 0
                month_year = datum['date'].rsplit('-', 1)[0]
                if 'daily' in datum:
                    monthly += datum['daily']
                    daily_output += str(datum['daily']) + ','
                else:
                    daily_output += ','
                if 'aggregate' in datum:
                    daily_output += str(datum['aggregate']) + '\n'
                else:
                    daily_output += '\n'
                daily_data.append(daily_output)
            if os.path.exists(self.bot_path + '/blogs_posted_daily.csv'):
                os.remove(self.bot_path + '/blogs_posted_daily.csv')
            if os.path.exists(self.bot_path + '/blogs_posted_monthly.csv'):
                os.remove(self.bot_path + '/blogs_posted_monthly.csv')
            daily_file = open(self.bot_path + '/blogs_posted_daily.csv','w')
            daily_file.writelines(daily_data)
            daily_file.close()
            monthly_file = open(self.bot_path + '/blogs_posted_monthly.csv','w')
            monthly_file.writelines(monthly_data)
            monthly_file.close()
            
            
            daily_data = ['Date,Daily,Total\n']
            monthly_data = ['Date,Monthly,Total\n']
            monthly = 0
            month_year = data_array['users_joined'][0]['date'].rsplit('-', 1)[0]
            for datum in data_array['users_joined']:
                daily_output = datum['date'] + ','
                day = datum['date'].split('-')[-1]
                if day == '01':
                    monthly_data.append(month_year + ',' + str(monthly) + ',' + str(datum['aggregate'] - datum['daily']) + '\n')
                    monthly = 0
                month_year = datum['date'].rsplit('-', 1)[0]
                if 'daily' in datum:
                    monthly += datum['daily']
                    daily_output += str(datum['daily']) + ','
                else:
                    daily_output += ','
                if 'aggregate' in datum:
                    daily_output += str(datum['aggregate']) + '\n'
                else:
                    daily_output += '\n'
                daily_data.append(daily_output)
            if os.path.exists(self.bot_path + '/users_joined_daily.csv'):
                os.remove(self.bot_path + '/users_joined_daily.csv')
            if os.path.exists(self.bot_path + '/users_joined_monthly.csv'):
                os.remove(self.bot_path + '/users_joined_monthly.csv')
            daily_file = open(self.bot_path + '/users_joined_daily.csv','w')
            daily_file.writelines(daily_data)
            daily_file.close()
            monthly_file = open(self.bot_path + '/users_joined_monthly.csv','w')
            monthly_file.writelines(monthly_data)
            monthly_file.close()
        
        
        if os.path.exists(self.bot_path + '/data_files.zip'):
            os.remove(self.bot_path + '/data_files.zip')
        open(self.bot_path + '/data_files.zip','a').close()
        zip = ZipFile(self.bot_path + '/data_files.zip','w')
        zip.write(self.bot_path + '/stories_approved_daily.csv',arcname='stories_approved_daily.csv')
        zip.write(self.bot_path + '/stories_approved_monthly.csv',arcname='stories_approved_monthly.csv')
        zip.write(self.bot_path + '/blogs_posted_daily.csv',arcname='blogs_posted_daily.csv')
        zip.write(self.bot_path + '/blogs_posted_monthly.csv',arcname='blogs_posted_monthly.csv')
        zip.write(self.bot_path + '/users_joined_daily.csv',arcname='users_joined_daily.csv')
        zip.write(self.bot_path + '/users_joined_monthly.csv',arcname='users_joined_monthly.csv')
        zip.close()
            
        with open(self.bot_path + '/data_files.zip','rb') as zipfile:
            await ctx.author.send(file=discord.File(zipfile,'data_files.zip'))

    async def fimficsub_service(self):
        """Trigger loop"""
        if os.path.exists(self.bot_path + "/istest"):
            await self.log_to_channel("Fimficsub running on debug! Exiting service.")
            return
        await self.log_to_channel("Starting fimficsub service.")
        await self.bot.wait_until_ready()
        try:
            while True:
                try:
                    refresh = await self.config.refresh()
                    if refresh:
                        log.info("Fimficsub service tick")
                        await self.spam_looper()
                    else:
                        refresh = 1
                    await asyncio.sleep(refresh * 60)
                except asyncio.CancelledError:
                    raise
                except:
                    e = sys.exc_info()[0]
                    log.error( "Error: %s" % e )
                    log.error( sys.exc_info())
                    log.exception(e)
                    await asyncio.sleep(120)
        except asyncio.CancelledError:
            await self.log_to_channel("Exiting fimficsub service.")

    @fimficsub.command(name='meta', pass_context=True)
    async def _meta(self, ctx):
        """Requests Metadata to be DMed to you"""
        meta = await self.metadata_spam()
        if meta:
            for msg in meta:
                await ctx.author.send(msg)
        else:
            await ctx.author.send("No Metadata!")

    @fimficsub.command(name='test', pass_context=True)
    async def _test(self, ctx, story : str=''):   
        log.setLevel(logging.DEBUG)
        # async with self.config.stories() as stories:
        #     for story_id, data in dict(stories).items():
        #         if 'channels' in data:
        #             new_list = []
        #             for values in list(data['channels']):
        #                 new_list.append(int(values))
        #             stories[story_id]['channels'] = new_list
        #         if 'users' in data:
        #             new_list = []
        #             for values in list(data['users']):
        #                 new_list.append(int(values))
        #             stories[story_id]['users'] = new_list
        # async with self.config.authors() as authors:
        #     for author_id, data in dict(authors).items():
        #         if 'channels' in data:
        #             new_list = []
        #             for values in list(data['channels']):
        #                 new_list.append(int(values))
        #             authors[author_id]['channels'] = new_list
        #         if 'users' in data:
        #             new_list = []
        #             for values in list(data['users']):
        #                 new_list.append(int(values))
        #             authors[author_id]['users'] = new_list



        await self.spam_looper()
        if os.path.exists("data/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging: On')
        else:
            log.setLevel(logging.INFO)
            log.error('Info Logging: On')
                
    async def spam_looper(self):
        try:
            log.info('Starting spam loop')
            channel_log = DiscordLog('INFO', 'Fimficsub')
            channel_log.log('```Starting spam loop```')
            #cleans up empty subs
            for cleanup_tag in [ 'authors', 'stories' ]:
                async with self.config.all() as data:
                    log.debug(data)
                    if cleanup_tag in data:
                        for id in list(data[cleanup_tag].keys()):
                            subscription = data[cleanup_tag][id]
                            if 'channels' in subscription and subscription['channels']:
                                break
                            if 'users' in subscription and subscription['users']:
                                break
                            del data[cleanup_tag][id]
                if not data['authors'] and not data['stories']:
                    if not self.idle:
                        log.info('Going idle...')
                        self.idle = True
                    return
                else:
                    if self.idle:
                        log.info('Going active!')
                        self.idle = False
                        await self.config.latest_update_time.set('')
            #sanity check on latest_update_time
            current_time = datetime.utcnow() - timedelta(minutes = 1)
            latest_update_time = await self.config.latest_update_time()
            if latest_update_time:
                latest_update = date_to_z(latest_update_time)
            else:
                latest_update_time = pyrfc3339.generate(current_time,accept_naive=True)
                log.debug('Last update not found, set to: ' + latest_update_time)
                await self.config.latest_update_time.set(latest_update_time)
                return None
            log.debug('Last update found: ' + latest_update)
            channel_log.log('Last update found: `' + latest_update + '`')
            channel_log.log('Current time: `' + pyrfc3339.generate(current_time,accept_naive=True) + '`')
            stories = {}
            #loop builds the array of stories if there is over 100 pending
            update_bounds = latest_update
            while True:
                story_list = await self.imagesafe.get_stories({ 
                    'filters' : { 'date_updated' : [update_bounds, pyrfc3339.generate(current_time,accept_naive=True)]},
                    'fields' : { 
                        'story' : [ 'name', 'title', 'short_description', 'completion_status', 'content_rating', 'date_updated', 'date_modified', 'date_published', 'num_words', 'cover_image', 'author', 'tags', 'num_likes', 'num_dislikes', 'color' ], 
                        'user' : [ 'date_joined', 'id', 'name', 'avatar', 'bio' ] 
                    }, 
                    'include' : 'characters,author,tags',
                    'sort' : 'date_updated'
                })
                log.debug(update_bounds)
                if story_list is None:
                    break
                if 'story' in story_list:
                    stories = { **stories, **story_list['story'] }
                    for story in story_list['story'].values():
                        if update_bounds < date_to_z(story['date_updated']):
                            update_bounds = date_to_z(story['date_updated'])
                    if len(story_list['story']) < 100:
                        break
                    continue
                break
            log.debug(len(stories))
            channel_log.log(str(len(stories)) + ' stories to process.')
            newest_update = latest_update
            targets_spammed = []
            #is there anything to process?
            users_spammed = []
            channels_spammed = []
            stories_found = []
            if stories:
                for story in stories.values(): #loops for days (and stories)
                    if 'id' not in story:
                        continue
                    embed = None
                    self.add_story_metadata(story, story['date_updated'])
                    log.debug(story['id'])
                    if date_to_z(story['date_updated']) == date_to_z(story['date_published']):
                        pre_spam = 'New Story: '
                    else:
                        pre_spam = 'Updated Story: '
                    sub_info = await self.config.stories()
                    log.debug(sub_info)
                    if sub_info:
                        if story['id'] in sub_info.keys(): #is anything subscribed to this story?
                            sent_message = []
                            if 'channels' in sub_info[story['id']]:#a channel is subscribed
                                for channel in sub_info[story['id']]['channels']:
                                    try:
                                        channel = self.bot.get_channel(int(channel))
                                        if channel is None:
                                            continue
                                        if '{channel} - {story_id}'.format(channel = channel.id, story_id = story['id']) in targets_spammed:
                                            continue
                                        if embed is None:
                                            settings = ImagesafeSettings()
                                            story['simple'] = False
                                            await self.imagesafe.add_fimfiction(story, settings = settings)
                                            settings.primary = False
                                            processed_links = []
                                            if not settings.image_data and settings.primary_image:
                                                log.debug(settings.primary_image)
                                                settings.image_data = await self.imagesafe.get_image_data(settings.primary_image)
                                            while settings.embed.links:
                                                links = settings.embed.links.pop(0)
                                                await asyncio.sleep(1)
                                                log.debug('searching for extra data from: ' + links)
                                                if links in processed_links:
                                                    continue
                                                processed_links.append(links)
                                                if links.startswith('file:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_filename(file_name = links, settings = settings)
                                                elif links.startswith('hash:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_from_hash(links, settings = settings)
                                                else:
                                                    await self.imagesafe.extract_embed(links, settings = settings)
                                            settings.embed.type = 'story'
                                            embed = settings.embed.embed
                                        sent_message.append( await channel.send(content=pre_spam + '<{}>'.format(embed.url), embed=embed) )
                                        targets_spammed.append('{channel} - {story_id}'.format(channel = channel.id, story_id = story['id']))
                                        channels_spammed.append('{channel.id} - {channel.name}'.format(channel=channel))
                                        stories_found.append('{story[id]} - {story[title]}'.format(story=story))
                                        self.add_story_metadata(story, story['date_updated'], True)
                                    except asyncio.CancelledError:
                                        raise
                                    except:
                                        e = sys.exc_info()[0]
                                        log.error( "Error: %s" % e )
                                        
                                        log.error( sys.exc_info())
                                        log.exception(e)
                            if 'users' in sub_info[story['id']]:#a user is subscribed
                                for user in sub_info[story['id']]['users']:
                                    try:
                                        user = self.bot.get_user(int(user))
                                        if user is None:
                                            continue
                                        if '{user} - {story_id}'.format(user = user.id, story_id = story['id']) in targets_spammed:
                                            continue
                                        if embed is None:
                                            settings = ImagesafeSettings()
                                            story['simple'] = False
                                            await self.imagesafe.add_fimfiction(story, settings = settings)
                                            settings.primary = False
                                            processed_links = []
                                            if not settings.image_data and settings.primary_image:
                                                log.debug(settings.primary_image)
                                                settings.image_data = await self.imagesafe.get_image_data(settings.primary_image)
                                            while settings.embed.links:
                                                links = settings.embed.links.pop(0)
                                                await asyncio.sleep(1)
                                                log.debug('searching for extra data from: ' + links)
                                                if links in processed_links:
                                                    continue
                                                processed_links.append(links)
                                                if links.startswith('file:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_filename(file_name = links, settings = settings)
                                                elif links.startswith('hash:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_from_hash(links, settings = settings)
                                                else:
                                                    await self.imagesafe.extract_embed(links, settings = settings)
                                            settings.embed.type = 'story'
                                            embed = settings.embed.embed
                                        sent_message.append( await user.send(content=pre_spam + '<{}>'.format(embed.url), embed=embed) )
                                        targets_spammed.append('{user} - {story_id}'.format(user = user.id, story_id = story['id']))
                                        users_spammed.append('{user.id} - {user.name}'.format(user=user))
                                        stories_found.append('{story[id]} - {story[title]}'.format(story=story))
                                        self.add_story_metadata(story, story['date_updated'], True)
                                    except asyncio.CancelledError:
                                        raise
                                    except:
                                        e = sys.exc_info()[0]
                                        log.error( "Error: %s" % e )
                                        
                                        log.error( sys.exc_info())
                                        log.exception(e)
                            for message in sent_message:
                                asyncio.ensure_future(self.add_emoji(message))
                    authors = await self.config.authors()
                    log.debug(authors)
                    if authors:
                        log.debug('I have authors to process!')
                        log.debug('Checking for story author id: ' + story['author'][0]['id'])
                        if story['author'][0]['id'] in authors.keys():#is anything subscribed to this writer?
                            log.debug('Found at least one author!')
                            sent_message = []
                            if 'channels' in authors[story['author'][0]['id']]:#a channel is subscribed
                                log.debug('Looking for channels to spam')
                                for channel in authors[story['author'][0]['id']]['channels']:
                                    try:
                                        channel = self.bot.get_channel(int(channel))
                                        if channel is None:
                                            continue
                                        log.debug('Processing channel: ' + str(channel.id))
                                        if '{channel} - {story_id}'.format(channel = channel.id, story_id = story['id']) in targets_spammed:
                                            continue
                                        if embed is None:
                                            settings = ImagesafeSettings()
                                            story['simple'] = False
                                            await self.imagesafe.add_fimfiction(story, settings = settings)
                                            settings.primary = False
                                            processed_links = []
                                            if not settings.image_data and settings.primary_image:
                                                log.debug(settings.primary_image)
                                                settings.image_data = await self.imagesafe.get_image_data(settings.primary_image)
                                            while settings.embed.links:
                                                links = settings.embed.links.pop(0)
                                                await asyncio.sleep(1)
                                                log.debug('searching for extra data from: ' + links)
                                                if links in processed_links:
                                                    continue
                                                processed_links.append(links)
                                                if links.startswith('file:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_filename(file_name = links, settings = settings)
                                                elif links.startswith('hash:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_from_hash(links, settings = settings)
                                                else:
                                                    await self.imagesafe.extract_embed(links, settings = settings)
                                            settings.embed.type = 'story'
                                            embed = settings.embed.embed
                                        sent_message.append( await channel.send(content=pre_spam + '<{}>'.format(embed.url), embed=embed) )
                                        targets_spammed.append('{channel} - {story_id}'.format(channel = channel.id, story_id = story['id']))
                                        channels_spammed.append('{channel.id} - {channel.name}'.format(channel=channel))
                                        stories_found.append('{story[id]} - {story[title]}'.format(story=story))
                                        self.add_story_metadata(story, story['date_updated'], True)
                                    except asyncio.CancelledError:
                                        raise
                                    except:
                                        e = sys.exc_info()[0]
                                        log.error( "Error: %s" % e )
                                        
                                        log.error( sys.exc_info())
                                        log.exception(e)
                            if 'users' in authors[story['author'][0]['id']]:#a user is subscribed
                                log.debug('Looking for users to spam')
                                for user in authors[story['author'][0]['id']]['users']:
                                    try:
                                        user = self.bot.get_user(int(user))
                                        log.debug(user)
                                        if user is None:
                                            continue
                                        log.debug('Processing user: ' + str(user.id))
                                        if '{user} - {story_id}'.format(user = user.id, story_id = story['id']) in targets_spammed:
                                            continue
                                        if embed is None:
                                            settings = ImagesafeSettings()
                                            story['simple'] = False
                                            await self.imagesafe.add_fimfiction(story, settings = settings)
                                            settings.primary = False
                                            processed_links = []
                                            if not settings.image_data and settings.primary_image:
                                                log.debug(settings.primary_image)
                                                settings.image_data = await self.imagesafe.get_image_data(settings.primary_image)
                                            while settings.embed.links:
                                                links = settings.embed.links.pop(0)
                                                await asyncio.sleep(1)
                                                log.debug('searching for extra data from: ' + links)
                                                if links in processed_links:
                                                    continue
                                                processed_links.append(links)
                                                if links.startswith('file:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_filename(file_name = links, settings = settings)
                                                elif links.startswith('hash:'):
                                                    links = links[5:]
                                                    await self.imagesafe.extract_from_hash(links, settings = settings)
                                                else:
                                                    await self.imagesafe.extract_embed(links, settings = settings)
                                            settings.embed.type = 'story'
                                            embed = settings.embed.embed
                                        sent_message.append( await user.send(content=pre_spam + '<{}>'.format(embed.url), embed=embed) )
                                        targets_spammed.append('{user} - {story_id}'.format(user = user.id, story_id = story['id']))
                                        users_spammed.append('{user.id} - {user.name}'.format(user=user))
                                        stories_found.append('{story[id]} - {story[title]}'.format(story=story))
                                        self.add_story_metadata(story, story['date_updated'], True)
                                    except asyncio.CancelledError:
                                        raise
                                    except:
                                        e = sys.exc_info()[0]
                                        log.error( "Error: %s" % e )
                                        
                                        log.error( sys.exc_info())
                                        log.exception(e)
                            for message in sent_message:
                                asyncio.ensure_future(self.add_emoji(message))
                    if newest_update < date_to_z(story['date_updated']):
                        newest_update = date_to_z(story['date_updated'])
            await self.config.latest_update_time.set(newest_update)
            if users_spammed:
                channel_log.log('Users spammed: \n' + '\n'.join(list(set(users_spammed))))
            if channels_spammed:
                channel_log.log('Channels spammed: \n' + '\n'.join(list(set(channels_spammed))))
            if stories_found:
                channel_log.log('Stories spammed: \n' + '\n'.join(list(set(stories_found))))
            channel_log.log('```End spam loop```')
            await self.log_to_channel(channel_log.get())

        except asyncio.CancelledError:
            raise
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
            
    def add_story_metadata(self,story, current_time : str = None, spammed : bool = False):
        with open(self.bot_path + '/metadata.json', 'w') as outfile:
            data = {}
            if current_time is None:
                current_time = pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
            else:
                current_time = date_to_z(current_time)
            if date_to_z(story['date_updated']) == date_to_z(story['date_published']):
                data['new_story'] = True
            if spammed:
                data['spammed'] = True
            self.metadata[current_time + '|' + story['id']] = data
            json.dump(self.metadata,outfile,indent=4,ensure_ascii=False)

    async def add_emoji(self,message, story : bool = True, writer : bool = True):
        if story:
            await message.add_reaction("🌟")
            await message.add_reaction("✴")
        if writer:
            await message.add_reaction("🇦")
            await message.add_reaction("🅰")
        if isinstance(message.channel,discord.TextChannel):
            if writer or story:
                await message.add_reaction("ℹ")

    async def log_to_channel(self, message : str = None):
        try:
            current_time = datetime.utcnow()
            if message is None:
                return
            channel = await self.config.log_channel()
            channel = self.bot.get_channel(channel)
            if channel is None:
                return
            await channel.send(message)
            if self.current_hour < current_time.hour or ( self.current_hour == 23 and current_time.hour == 0 ):
                self.current_hour = current_time.hour
                meta = await self.metadata_spam()
                if meta:
                    for msg in meta:
                        await channel.send(msg)
                else:
                    await channel.send("No Metadata!")
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            raise
    
    async def metadata_spam(self, source : str = 'Fimficsub'):
        if not self.metadata:
            return
        current_time = datetime.utcnow()
        oldest = pyrfc3339.generate(current_time - timedelta(days = 30), accept_naive=True)
        with open(self.bot_path + '/metadata.json', 'w') as outfile:
            for key in list(self.metadata.keys()):
                if key.split('|',1)[0] < oldest:
                    del self.metadata[key]
            json.dump(self.metadata,outfile,indent=4,ensure_ascii=False)
        msg = '```\n'
        msg += 'Total updates:\n'
        msg += '  Number of updates in last hour: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(hours = 1), accept_naive=True) and 'new_story' not in value)).keys())) + '\n'
        msg += '  Number of updates in last day: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'new_story' not in value)).keys())) + '\n'
        if len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] < pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'new_story' not in value)).keys()) != 0:
            msg += '  Number of updates in last week: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 7), accept_naive=True) and 'new_story' not in value)).keys())) + '\n'
        
        msg += '\nTotal new stories:\n'
        msg += '  Number of stories in last hour: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(hours = 1), accept_naive=True) and 'new_story' in value)).keys())) + '\n'
        msg += '  Number of stories in last day: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'new_story' in value)).keys())) + '\n'
        if len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] < pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'new_story' in value)).keys()) != 0:
            msg += '  Number of stories in last week: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 7), accept_naive=True) and 'new_story' in value)).keys())) + '\n'
                
        msg += '\nSubscribed updates:\n'
        msg += '  Number of updates in last hour: ' + str(len(dict((key,value) for key, value in self.metadata.items() if ( key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(hours = 1), accept_naive=True) and 'spammed' in value and 'new_story' not in value)).keys())) + '\n'
        msg += '  Number of updates in last day: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'spammed' in value and 'new_story' not in value)).keys())) + '\n'
        if len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] < pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'spammed' in value and 'new_story' not in value)).keys()) != 0:
            msg += '  Number of updates in last week: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 7), accept_naive=True) and 'spammed' in value and 'new_story' not in value)).keys())) + '\n'
                
        msg += '\nSubscribed new stories:\n'
        msg += '  Number of stories in last hour: ' + str(len(dict((key,value) for key, value in self.metadata.items() if ( key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(hours = 1), accept_naive=True) and 'spammed' in value and 'new_story' in value)).keys())) + '\n'
        msg += '  Number of stories in last day: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'spammed' in value and 'new_story' in value)).keys())) + '\n'
        if len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] < pyrfc3339.generate(current_time - timedelta(days = 1), accept_naive=True) and 'spammed' in value and 'new_story' in value)).keys()) != 0:
            msg += '  Number of stories in last week: ' + str(len(dict((key,value) for key, value in self.metadata.items() if (key.rsplit('|',1)[0] > pyrfc3339.generate(current_time - timedelta(days = 7), accept_naive=True) and 'spammed' in value and 'new_story' in value)).keys())) + '\n'
            
        msg += '\nAverage:\n'
        time_array = sorted(list(self.metadata.keys()))
        if time_array[0] is not None:
            time_diff = pyrfc3339.parse(time_array[-1].rsplit('|',1)[0]) - pyrfc3339.parse(time_array[0].rsplit('|',1)[0])
            time_diff = time_diff.total_seconds() / 3600
            msg += '  Stories per hour: {:10.2f}\n'.format(len(dict((key,value) for key, value in self.metadata.items() if 'new_story' in value).keys()) / time_diff)
            msg += '  Updates per hour: {:10.2f}\n'.format(len(dict((key,value) for key, value in self.metadata.items() if 'new_story' not in value).keys()) / time_diff)
            
        msg += '\nHourly breakdown (UTC):\n'
        msg += '         |'
        msg2 = '  New    |'
        msg3 = '  Updates|'
        hour_array = [ 
            '00',
            '01',
            '02',
            '03',
            '04',
            '05',
            '06',
            '07',
            '08',
            '09',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23'
        ]
        time_diff = pyrfc3339.parse(time_array[-1].rsplit('|',1)[0]) - pyrfc3339.parse(time_array[0].rsplit('|',1)[0])
        time_diff = time_diff.total_seconds() / 86400
        for hour in hour_array:
            value_new_stories = len(dict((key,value) for key, value in self.metadata.items() if pyrfc3339.parse(key.rsplit('|',1)[0]).hour == int(hour) and 'new_story' in value).keys()) / time_diff
            value_updates = len(dict((key,value) for key, value in self.metadata.items() if pyrfc3339.parse(key.rsplit('|',1)[0]).hour == int(hour) and 'new_story' not in value).keys()) / time_diff
            msg +=  ' ' + hour + ' |'
            msg2 += '{:4.1f}|'.format(value_new_stories)
            msg3 += '{:4.1f}|'.format(value_updates)
        msg += '\n' + msg2 + '\n' + msg3 + '\n'
        msg += '\nIt is now {} UTC\n'.format(pyrfc3339.generate(current_time, accept_naive=True))
        msg += '```'
        return [ "METADATA - {source}: {message}".format(source=source, message=msg) ]
    
    def __unload(self):
        self.sub_task.cancel()
      
def jsonapi_unflatten( data ):
    if 'error' in data:
        return data
    output_data = {}
    included_dict = {}
    if 'included' in data:
        for datum in data['included']:
            if datum['type'] not in included_dict:
                included_dict[datum['type']] = {}
            included_dict[datum['type']][datum['id']] = datum
    if not isinstance(data['data'],list):
        data['data'] = [ data['data'] ]
    for datum in data['data']:
        datum = unflatten( datum, included_dict)
        if datum['type'] not in output_data:
            output_data[datum['type']] = {}
        output_data[datum['type']][datum['id']] = datum
    return output_data

def unflatten( data, data_included ):
    if 'relationships' in data:
        for relationship_name, relationship_data in data['relationships'].items():
            if isinstance(relationship_data['data'], dict):
                relationship_data['data'] = [ relationship_data['data'] ]
            for datum in relationship_data['data']:
                if datum['type'] not in data_included:
                    continue
                else:
                    if relationship_name not in data:
                        data[relationship_name] = []
                    r_type = datum['type']
                    r_id = datum['id']            
                    extra_data = {}
                    if 'relationships' in data_included[r_type][r_id]:
                        extra_data = unflatten(data_included[r_type][r_id],data_included)
                    if 'meta' in data_included[r_type][r_id]:
                        if 'url' in data_included[r_type][r_id]['meta']:
                            extra_data['url'] = data_included[r_type][r_id]['meta']['url']
                    if 'attributes' in data_included[r_type][r_id]:
                        data[relationship_name].append({**data_included[r_type][r_id]['attributes'], **{ 'id' : r_id,}, **extra_data})
                    else:
                        data[relationship_name].append({**{ 'id' : r_id}, **extra_data})
                        #'type' : r_type,
        del data['relationships']
    if 'links' in data:
        del data['links']
    if 'attributes' in data:
        for key,value in data['attributes'].items():
            data[key] = value
        del data['attributes']
    if 'meta' in data:
        if 'url' in data['meta']:
            data['url'] = data['meta']['url']
        del data['meta']
    return data

def date_to_z(date : str = None):
    if date is None:
        raise ValueError
    return pyrfc3339.generate(pyrfc3339.parse(date),utc=True)

def fixup_fimfic_urls( url ):
    broken_url = list(urlparse(url))
    if broken_url[2].startswith('/api/v2'):
        broken_url[2] = broken_url[2][7:]
        url = urlunparse(broken_url)
    if url.startswith('//'):
        return 'https:' + url
    if not url.startswith('https://www.fimfiction.net'.lower()):
        return 'https://www.fimfiction.net' + url
    return url