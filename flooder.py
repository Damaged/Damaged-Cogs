import discord
import asyncio
import logging
from discord.ext import commands
from .utils.dataIO import dataIO
from .utils import checks
import os
from __main__ import send_cmd_help

log = logging.getLogger("red.flooder")
log.setLevel(logging.ERROR)

data_format = {
    'enabled' : False,
    'flood_channel' : False
}

class Flooder:
    def __init__(self, bot):
        self.bot = bot
        self.data = dataIO.load_json("data/flooder/data.json")

    @commands.group(name='flood',no_pm=True, pass_context=True)
    async def _flood(self, ctx):
        """Configure flooding"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @_flood.command(name='test',no_pm=True, pass_context=True)
    async def _test(self, ctx):
        await self.listener(ctx.message)

    @_flood.command(name='toggle',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _toggle(self, ctx):
        """Turns flooding on/off"""
        server = ctx.message.server
        if server.id not in self.data['config']:
            await self.bot.say("Flood channel has not been defined!")
            return
        self.data['config'][server.id]['enabled'] = not self.data['config'][server.id]['enabled']
        dataIO.save_json("data/flooder/data.json", self.data)
        if self.data['config'][server.id]['enabled']:
            await self.bot.say("Enabled server flooding.")
        else:
            await self.bot.say("Disabled server flooding.")
    
    @_flood.command(name='channel',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _channel(self, ctx, channel : discord.Channel = None ):
        """Sets flooding channel"""
        server = ctx.message.server
        if channel is None or self.bot.get_channel(channel.id) is None:
            await self.bot.say("Invalid channel or I can't see that channel!")
            return
        if server.id not in self.data['config']:
            self.data['config'][server.id] = data_format
        self.data['config'][server.id]['flood_channel'] = channel.id
        dataIO.save_json("data/flooder/data.json", self.data)
        await self.bot.say("Flooding channel set to: {0.mention}".format(channel))

    @_flood.command(name='hide',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _hide_channel(self, ctx, channel : discord.Channel = None ):
        """Toggles a channel hidden from flooding"""
        server = ctx.message.server
        if channel is None or self.bot.get_channel(channel.id) is None:
            await self.bot.say("Invalid channel or I can't see that channel!")
            return
        if server.id not in self.data['config']:
            await self.bot.say("Flood channel has not been defined!")
            return
        if 'hidden' not in self.data['config'][server.id]:
            self.data['config'][server.id]['hidden'] = []
        if channel.id in self.data['config'][server.id]['hidden']:
            self.data['config'][server.id]['hidden'].remove(channel.id)
            dataIO.save_json("data/flooder/data.json", self.data)
            await self.bot.say("Now Flooding {channel.mention} again.".format(channel=channel))
            return
        else:
            self.data['config'][server.id]['hidden'].append(channel.id)
            dataIO.save_json("data/flooder/data.json", self.data)
            await self.bot.say("Hiding {channel.mention} from Flood.".format(channel=channel))
            return
        
    async def listener3(self, message):
        """Deleted a message"""
        server = message.server
        channel = message.channel
        if not self.is_valid(message):
            return
        embed = self.make_embed(message)
        embed.title = "Deleted"
        await self.bot.send_message(self.bot.get_channel(self.data['config'][server.id]['flood_channel']),'Channel: {channel.mention} User: {user.display_name}'.format(channel=channel, user=message.author), embed=embed)

    async def listener2(self, old_message, message):
        """Edited a message"""
        server = message.server
        channel = message.channel
        if not self.is_valid(message):
            return
        embed = self.make_embed(message)
        embed.title = "Edited"
        await self.bot.send_message(self.bot.get_channel(self.data['config'][server.id]['flood_channel']),'Channel: {channel.mention} User: {user.display_name}'.format(channel=channel, user=message.author), embed=embed)
    
    async def listener(self, message):
        """Sent a message"""
        server = message.server
        channel = message.channel
        if not self.is_valid(message):
            return
        embed = self.make_embed(message)
        embed.title = "Sent"
        await self.bot.send_message(self.bot.get_channel(self.data['config'][server.id]['flood_channel']),'Channel: {channel.mention} User: {user.display_name}'.format(channel=channel, user=message.author), embed=embed)
           
    def is_valid(self, message):
        server = message.server
        if message.author.id == self.bot.user.id:
            log.debug("Mummy said I shouldn't talk to myself!")
            return False
        if message.author.bot:
            log.debug("I don't listen to bots!")
            return False
        if callable(self.bot.command_prefix):
            prefixes = self.bot.command_prefix(self.bot, message)
        else:
            prefixes = self.bot.command_prefix
        for p in prefixes:
            if message.content.startswith(p):
                return False
        if server.id not in self.data['config']:
            return False
        if 'hidden' in self.data['config'][server.id]:
            if message.channel.id in self.data['config'][server.id]['hidden']:
                return False
        try:
            if message.channel.id == self.data['config'][server.id]['flood_channel']:
                log.debug("This is the flood channel!")
                return False
            if not self.data['config'][server.id]['enabled']:
                log.debug("Flooding disabled!")
                return False
        except:
            return False
        return True

    def make_embed(self, message):
        log.debug( message.content )
        embed = discord.Embed(color=message.author.color.value)
        content = message.content
        if len(content) > 1700:
            log.debug( content )
            content = content[:1700]
            log.debug( content )
            content = content + '[...]'
            log.debug( content )
        embed.description = content
        log.debug('desc= ' + embed.description)
        return embed  
        
def check_folder():
    if not os.path.exists("data/flooder"):
        print("Creating data/flooder folder...")
        os.makedirs("data/flooder")

def check_files():
    data_default = {'config': {}}
    if not dataIO.is_valid_json("data/flooder/data.json"):
        print("Creating default data.json...")
        dataIO.save_json("data/flooder/data.json", data_default)
        
def setup(bot):
    check_folder()
    check_files()
    n = Flooder(bot)
    bot.add_listener(n.listener, 'on_message')
    bot.add_listener(n.listener2, 'on_message_edit')
    bot.add_listener(n.listener3, 'on_message_delete')
    bot.add_cog(n)