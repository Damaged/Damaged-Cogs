import discord
from discord.ext import commands
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
import os
import re
import asyncio
import aiohttp
import logging

log = logging.getLogger("red.freegame")
log.setLevel(logging.ERROR)
    
class FreeGame:
    def __init__(self, bot):
        self.bot = bot
        self.data = fileIO("data/freegame/data.json", "load")
        self.sub_task = bot.loop.create_task(self.freegame_service())

    @commands.group(name='freegame',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _feedsub(self, ctx):
        """Manages freegame"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)
            
    @_feedsub.command(name='refresh', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes)
        
        [p]freegame refresh <time>"""
        server = ctx.message.server
        if refresh_time < 1:
            await self.bot.say("You can only enter a time 1 hour and over.")
            return
        self.data['refresh'] = refresh_time
        await self.bot.say( "Refresh time set to %i hours." % (refresh_time))
        fileIO("data/freegame/data.json", "save", self.data)

    @_feedsub.command(name='add', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _add(self, ctx, channel : discord.Channel = None):
        """Add a channel to spam
        
        [p]freegame add #<channel>"""
        server = ctx.message.server
        if channel is None:
            await send_cmd_help(ctx)
            return
        if server.id not in self.data['subs']:
            self.data['subs'][server.id] = {}
        if channel.id in self.data['subs'][server.id].keys():
            return await self.bot.say('That feed is already subscribed!')
        self.data['subs'][server.id][channel.id] = True
        fileIO("data/freegame/data.json", "save", self.data)
        await self.bot.say("Added {channel}.".format(channel = channel.mention))
        await self.spam_looper(force_channel = channel)
        
    @_feedsub.command(name='del', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _del(self, ctx, channel : discord.Channel = None):
        """Del a channel
        
        [p]freegame del #<channel> feed"""
        server = ctx.message.server
        if channel is None:
            await send_cmd_help(ctx)
            return
        if server.id not in self.data['subs']:
            return await self.bot.say('No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.say('Not subbed on this channel!')
        del self.data['subs'][server.id][channel.id]
        fileIO("data/freegame/data.json", "save", self.data)
        await self.bot.say("Done.")
        
    @_feedsub.command(name='test', pass_context=True)
    async def _test(self, ctx):
        self.data = fileIO("data/freegame/data.json", "load")
        #self.data['last_game'] = ""
        #fileIO("data/freegame/data.json", "save", self.data)
        #await self.spam_looper()        
        
    async def freegame_service(self):
        """Trigger loop"""
        #return #comment out when not testing
        await self.bot.wait_until_ready()
        try:
            log.debug( "Entering try block, starting loop." )
            while True:
                try:
                    await self.spam_looper()
                except:
                    pass
                log.info( "freegame: Task done, sleeping." )
                await asyncio.sleep(int(self.data['refresh']) * 60 * 60)
                log.info("freegame service tick.")
        except asyncio.CancelledError:
            pass
        
    def __unload(self):
        self.sub_task.cancel()
        pass
        
    async def spam_looper(self, force_channel : discord.Channel = None):
        if not self.data:
            log.debug( "Program data empty? Bail out!" )
            return None
        async with  aiohttp.get('https://www.humblebundle.com') as response:
            website = await response.text()
        log.debug(website)
        freegame = re.findall(r'href="(https?://(?:www\.)humblebundle\.com/store/\S+)\?\S+campaign=free\S+"', website)
        if not freegame:
            if force_channel is not None:
                asyncio.ensure_future(self.bot.send_message( force_channel, "There are no free games at this time."))
            return None
        freegame = freegame[0]
        log.debug("I found a game: " + freegame)
        if freegame == self.data['last_game'] and force_channel is None:
            log.debug("But I already spammed it :(")
            return None
        elif freegame != self.data['last_game']:
            force_channel = None
        self.data['last_game'] = freegame
        fileIO("data/freegame/data.json", "save", self.data)
        for server in self.data['subs'].keys():
            server = self.bot.get_server(server)
            if server is None:
                continue
            log.debug('Found server: ' + server.name)
            for channel, data in self.data['subs'][server.id].items():
                channel = self.bot.get_channel(channel)
                if channel is None:
                    continue
                if force_channel is not None:
                    if force_channel.id != channel.id:
                        continue
                log.debug( "Looking for a channel to spam, found: {}".format(channel.name) )
                asyncio.ensure_future(self.bot.send_message( channel, "I found a free game on Humble Bundle: {}".format(freegame)))
                        
def check_folder():
    if not os.path.exists("data/freegame"):
        print("Creating data/freegame folder...")
        os.makedirs("data/freegame")

def check_files():
    data_default = {"refresh":12, 'subs' : {}}
    if not fileIO("data/freegame/data.json", "check") or not fileIO("data/freegame/data.json", "load"):
        print("Creating default freegame data.json...")
        fileIO("data/freegame/data.json", "save", data_default)
    else:
        data = fileIO("data/freegame/data.json", "load")

def setup(bot):
    check_folder()
    check_files()
    bot.add_cog(FreeGame(bot))