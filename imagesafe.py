import discord
from redbot.core import Config, checks, commands, data_manager
from redbot.core.utils.menus import menu, close_menu
from urllib.parse import urlparse, quote_plus, unquote, quote
from hashlib import md5, sha512
import os
import re
import logging
import asyncio
import aiohttp
import requests
import json
from datetime import datetime, timedelta
import pyrfc3339 
import pytz
import html2text
from bs4 import BeautifulSoup
from pyembed.core import PyEmbed
from functools import partial
import math
from ao3 import AO3
import sys
from PIL import Image, ImageStat
import imagehash
import io
import tempfile
from multiprocessing import Pool
#from .utils.fanfiction_net import Story
import zlib
import cv2
import numpy as np
from .derpibooru_archive import DerpibooruArchive
from .data_storage import DataStorage
#from .menu import Menu
import opengraph_py3
from yarl import URL
import copy
user_agent = 'ENTER A SANE USER AGENT'

# derpi_verify_image_name = {
#     'thumb_tiny' : True,
#     'thumb_small' : True,
#     'thumb' : True,
#     'small' : True,
#     'medium' : True,
#     'large' : True,
#     'tall' : True,
#     'full' : True,
#     'webm' : True,
#     'mp4' : True
# }

fimfic_tags = {
    'my little pony: friendship is magic' : 'MLP',
    'mlp: fim' : 'MLP',
    'my little pony: equestria girls' : 'EqG',
    'my little pony: the movie' : 'MLP:Movie',
    '2nd person' : '2nd',
    'adventure' : 'Adv',
    'alternate universe' : 'Alt.U',
    'alt. universe' : 'Alt.U',
    'anthro' : 'An',
    'comedy' : 'Com',
    'crossover' : 'X\'ver',
    'dark' : 'Dark',
    'drama' : 'Drama',
    'equestria girls' : 'EqG',
    'horror' : 'Hor',
    'human' : 'Human',
    'mystery' : 'Myst',
    'random' : 'Rand',
    'romance' : 'Rom',
    'sad' : 'Sad',
    'science fiction' : 'Sci-Fi',
    'slice of life' : 'SoL',
    'thriller' : 'Thr',
    'tragedy' : 'Tra',
    'dragon ball z' : 'DBZ'
}

number_convertor = [
    '0⃣',
    '1⃣', 
    '2⃣', 
    '3⃣', 
    '4⃣', 
    '5⃣',
    '6⃣',
    '7⃣',
    '8⃣',
    '9⃣',
    '🔟'
]
    
                    
cache_duration = {
    'derpi' : 5 , 
    'derpihash' : 5 , 
    'e621' : 5 , 
    'deviant' : 1024 , 
    'tumblr' : 5, 
    'fimfic' : 5, 
    'fimfic_story' : 5, 
    'fimfic_user' : 5, 
    'fimfic_blog' : 5, 
    'fimfic_group' : 5,
    'furaffinity' : 5,
    'fasearch' : 5,
    'inkbunny_url' : 5,
    'inkbunny_sub' : 5,
    'inkbunny_md5' : 5,
    'ao3' : 5,
    'fanficnet' : 5,
    'image_data' : 120,
    'saucenao' : 1024,
    'derpi_artist' : 1024
}



log = logging.getLogger("red.imagesafe")
log.setLevel(logging.ERROR)

class FoalconException(Exception):
    """This bot will absolutely not allow this"""
    def __init__(self, message : str = None):
        if message:
            log.info('FOALCON POSTED: ' + message)
        else:
            log.error('FOALCON POSTED')

class DiscordLog:
    def __init__(self, urgency : str = 'Error', source : str = 'Unknown', time_stat : list = [0.0,0]):
        self.log_message = '{urgency} - {source}:\n'.format(urgency=urgency, source=source)
        self.urgency = urgency
        self.source = source
        self.time = datetime.utcnow().replace(tzinfo=pytz.utc)
        self.time_stat = time_stat
        
    @property
    def log(self):
        time_diff = datetime.utcnow().replace(tzinfo=pytz.utc) - self.time
        num_diff = time_diff.seconds + ( time_diff.microseconds / 1000000 )
        self.log_message += "```Took {:.3f} seconds.\n".format(num_diff)
        self.time_stat[0] += num_diff 
        self.time_stat[1] += 1
        self.log_message += "Avrg {:.3f} seconds.```".format(self.time_stat[0] / self.time_stat[1])
        return self.log_message
    
    @log.setter
    def log(self, message : str = None):
        if message is None:
            raise ValueError
        self.log_message += "{message}\n".format(message=message)
        

class ConfidenceValue:
    """Class to store values based off how confident you are they
    are the best value. Higher values of confidence are better."""
    def __init__(self, data : str = None, confidence : int = -1):
        self.data = {
            'data' : data,
            'confidence' : confidence
        }
       
    def __repr__(self):
        return str(self.data['data'])
        
    @property
    def value(self):
        return self.data['data']
    
    @value.setter
    def value(self, value):
        try:
            data, confidence = value
        except ValueError:
            data = value
            confidence = -1
        confidence = int(confidence)
        log.debug(confidence)
        log.debug(self.data['confidence'])
        log.debug(data)
        if self.data['confidence'] >= confidence:
            log.debug('Didn\'t write')
            return
        self.data = {
            'data' : data,
            'confidence' : confidence
        }
        
    def debug(self):
        return { 'data' : self.data['data'], 'confidence' : self.data['confidence'] }

class ImageEmbed:
    """Stores all the bits and pieces needed to make an imagesafe
    embed, and exposes methods to add data and finally compile
    said embed."""
    def __init__(self):
        self.embed_data = {
            'title' : ConfidenceValue('Untitled'),
            'title_url' : ConfidenceValue(),
            'description' : ConfidenceValue(),
            'artist' : ConfidenceValue(),
            'artist_url' : ConfidenceValue(),
            'artist_image' : ConfidenceValue(),
            'links' : {},
            'image' : ConfidenceValue(),
            'misc' : {},
            'tags' : {},
            'color' : ConfidenceValue('0x000000'),
            'source' : ConfidenceValue(),
            'explicit' : ConfidenceValue(),
            'sources' : []
        }
        self._embed = None
        self.embed_type = None
        self._links = []
        self._last_hash = ''
            
    def shorten_desc(self, desc : str = ''):
        if not desc or '\n' not in desc:
            return desc
        desc = re.sub(r'\*\*(\s|\n)+\*\*','\g<1>',desc)
        desc = re.sub(r'_(\s|\n)+_','\g<1>',desc)
        desc = re.sub(r'\[\]\([^\)]+\)','',desc)
        desc = re.sub(r'\n(?:\s|\n)+\n','\n\n',desc)
        return desc

    @property    
    def hash(self):
        output = ''
        for title, value in self.embed_data.items():
            if title == 'sources':
                continue
            output += str(value)
        output += str(self.embed_type)
        hash_sha512 = sha512()
        hash_sha512.update(output.encode('utf-8'))
        return hash_sha512.hexdigest()

    def __repr__(self):
        output = ''
        for title, value in self.embed_data.items():
            output += ' : ' + str(value)
        output += str(self.embed_type)
        return output

    def set_value(self, title : str, value, confidence : int = -1):
        if title is None:
            raise ValueError
        log.debug(title)
        self.embed_data[title].value = (value, confidence)
        
    def set_title(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        self.set_value('title', datum, confidence)
        
    def set_color(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        self.set_value('color', datum, confidence)
                
    def set_title_url(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise
        self.set_value('title_url', datum, confidence)        

    def set_description(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        if not datum.strip():
            return
        datum = self.shorten_desc(datum)
        if len(datum) > 800:
            return
        self.set_value('description', datum, confidence)        

    def set_long_description(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        datum = self.shorten_desc(datum)
        self.set_value('description', datum, confidence)       
        
    def set_artist(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        self.set_value('artist', datum, confidence)
        
    def set_artist_url(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        self.set_value('artist_url', datum, confidence)       
        
    def set_artist_image(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        self.set_value('artist_image', datum, confidence)   
        
    def add_link(self, title : str = None, url : str = None):
        if title is None or url is None:
            raise ValueError
        if url in self.embed_data['links'].values():
            return
        self.embed_data['links'][title] = url
        self.embed_data['source'] = ConfidenceValue()
        
    def set_source(self, title : str = None, url : str = None,confidence : int = -1):
        if title is None or url is None:
            raise ValueError
        self.set_value('source', json.dumps({ 'name' : title, 'link' : url}), confidence)   
        
    def set_image(self, datum : str = None, confidence : int = -1):
        if datum is None:
            raise ValueError
        self.set_value('image', datum, confidence)  

    def set_explicit(self, explicit : str = 'explicit', confidence : int = -1):
        self.set_value('explicit', explicit, confidence)
    
    @property
    def is_explicit(self):
        if self.embed_data['explicit'].value is None or not self.embed_data['explicit'].value:
            return False
        return self.embed_data['explicit'].value

    @property
    def sources(self):
        return self.embed_data['sources']
    
    @sources.setter
    def sources(self, source : str = ''):
        if not source:
            raise ValueError
        self.embed_data['sources'].append(source)

    def rem_image(self):
        self.embed_data['image'] = ConfidenceValue()
        
    def add_misc(self, title : str = None, datum : str = None):
        if title is None or datum is None:
            raise ValueError
        log.debug('add_misc')
        log.debug(title)
        log.debug(datum)
        if title in self.embed_data['links'].keys():
            return
        self.embed_data['misc'][title] = datum 
    
    @property
    def url(self):
        return self.embed_data['title_url'].value

    @property
    def type(self):
        if self.embed_type is not None:
            return self.embed_type
        else:
            return None
    
    @type.setter
    def type(self, type : str = ''):
        if type == 'image' or type == 'story':
            self.embed_type = type
            return
        raise ValueError('Must provide correct string ("image" or "story")')
            
    def debug(self):
        log.debug('ImageEmbed debug!')
        for key, data in self.embed_data.items():
            if isinstance(data, ConfidenceValue):
                log.debug(key + ':' + str(data.debug()))
            else:
                log.debug(key + ':' + str(data))
    
    def __output(self, rebuild : bool = False):
        self.type = 'image'
        embed = discord.Embed()
        embed.type = 'rich'
        embed.color = int(self.embed_data['color'].value,16)
        if self.embed_data['title_url'].value is None:
            raise ValueError('Missing Title URL')
        embed.url = self.embed_data['title_url'].value
        if self.embed_data['description'].value is not None:
            embed.description = self.embed_data['description'].value
        embed.title = self.embed_data['title'].value
        if self.embed_data['image'].value is not None:
            embed.set_image(url=self.embed_data['image'].value)
        if self.embed_data['artist'].value is not None:
            if self.embed_data['artist_url'].value is None:
                self.embed_data['artist_url'].value = ('', 0)
            if self.embed_data['artist_image'].value is not None:
                embed.set_author(name=self.embed_data['artist'].value, url=self.embed_data['artist_url'].value, icon_url=self.embed_data['artist_image'].value)
            else:
                embed.set_author(name=self.embed_data['artist'].value, url=self.embed_data['artist_url'].value)
        links = []
        for title, datum in self.embed_data['links'].items():
            links.append('[{title}]({datum})'.format(title=title, datum=datum))
        if self.embed_data['source'].value:
            source = json.loads(self.embed_data['source'].value)
            links.append('[{title}]({datum})'.format(title=source['name'], datum=source['link']))
        #if 'Rating' in self.embed_data['misc']:
        #    embed.add_field(name='Rating', value=self.embed_data['misc']['Rating'])
        #    del self.embed_data['misc']['Rating']
        #if 'Status' in self.embed_data['misc']:
        #    embed.add_field(name='Status', value=self.embed_data['misc']['Status'])
        #    del self.embed_data['misc']['Status']
        for title, datum in self.embed_data['misc'].items():
            embed.add_field(name=title, value=datum)
        if links:
            embed.add_field(name='Links',value='\n'.join(links))
        self._embed = embed
        return self._embed
        
    def __output_story(self, rebuild : bool = False):
        #if self._embed is None or rebuild or self.embed_type is None or self.embed_type != 'story':
        self.type = 'story'
        embed = discord.Embed()
        embed.type = 'rich'
        embed.color = int(self.embed_data['color'].value,16)
        if self.embed_data['title_url'].value is None:
            raise ValueError('Missing Title URL')
        embed.url = self.embed_data['title_url'].value
        if self.embed_data['description'].value is not None:
            embed.description = self.embed_data['description'].value
        embed.title = self.embed_data['title'].value
        if self.embed_data['image'].value is not None:
            embed.set_thumbnail(url=self.embed_data['image'].value)
        if self.embed_data['artist'].value is not None:
            if self.embed_data['artist_url'].value is None:
                self.embed_data['artist_url'].value = ('', 0)
            if self.embed_data['artist_image'].value is not None:
                embed.set_author(name=self.embed_data['artist'].value, url=self.embed_data['artist_url'].value, icon_url=self.embed_data['artist_image'].value)
            else:
                embed.set_author(name=self.embed_data['artist'].value, url=self.embed_data['artist_url'].value)
        links = []
        for title, datum in self.embed_data['links'].items():
            links.append('[{title}]({datum})'.format(title=title, datum=datum))
        if self.embed_data['source'].value:
            source = json.loads(self.embed_data['source'].value)
            links.append('[{title}]({datum})'.format(title=source['name'], datum=source['link']))
        if 'Rating' in self.embed_data['misc']:
            embed.add_field(name='Rating', value=self.embed_data['misc']['Rating'])
        if 'Status' in self.embed_data['misc']:
            embed.add_field(name='Status', value=self.embed_data['misc']['Status'])
        for title, datum in self.embed_data['misc'].items():
            if title == 'Rating' or title == 'Status':
                continue
            embed.add_field(name=title, value=datum)
        if links:
            embed.add_field(name='Links',value='\n'.join(links))
        self._embed = embed
        return self._embed
    
    @property
    def embed(self):
        log.debug(self.embed_type)
        if self.embed_type is not None:
            if self.embed_type == 'story':
                log.debug('Making story embed')
                output = self.__output_story()
            else:
                log.debug('Making image embed')
                output = self.__output()
        else:
            log.debug('Making image embed')
            output = self.__output()
        self._last_hash = self.hash
        return output

    @property
    def updated(self):
        return self._last_hash == self.hash

    @property
    def links(self):
        return self._links
    
    @links.setter
    def links(self, link):
        if not link:
            raise ValueError
        self._links.append(link)
        

            
class ImagesafeSettings():
    def __init__(self, embed : ImageEmbed = None, log_data : DiscordLog = None, primary : bool = True, image_data : dict = None, explicit : str = '', skip_direct : bool = False, hide_image : bool = False, enable_fimfic : bool = True, story : bool = False, image : bool = False, primary_image : str = None, mode : str = 'full', target_user : discord.User = None, hash_source : bool = False):
        if embed is None:
            self.embed = ImageEmbed()
        else:
            self.embed = embed
        self.log_data = log_data
        self.primary = primary
        self.image_data = image_data
        self.explicit = explicit
        self.skip_direct = skip_direct
        self.hide_image = hide_image
        self.enable_fimfic = enable_fimfic
        self.image = image
        self.story = story
        self.primary_image = primary_image
        self.mode = mode
        self.target_user = target_user
        self.hash_source = hash_source

    def __repr__(self):
        output = '\nmode: ' + str(self.mode) + '\n'
        output += 'primary: ' + str(self.primary) + '\n'
        output += 'explicit: ' + str(self.explicit) + '\n'
        output += 'skip_direct: ' + str(self.skip_direct) + '\n'
        output += 'hide_image: ' + str(self.hide_image) + '\n'
        output += 'enable_fimfic: ' + str(self.enable_fimfic) + '\n'
        output += 'image: ' + str(self.image) + '\n'
        output += 'story: ' + str(self.story) + '\n'
        output += 'primary sourced image: ' + str(self.primary_image) + '\n'
        output += 'image_data: ' + str(self.image_data) + '\n'
        if self.embed is not None:
            try:
                output += 'sources: ' + str(self.embed.sources) + '\n'
            except:
                pass
            try:
                output += 'urls: ' + str(self.embed.links) + '\n'
            except:
                pass
            try:
                output += 'type: ' + str(self.embed.type) + '\n'
            except:
                pass
        return output

    def __call__(self, embed : ImageEmbed = None, log_data : DiscordLog = None, primary : bool = None, image_data : dict = None, explicit : str = None, skip_direct : bool = None, hide_image : bool = None, enable_fimfic : bool = None, story : bool = None, image : bool = None, primary_image : str = None, mode : str = None, target_user : discord.User = None, hash_source : bool = None):
        output = ImagesafeSettings()
        if embed is not None:
            output.embed = embed
        else:
            output.embed = self.embed
        if log_data is not None:
            output.log_data = log_data
        else:
            output.log_data = self.log_data
        if primary is not None:
            output.primary = primary
        else:
            output.primary = self.primary
        if image_data is not None:
            output.image_data = image_data
        else:
            output.image_data = self.image_data
        if explicit is not None:
            output.explicit = explicit
        else:
            output.explicit = self.explicit
        if skip_direct is not None:
            output.skip_direct = skip_direct
        else:
            output.skip_direct = self.skip_direct
        if hide_image is not None:
            output.hide_image = hide_image
        else:
            output.hide_image = self.hide_image
        if enable_fimfic is not None:
            output.enable_fimfic = enable_fimfic
        else:
            output.enable_fimfic = self.enable_fimfic
        if image is not None:
            self.image = image
        else:
            output.image = self.image
        if story is not None:
            self.story = story
        else:
            output.story = self.story
        if primary_image is not None:
            self.primary_image = primary_image
        else:
            output.primary_image = self.primary_image
        if mode is not None:
            self.mode = mode
        else:
            output.mode = self.mode
        if target_user is not None:
            self.target_user = target_user
        else:
            output.target_user = self.target_user
        if hash_source is not None:
            self.hash_source = hash_source
        else:
            output.hash_source = self.hash_source
        return output



class Imagesafe(commands.Cog):
    """The big cahoona. Imagesafe is a class/cog for Discord to capture
    image URLs, interrogate the source, and build a coherant reply to
    the user that the original user can still remove if they wish. It 
    will create nice and consistent embeds with useful data and, if
    possible, the source of the image. Sources will also be interrogated 
    for further data."""
    def __init__(self, bot):
        """Initializes Imagesafe."""
        self.bot_path = str(data_manager.cog_data_path(cog_instance=self))
        self.full_init = False
        self.archive = None
        self.hash_storage = None
        if bot is not None: #bypass that checks if Imagesafe was called simply to access its embed features
            self.full_init = True
            self.bot = bot
            asyncio.create_task(self.cleanup_history())
            self.archive = DerpibooruArchive(self.bot_path)
            self.hash_storage = DataStorage(self.bot_path)
            self.metrics = {}
        self.config = Config.get_conf(self, identifier=0x596f7574756265537562)
        default_global = {
            "refresh":15,
            "tumblr_api_key":'',
            "fimfic_api_key":{
                'api_key' : '',
                'client_id' : '',
                'client_secret' : ''
            },
            "fa_cookies":{
                '__auc' : '', 
                '__cfdruid' : '', 
                '__qca' : '',
                'a' : '',
                'b' : ''
            },
            "saucenao_api_key":'',
            "inkbunny_userdata":{
                "username":'',
                "password":''
            },
            "spam_channel":None
        }
        default_guild = {
            "mode" : 'off',
            "fimfic" : False,
            "shortcuts" : False
        }
        default_channel = {
            "history" : {}
        }
        self.config.register_global(**default_global)
        self.config.register_channel(**default_channel)
        self.config.register_guild(**default_guild)

        self.bot_path = str(data_manager.cog_data_path(cog_instance=self))
        if not os.path.exists(self.bot_path + "/cache"):
            os.makedirs(self.bot_path + "/cache")
        if os.path.exists(self.bot_path + "/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.INFO)
            log.error('Error Logging On')

        self.session = aiohttp.ClientSession()

        self.cache = { 'derpi' : {}, 'derpi_reverse' : {}, 'derpihash' : {}, 'e621' : {} , 'deviant' : {} , 'tumblr' : {}, 'fimfic' : {}, 'fimfic_story' : {}, 'fimfic_user' : {}, 'fimfic_blog' : {}, 'fimfic_group' : {}, 'furaffinity' : {}, 'fasearch' : {}, 'inkbunny_url' : {}, 'inkbunny_sub' : {}, 'inkbunny_md5' : {}, 'ao3' : {}, 'fanficnet' : {}, 'image_data' : {}, 'saucenao' : {}, 'derpi_artist' : {} } #everypony needs a cache!
        #lots and lots of pre-compiled regex to save on processing speed later
        self.regex_tumblr = re.compile(r'(https?://(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.tumblr\.com\b[-a-zA-Z0-9@:%_\+.~#?&//=]*)', flags=re.IGNORECASE)
        self.regex_e621 = re.compile(r'https?://(?:www\.)?e621\.net/post/show/([0-9]+)(?:[-a-zA-Z0-9@:%_\+.~#?&//=]*)?', flags=re.IGNORECASE)
        self.regex_deviantart = re.compile(r'(https?://(?:www\.)?(?:[-a-zA-Z0-9@:%._\+~#=]{2,256})?deviantart\.(?:com|net)/(?:[-)a-zA-Z0-9@:%._\+~#=]{2,256}/)?art\b(?:[-a-zA-Z0-9@:%_\+.~#?&//=]*))', flags=re.IGNORECASE)
        self.regex_faveme = re.compile(r'(https?://(?:www\.)?fav\.me/(d?\S+))', flags=re.IGNORECASE)
        self.regex_derpibooru = re.compile(r'https?://(?:www\.)?(?:derpibooru|trixiebooru)\.org/(?:images/)?([0-9]+)(\D\S*)?', flags=re.IGNORECASE)
        self.regex_fimficstory = re.compile(r'https?://(?:www\.)?cdn-img\.fimfiction\.net/story/\w{4}-[0-9]*?-([0-9]*?)-(?:tiny|medium|large|full)', flags=re.IGNORECASE)
        self.regex_fimficgroup = re.compile(r'https?://(?:www\.)?cdn-img\.fimfiction\.net/group/\w{4}-[0-9]*?-([0-9]*?)-[0-9]{1,3}', flags=re.IGNORECASE)
        self.regex_fimficuser = re.compile(r'https?://(?:www\.)?cdn-img\.fimfiction\.net/user/\w{4}-[0-9]*?-([0-9]*?)-[0-9]{1,3}', flags=re.IGNORECASE)
        self.regex_fimficurl = re.compile(r'https?://(?:www\.)?fimfiction\.net/story/([0-9]+)(?:/\S+)?', flags=re.IGNORECASE)
        self.regex_fimficurl_chapters = re.compile(r'https?://(?:www\.)?fimfiction\.net/story/([0-9]+)/[0-9]+/\S+', flags=re.IGNORECASE)
        self.regex_fimficblogurl = re.compile(r'https?://(?:www\.)?fimfiction\.net/blog/([0-9]+)(?:/\S+)', flags=re.IGNORECASE)
        self.regex_fimficuserurl = re.compile(r'https?://(?:www\.)?fimfiction\.net/user/([0-9]+)(?:/\S+)', flags=re.IGNORECASE)
        self.regex_fimficgroupurl = re.compile(r'https?://(?:www\.)?fimfiction\.net/group/([0-9]+)(?:/\S+)', flags=re.IGNORECASE)
        self.regex_fafullimage = re.compile(r'https?://(?:www\.)?[a-z]\.facdn\.net/art/([^/]+)/([0-9]+)/[0-9]+\.([^_]+)_\S+', flags=re.IGNORECASE)
        self.regex_fafullimage_partial = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)/([0-9]+)\.([^_]+)_\S+', flags=re.IGNORECASE)
        self.regex_fasmallimage = re.compile(r'https?://(?:www\.)?[a-z]\.facdn\.net/([0-9]+)@[0-9]+-[0-9]+\.\S+', flags=re.IGNORECASE)
        self.regex_faurls = re.compile(r'https?://(?:www\.)?furaffinity\.net/(?:full|view)/([0-9]+)/?', flags=re.IGNORECASE)
        self.regex_derpicdn1 = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)/img/(?:view/)?[0-9]+/[0-9]+/[0-9]+/([0-9]+)(?:(?:/|__)\S+)?\.\S+', flags=re.IGNORECASE)
        self.regex_derpicdn2 = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)(?:cdn\.discordapp\.com|derpicdn\.net))(?:/\S+)?/([0-9]+)(?:__\S+)?\.\S+', flags=re.IGNORECASE)
        self.regex_discordcamo = re.compile(r'https?://(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.discordapp\.net\b(?:[-a-zA-Z0-9@:%_\+.~#?&//=]*)\b((?:(?:ftp|http)[s]*\/|www\.)[^\.]+\.[^ \n]+)(?:\?\S+)?', flags=re.IGNORECASE)
        self.regex_fimficcamo = re.compile(r'https?://(?:www\.)?camo\.fimfiction\.net\b(?:[-a-zA-Z0-9@:%_\+.~#?&//=]*)\b((?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)', flags=re.IGNORECASE)
        self.regex_deviantart_raw1 = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)/[^/]+?by[^/]+?(?:-|_)(d[a-z0-9]+)(?:-pre|-fullview)?\.[0-9a-zA-Z]{1,9}', flags=re.IGNORECASE)
        self.regex_deviantart_raw2 = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)/(d[a-z0-9]+)[^a-z0-9][^/]+\.(?:jpg|jpeg|bmp|png|gif|apng)\S*', flags=re.IGNORECASE)
        self.regex_e621_raw = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)([a-fA-F\d]{32})\.[0-9a-zA-Z]{3,256}', flags=re.IGNORECASE)
        self.regex_tumblr_raw = re.compile(r'(https?://([0-9]{2}\.media|data|s3\.amazonaws\.com/data|a)\.tumblr\.com/([a-fA-F\d]{32}/)?tumblr_.+_.+\.[a-zA-Z0-9]{1,255})', flags=re.IGNORECASE)
        self.regex_inkbunny_page = re.compile(r'https?://(?:www\.)?inkbunny\.net/s/([0-9]+)/?', flags=re.IGNORECASE)
        self.regex_inkbunny_image = re.compile(r'https?://\S+?\.metapix\.net/files/(?:full|screen)/[0-9]{4}/([0-9]+)_\S+_(?:[0-9]{4}_)?\S+', flags=re.IGNORECASE)
        self.regex_archiveofourown = re.compile(r'https?://(?:www\.)?archiveofourown\.org/works/(\d+)(?:/\S+)?', flags=re.IGNORECASE)
        self.regex_equals = re.compile(r'((derpi|story)|e621|da)=((?(2)\d+|[a-z0-9]+))', flags=re.IGNORECASE)
        self.regex_derpiarrows = re.compile(r'>>(\d+)', flags=re.IGNORECASE)
        self.regex_url = re.compile(r'(?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+)', flags=re.IGNORECASE)
        self.rate = {}
        self.inkbunny_sid = ''
        self.time_stat = [ 0.0 , 0 ]

    def __unload(self):
        if self.full_init:
            self.archive.stop_services()
            self.hash_storage.stop_services()
        if self.session:
            self.session.close()
    
    #cache handling functions
    def add_to_cache(self, key, item, type, etag : str = None, last_modified : str = None):
        if etag is not None or last_modified is not None:
            datum = {'data' : item}
            if etag is not None: 
                datum['etag'] = etag
            if last_modified is not None:
                datum['last_modified'] = last_modified
            log.debug(datum)
            hash_md5 = md5()
            hash_md5.update(str(key).encode('utf-8'))             
            log.debug(self.bot_path + "/cache/{key} - {type}.json".format(key=hash_md5.hexdigest(), type=type))
            with open(self.bot_path + '/cache/{key} - {type}.json'.format(key=hash_md5.hexdigest(), type=type), 'w') as outfile:
                json.dump(datum,outfile,indent=4,ensure_ascii=False)
            return
        if type not in self.cache.keys():
            log.error("unable to find cache type:")
            log.error(type)
            raise ValueError("unable to find cache type: " + str(type))
        self.cleanup_cache()
        if key in cache_duration:
            if cache_duration[key] is None:
                duration = None
            else:
                duration = pyrfc3339.generate(datetime.utcnow() + timedelta(seconds = int(cache_duration[key]) * 60 ),accept_naive=True)
        else:
            duration = pyrfc3339.generate(datetime.utcnow() + timedelta(seconds = 120 ),accept_naive=True)
        datum = {
            'data' : item,
            'expires' : duration
        }
        self.cache[type][key] = datum
            
    def get_etag(self,key,type):
        hash_md5 = md5()
        hash_md5.update(str(key).encode('utf-8')) 
        log.debug(self.bot_path + "/cache/{key} - {type}.json".format(key=hash_md5.hexdigest(), type=type))
        if os.path.exists(self.bot_path + "/cache/{key} - {type}.json".format(key=hash_md5.hexdigest(), type=type)):
            log.debug('Found an etag cache')
            with open(self.bot_path + "/cache/{key} - {type}.json".format(key=hash_md5.hexdigest(), type=type)) as json_file:
                data = json.load(json_file)
                log.debug(data)
            return data
        return None
            
    def get_cache(self, key, type):
        if type not in self.cache.keys():
            log.error("unable to find cache type:")
            log.error(type)
            raise ValueError("unable to find cache type: " + str(type))
        self.cleanup_cache()
        if key in self.cache[type].keys():
            log.debug('Cache hit: ' + type)
            log.debug('Key: ' + key)
            return self.cache[type][key]['data']
        else:
            log.debug('Cache miss!')
            return None
    
    def find_emoji(self):
        """Helper function to locate where the "source" emoji
        is"""
        source_emoji = False
        spoiler_emoji = False
        foalcon_emoji = False
        try:
            if self.source_emoji.id:
                source_emoji = True
        except:
            pass
        try:
            if self.spoiler_emoji.id:
                spoiler_emoji = True
        except:
            pass
        try:
            if self.foalcon_emoji.id:
                foalcon_emoji = True
        except:
            pass
        if not source_emoji:
            self.source_emoji = None
            for guild in self.bot.guilds:
                for emoji in guild.emojis:
                    if 'source' in emoji.name:
                        self.source_emoji = emoji
                        break
                if self.source_emoji is not None:
                    break
        if not spoiler_emoji:
            self.spoiler_emoji = None
            for guild in self.bot.guilds:
                for emoji in guild.emojis:
                    if 'spoiler' in emoji.name:
                        self.spoiler_emoji = emoji
                        break
                if self.spoiler_emoji is not None:
                    break
        if not foalcon_emoji:
            self.foalcon_emoji = None
            for guild in self.bot.guilds:
                for emoji in guild.emojis:
                    if 'foalcon' in emoji.name:
                        self.foalcon_emoji = emoji
                        break
                if self.foalcon_emoji is not None:
                    break

    async def on_raw_reaction_add(self, msg):
        try:
            if not msg:
                return
            channel = self.bot.get_channel(msg.channel_id)
            message = await channel.get_message(msg.message_id)
            reaction = None
            for datum in message.reactions:
                if isinstance(datum.emoji, str):
                    if msg.emoji.is_custom_emoji():
                        continue
                    if datum.emoji == msg.emoji.name:
                        reaction = datum
                        break
                else:
                    if msg.emoji.is_unicode_emoji():
                        continue
                    if datum.emoji.id == msg.emoji.id:
                        reaction = datum
                        break
            if reaction is None:
                return
            if msg.guild_id is not None:
                guild = self.bot.get_guild(msg.guild_id)
                member = guild.get_member(msg.user_id)
            else:
                member = await self.bot.get_user_info(msg.user_id)
            await self.reaction(reaction,member)
            return
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)

    @checks.is_owner()
    @commands.command(name='ismetrics')
    async def _metrics(self, ctx):
        log.debug(self.metrics)
        msg = 'Metrics: ```'
        for title, data in self.metrics.items():
            msg += title.capitalize() + ': ' + str(data) + '\n'
        msg += ' ```'
        await ctx.send(msg)
        self.metrics = {}

    @checks.is_owner()
    @commands.command(name='imagesafe')
    async def _test(self, ctx):
        """Test function for imagesafe. Only use if you
        know what you are doing."""
        log.setLevel(logging.DEBUG)
        await ctx.send("I hear you!")
        await self.on_message(ctx.message, command = True)


        # text_re = re.compile('\S+(?:-|_)(d[a-z0-9]+)(?:-pre|-fullview)?\.\S+')
        # result = self.archive.run_query("SELECT id FROM image_data_table WHERE source_url = '' AND file_name REGEXP '\S+(?:-|_)(d[a-z0-9]+)(?:-pre|-fullview)?\.\S+' ORDER BY id")
        # sources = {}
        # for image_id in result:
        #     image_id = image_id[0]
        #     log.debug(image_id)
        #     if int(image_id) <  429870:
        #         continue
        #     data = self.archive[str(image_id)]
        #     if data is None:
        #         continue
        #     if data['source_url']:
        #         continue
        #     pattern = text_re.fullmatch(data['file_name'])
        #     if pattern is None:
        #         continue
        #     code = pattern.group(1)
        #     url = 'http://fav.me/{code}'.format(code=code)
        #     log.debug(url)
        #     output = await self.get_deviant_data(url)
        #     if output is None:
        #         log.info('No deviant data')
        #         continue
        #     try:
        #         if 'fullsize_url' in output:
        #             url = output['fullsize_url']
        #         else:
        #             url = output['url']
        #         image_data = await self.get_image_data(url=url, headers={})
        #     except:
        #         continue
        #     if image_data is None:
        #         log.info('Image not fetchable')
        #         continue
        #     try:
        #         url = 'https:' + data['representations']['full']
        #         image_data2 = await self.get_image_data(url=url, headers={})
        #     except:
        #         continue
        #     if image_data2 is None:
        #         log.info('Image not fetchable')
        #         continue
        #     if not compare_average_colors(image_data['quads'],image_data2['quads']):
        #         log.info('RGB don\'t match')
        #         continue
        #     url = 'http://fav.me/{code}'.format(code=code)
        #     async with self.session.request("GET", url) as resp: 
        #         output = await resp.text()
        #         log.debug(output)
        #         soup = BeautifulSoup(output, 'html.parser')
        #         head_data = soup.find('head')
        #         result = head_data.find('meta', attrs={'property' : 'og:url'}).get('content')
        #     sources[str(data['id'])] = result
        #     await ctx.send('<https://derpibooru.org/{}>'.format(data['id']) + ' sourced to <' + result + '>')

        # with open(self.bot_path + '/sources.json', 'w') as outfile:
        #     json.dump(sources,outfile,indent=4,ensure_ascii=False)



        #embed = discord.Embed()
        #embed.title = '||this should be hidden||'
        #embed.type = 'rich'
        #embed.set_image(url='https://derpicdn.net/img/view/2019/2/4/1952961.png')
        #await self.bot.send_message(ctx.message.channel, content=None, embed=embed)
        #data_array = dataIO.load_json("data/imagesafe/stories_approved.json")
        #output = "date,Everyone,Teen,Mature\r\n"
        #everyone = 0
        #teen = 0
        #mature = 0
        #previous_month_year = data_array[0]['date'].rsplit('-', 1)[0]
        #for datum in data_array:
        #    day = datum['date'].split('-')[-1]
        #    month_year = datum['date'].rsplit('-', 1)[0]
        #    if day == '01':
        #        output += month_year + ',' + str(everyone) + ',' + str(teen) + ',' + str(mature) + '\n'
        #        everyone = 0
        #        teen = 0
        #        mature = 0
        #        previous_month_year = month_year
        #    if 'Everyone' in datum:
        #        everyone += datum['Everyone']
        #    if 'Teen' in datum:
        #        teen += datum['Teen']
        #    if 'Mature' in datum:
        #        mature += datum['Mature']
        #dataIO.save_json("data/imagesafe/stories_approved_output.json", [ output ])
            
        
        #story = Story(url='https://www.fanfiction.net/s/12876021/8/Monkey-D-Harry-and-the-Goblet-of-Fire')
        #await self.bot.say(story.title)
        
        
        #loop = asyncio.get_event_loop()
        #response = loop.run_in_executor(None, partial(PyEmbed().embed, url='https://xkcd.com/341/'))
        #output await response
        #log.debug( output )
        
        #loop = asyncio.get_event_loop()
        #response = loop.run_in_executor(None, partial(requests.request,"GET",'https://derpicdn.net/img/view/2019/1/1/1923918__safe_artist-colon-andypriceart_glitter+drops_tempest+shadow_idw_spoiler-colon-comic68_faic.jpeg'))
        #website = await response
        #if website.status_code == 200:
        #    downloaded = 0
        #    filesize = int(website.headers['content-length'])
        #    buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
        #    for chunk in website.iter_content():
        #        downloaded += len(chunk)
        #        buffer.write(chunk)
        #    buffer.seek(0)
        #    img = Image.open(io.BytesIO(buffer.read()))
        #    width, height = img.size
        #    log.debug('Width: ' + str(width) + ' Height: ' + str(height))
        #    rgb_img = img.convert('RGB')
        #    stats = ImageStat.Stat(rgb_img)
        #    log.debug(stats.mean)
        #    log.debug(list(get_average_color({'x':0,'y':0}, {'x':rgb_img.width,'y':rgb_img.height}, rgb_img)))
        #    
        # start_time = datetime.utcnow()
        # async with aiohttp.ClientSession() as session:
        #     async with session.get('https://derpicdn.net/img/view/2019/1/26/1945331.png') as resp:
        #         if resp.status == 200:

        # # response = loop.run_in_executor(None, partial(requests.request,"GET",'https://derpicdn.net/img/view/2019/1/26/1945331.png', stream=True))
        # # website = await response
        # # if website.status_code == 200:
        #             downloaded = 0
        #             #filesize = int(resp.headers['content-length'])
        #             buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
        #             async for chunk in resp.content.iter_chunked(1024000):
        #                 downloaded += len(chunk)
        #                 buffer.write(chunk)
        #             buffer.seek(0)
        #             end_time = datetime.utcnow()
        #             log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        #             log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        #             diff_time = end_time - start_time
        #             log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])

        #             start_time = datetime.utcnow()
        #             image = np.asarray(bytearray(buffer.read()), dtype="uint8")
        #             s = Simple2DAnalyser(image)
        #             log.debug(s.nw)
        #             log.debug(s.ne)
        #             log.debug(s.sw)
        #             log.debug(s.se)
        #             end_time = datetime.utcnow()
        #             log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        #             log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        #             diff_time = end_time - start_time
        #             log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])

        #             buffer.seek(0)
        #             start_time = datetime.utcnow()
        #             img = Image.open(io.BytesIO(buffer.read()))
        #             width, height = img.size
        #             log.debug('Width: ' + str(width) + ' Height: ' + str(height))
        #             rgb_img = img.convert('RGB')
        #             keyvalue = []
        #             log.debug(get_average_color({'x':0,'y':0}, {'x':int(width/2),'y':int(height/2)}, rgb_img))
        #             log.debug(get_average_color({'x':int(width/2) + 1,'y':0}, {'x':width,'y':int(height/2)}, rgb_img))
        #             log.debug(get_average_color({'x':0,'y':int(height/2)+1}, {'x':int(width/2),'y':height}, rgb_img))
        #             log.debug(get_average_color({'x':int(width/2) + 1,'y':int(height/2)+1}, {'x':width,'y':height}, rgb_img))

        #             end_time = datetime.utcnow()
        #             log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        #             log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        #             diff_time = end_time - start_time
        #             log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])

        # await ctx.send('Done!')


        #    keyvalue2 = keyvalue
        #match = True
        #for blocks in range(0,3):
        #    for RGB in range(0,2):
        #        if abs(keyvalue1[blocks][RGB] - keyvalue2[blocks][RGB]) > 2:
        #            log.debug('DON\'T MATCH!')
        #            await self.bot.say('They don\'t match!')
        #            match = False
        #            break
        #    if not match:
        #        break
        #if match:
        #    log.debug('Files match')
        #    await self.bot.say('They match!')
        #loop = asyncio.get_event_loop()
        #hash_sha512 = sha512()
        #url = 'https://derpicdn.net/img/2018/12/15/1909481/tall.jpeg'
        #response = loop.run_in_executor(None, partial(requests.request,"GET", url))
        #website = await response
        #if website.status_code != requests.codes.ok:
        #    return None
        #hash_sha512.update(website.content)
        #log.debug(hash_sha512.hexdigest())
        
        if os.path.exists(self.bot_path + "/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.ERROR)
            log.error('Error Logging On')
            
    @commands.command(name='imagesafecache')
    async def _cache(self, ctx):
        """Test function for imagesafe. Only use if you
        know what you are doing."""
        log.setLevel(logging.DEBUG)
        log.debug(self.cache)
        if os.path.exists(self.bot_path + "/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.ERROR)
            log.error('Error Logging On')

    @commands.command(name='isfimfickey')
    @checks.is_owner()
    async def _fimfic_key(self, ctx):        
        """Sets the cog's API key (must be obtained from fimfic)

        [p]isfimfickey"""
        await ctx.author.send("Please enter your client_id.")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        client_id = message.content
        await ctx.author.send("Please enter your client_secret.")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        client_secret = message.content
        website = await get_key(client_id,client_secret)
        if 'errors' in website:
            return await ctx.author.send("Key is invalid!")
        async with self.config.fimfic_api_key() as fimfic_api_key:
            fimfic_api_key['api_key'] = website['access_token']
            fimfic_api_key['client_id'] = client_id
            fimfic_api_key['client_secret'] = client_secret
        await ctx.author.send("Key is set!")

    @commands.command(name='isfacookies')
    @checks.is_owner()
    async def _fa_cookies(self, ctx):        
        """Sets the cog's FurAffinity cookies (must be obtained from a browser that is logged in.)

        [p]isfacookies"""
        await ctx.author.send("Please enter the __auc cookie")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        cookie__auc = message.content
        await ctx.author.send("Please enter the __cfdruid cookie")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        cookie__cfdruid = message.content
        await ctx.author.send("Please enter the __qca cookie")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        cookie__qca = message.content
        await ctx.author.send("Please enter the a cookie")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        cookie_a = message.content
        await ctx.author.send("Please enter the b cookie")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        cookie_b = message.content
        async with self.config.fa_cookies() as fa_cookies:
            fa_cookies['__auc'] = cookie__auc
            fa_cookies['__cfdruid'] = cookie__cfdruid
            fa_cookies['__qca'] = cookie__qca
            fa_cookies['a'] = cookie_a
            fa_cookies['b'] = cookie_b            
        await ctx.author.send("Cookies are set!")

    @commands.command(name='isinkbunny')
    @checks.is_owner()
    async def _inkbunny_logon(self, ctx):        
        """Sets the cog's InkBunny logon username + password

        [p]isinkbunny"""
        await ctx.author.send("Please enter the `username`")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        username = message.content
        await ctx.author.send("Please enter the `password`")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        password = message.content
        data = { 
            'username' : username, 
            'password' : password
        }
        async with self.config.inkbunny_userdata() as inkbunny_userdata:
            inkbunny_userdata['username'] = username
            inkbunny_userdata['password'] = password
        await ctx.author.send("Login is set!")

    @commands.command(name='istumblrkey')
    @checks.is_owner()
    async def _tumblr_key(self, ctx):        
        """Sets the cog's API key (must be obtained from Tumblr)

        [p]istumblrkey"""
        await ctx.author.send("Please enter your tumblr Consumer Key.")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        client_id = message.content
        await self.config.tumblr_api_key.set(client_id)
        await ctx.author.send("Key is set!")

    @commands.command(name='issaucenao')
    @checks.is_owner()
    async def _saucenao_key(self, ctx):        
        """Sets the cog's API key (must be obtained from Saucenao)"""
        await ctx.author.send("Please enter your Saucenao Key.")
        try:
            message = await self.bot.wait_for('message', timeout=60, check=lambda m: m.author.id==ctx.author.id and isinstance(m.channel,discord.DMChannel))
        except asyncio.TimeoutError:
            return await ctx.author.send("Aborted.")
        client_id = message.content
        await self.config.saucenao_api_key.set(client_id)
        await ctx.author.send("Key is set!")

    @commands.command(name='imagesafemode')
    @checks.is_owner()
    async def _mode(self, ctx):        
        """Toggles between full (requires comment delete priv), basic mode, minimalist mode, and off

        [p]imagesafemode"""
        mode = await self.config.guild(ctx.guild).mode()
        embed = discord.Embed()
        embed.title = 'Mode Menu'
        embed.description = 'Current mode: {}'.format(mode)
        embed.add_field(name='1⃣', value='Off', inline=False)
        embed.add_field(name='2⃣', value='Minimalist', inline=False)
        embed.add_field(name='3⃣', value='Basic', inline=False)
        embed.add_field(name='4⃣', value='Full', inline=False)
        embed.add_field(name='0⃣', value='Abort', inline=False)

        


        await menu(ctx, [embed], {
            '0⃣' : self.set_mode,
            '1⃣' : self.set_mode,
            '2⃣' : self.set_mode,
            '3⃣' : self.set_mode,
            '4⃣' : self.set_mode
        },
        None, 0, 300)





        return
        # try:
        #     mode = await self.config.guild(ctx.guild).mode()
        #     config_dict = { 
        #         'Off' : self.set_mode(ctx.guild, 'off'),
        #         'Minimalist' : self.set_mode(ctx.guild, 'mini'),
        #         'Basic' : self.set_mode(ctx.guild, 'basic'),
        #         'Full' : self.set_mode(ctx.guild, 'full'),
        #         'title' : 'This is the title of the menu',
        #         'description' : 'Current mode: {}'.format(mode),
        #     }
        #     menu = Menu(ctx, config = config_dict, user = ctx.message.author, bot = self.bot, log = log, timeout = 60)
        #     while not menu.done:
        #         await asyncio.sleep(1)
        #     mode = await self.config.guild(ctx.guild).mode()
        #     await ctx.channel.send("Mode is now: {}".format(mode))
        # except:
        #     e = sys.exc_info()[0]
        #     log.error( "Error: %s" % e )
        #     log.error( sys.exc_info())
        #     log.exception(e)
        #     raise   
        # guild_mode = await self.config.guild(ctx.guild).mode()
        # if guild_mode == 'off':
        #     self.find_emoji()
        #     if self.source_emoji:
        #         guild_mode = 'mini'
        #         mode = "Minimalist"
        #     else:
        #         await ctx.channel.send('No "source" emoji found!')
        #         guild_mode = 'basic'
        #         mode = "Basic"
        # elif guild_mode == 'mini':
        #     guild_mode = 'basic'
        #     mode = "Basic"
        # elif guild_mode == 'basic':
        #     guild_mode = 'full'
        #     mode = "Full"
        # else:
        #     guild_mode = 'off'
        #     mode = "Off"
        # await self.config.guild(ctx.guild).mode.set(guild_mode)
        # await ctx.channel.send("Mode is now: {}".format(mode))


    async def set_mode(self, ctx, pages, controls, message, page, timeout, emoji):
        case_table = {
            '0⃣' : '',
            '1⃣' : 'off',
            '2⃣' : 'mini',
            '3⃣' : 'basic',
            '4⃣' : 'full'
        }
        result = case_table.get(emoji)
        if result:
            await self.config.guild(ctx.guild).mode.set(result)
            mode = await self.config.guild(ctx.guild).mode()
            await ctx.channel.send("Mode is now: {}".format(mode))        
        await close_menu(ctx,pages,controls,message,page,timeout,emoji)

    #async def set_mode(self, guild, guild_mode):
    #    await self.config.guild(guild).mode.set(guild_mode)
    
    @commands.command(name='imagesafelog')
    @checks.is_owner()
    @commands.guild_only()
    async def _channel(self, ctx, spam_channel : discord.TextChannel):        
        """Set the log channel for the cog
        
        [p]fimficsub log <channel name>"""
        if not spam_channel:
            await self.config.spam_channel.set(None)
            return await self.bot.say( "Log channel cleared." )
        await self.config.spam_channel.set(spam_channel.id)
        await ctx.channel.send("Channel to spam logs set to {}.".format(spam_channel.mention))
    
    async def log_to_channel(self, message : str = None):
        try:
            if message is None:
                return
            channel = self.bot.get_channel(await self.config.spam_channel())
            if channel is None:
                return
            await channel.send(message)
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            raise    

    @commands.command(name='imagesafefimfic')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def _fimfic(self, ctx):        
        """Toggles fimfic story URL catching on and off

        [p]imagesafefimfic"""
        await self.config.guild(ctx.guild).fimfic.set(not await self.config.guild(ctx.guild).fimfic())
        await ctx.send("Fimfic URL capturing is now: {}".format(await self.config.guild(ctx.guild).fimfic()))
                
    @commands.command(name='imagesafeshortcuts')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def _shortcuts(self, ctx):        
        """Toggles shortcuts eg derpi=12345

        [p]imagesafeshortcuts"""
        await self.config.guild(ctx.guild).shortcuts.set(not await self.config.guild(ctx.guild).shortcuts())
        await ctx.send("Shortcuts are now: {}".format(await self.config.guild(ctx.guild).shortcuts()))
    
    async def reaction(self, reaction,member):
        try:
            """Triggered function to grab interactions from users"""
            orig_message_id = int(reaction.message.id)
            log.debug(reaction.emoji)
            if member.bot:
                log.debug("First abort")
                return
            if not reaction.custom_emoji:
                if reaction.emoji != "🚫" and reaction.emoji != "📔" and reaction.emoji != "🖼":
                    log.debug("Second abort")
                    return
            else:
                log.debug("Got emoji: " + reaction.emoji.name)
                emoji_list = [ 'source' ]
                found_emoji = False
                for emoji_name in emoji_list:
                    if emoji_name in reaction.emoji.name:
                        found_emoji = True
                if not found_emoji:
                    log.debug("Second abort pt2")
                    return
                if 'source' in reaction.emoji.name:
                    asyncio.ensure_future(self.on_message(reaction.message, target_user = member))
                    return
            if not reaction.message.author.bot:
                log.debug("Third abort")
                return
            if reaction.message.author.id == member.id:
                if reaction.emoji == "📔":
                    asyncio.ensure_future(self.on_message(reaction.message, story=True))
                    return
                if reaction.emoji == "🖼":
                    asyncio.ensure_future(self.on_message(reaction.message, image=True))
                    return
            else:
                if reaction.emoji == "📔":
                    asyncio.ensure_future(self.on_message(reaction.message, target_user = member, story=True))
                    return
                if reaction.emoji == "🖼":
                    asyncio.ensure_future(self.on_message(reaction.message, target_user = member, image=True))
                    return
            message_list = []
            if isinstance(reaction.message.channel, discord.TextChannel):
                async with self.config.channel(reaction.message.channel).history() as history:
                    #log.debug(history)
                    if str(reaction.message.id) not in history.keys():
                        log.debug("Sixth abort")
                        return
                    if not history[str(reaction.message.id)]:
                        log.debug("Seventh abort")
                        return
                    data = history[str(reaction.message.id)]
                if not reaction.message.channel.permissions_for(member).manage_messages and int(member.id) != int(data['user']):
                    log.debug("Eighth abort")
                    return
                if 'extra_messages' in data:
                    for extra_messages in data['extra_messages']:
                        message_list.append(await reaction.message.channel.get_message(extra_messages))
            message_list.append(reaction.message)
            if isinstance(reaction.message.channel, discord.TextChannel):
                await reaction.message.channel.delete_messages(message_list)
            else:
                for message in message_list:
                    await message.delete()
            async with self.config.channel(reaction.message.channel).history() as history:
                del history[str(orig_message_id)]
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            raise    

    async def cleanup_history(self):
        """Does what's on the label. Sorts through the
        history of posted messages and cleans out old ones
        (mostly since we can't react to those anyway
        GG Discord API)"""
        cleanup_datetime = pyrfc3339.generate(datetime.utcnow() - timedelta(days = 30),accept_naive=True)

        all_channel_data = await self.config.all_channels()
        for channel_id in all_channel_data.keys():
            channel = self.bot.get_channel(int(channel_id))
            async with self.config.channel(channel).history() as history:
                for message_id,message_data in dict(history).items():
                    if message_data['date'] < cleanup_datetime:
                        del history[message_id]
    
    def cleanup_cache(self):
        """Cache cleanup."""
        #log.debug(self.cache)
        cleanup_datetime = pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
        for type in list(self.cache.keys()):
            for item in list(self.cache[type].keys()):
                expiery = self.cache[type][item]['expires']
                if expiery is None:
                    continue
                if expiery < cleanup_datetime:
                    del self.cache[type][item]
    
    async def on_raw_message_edit(self, msg):
        """Did someone change something? Better check it!"""
        try:
            if not msg:
                return
            if 'content' not in msg.data or not msg.data['content']:
                return
            channel = self.bot.get_channel(int(msg.data['channel_id']))
            message = await channel.get_message(msg.message_id)
            await self.on_message(message)
            return
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)

    async def on_message(self, message, target_user : discord.Member = None, command : bool = False, story : bool = False, image : bool = False):
        """Function that tries to abort quickly, but otherwise
        scans messages (and edits) for content that will trigger
        it."""
        if message.guild is None:
            target_user = message.author
        else:
            if await self.config.guild(message.guild).mode() == 'off':
                return
        if message.author.bot and not target_user:
            return
        ctx = await self.bot.get_context(message)
        log.debug(ctx.prefix)
        if not command and ctx.prefix is not None and not target_user:
            if message.content.startswith(ctx.prefix):
                return log.debug('Not on commands!')
        else:
            log.debug('Testing! Wooooo!')
        try:
            channel_log = DiscordLog('INFO', 'Imagesafe', self.time_stat)
            channel_log.log = '```Building Embed```'
            log.debug(message.content)
            words = message.clean_content 
            mode = 'full'   
            skip_direct = False
            if target_user is None:
                if ctx.channel.permissions_for(ctx.author).embed_links:
                    no_show = False
                else:
                    no_show = True
                log.debug('target_user is None')
                mode = await self.config.guild(ctx.guild).mode()
                if '🚫' in message.content and mode == 'full': #added for Belle (and my sanity)
                    mode = 'mini'
                if 'full' not in mode:
                    skip_direct = True
                log.debug('Checking for fimfic')
                if await self.config.guild(ctx.guild).fimfic():
                    log.debug('YAY Fimfic')
                    do_fimfic = True
                else:
                    log.debug('No Fimfic')
                    do_fimfic = False
            else:
                no_show = False
                do_fimfic = True
            link_array = []
            user_said = ''
            if target_user is None and not ctx.channel.is_nsfw():
                log.debug('Channel is blocked from explicit!')
                explicit = 'https://derpicdn.net/img/view/2012/7/13/42206__safe_applejack_earth+pony_female_frown_mare_meta-colon-explicit_needs+more+saturation_official+spoiler+image_open+mouth_pony_shocked_solo_spoile.png'
            else:
                explicit = False
            if words:
                if do_fimfic:
                    for pattern in re.findall(r'(search="([^"]+))"',words, flags=re.IGNORECASE):
                        log.debug(pattern)
                        output = await self.search_stories(pattern[1])
                        if not output:
                            log.debug("No output")
                            result = "Nothing Found"
                        else:
                            log.debug("Got output")
                            log.debug(next(iter(output.values())))
                            result = 'story={input}'.format(input = next(iter(output.values()))['id'])
                        words = words.replace(pattern[0], result, 1)
                if self.regex_equals.findall(words) or self.regex_url.findall(words) or self.regex_derpiarrows.findall(words):
                    processing_words = [
                        { 'process' : True, 'datum' : words}
                    ]
                    done = False
                    i = 0
                    log.debug('start')
                    while not done:
                        done = True
                        log.debug(processing_words)
                        for idx in range(len(processing_words)):
                            log.debug(processing_words[idx])
                            log.debug(done)
                            if processing_words[idx]['process']:
                                log.debug('processing')
                                match1 = re.search(r'```(?:[^`]|`[^`]|``[^`])+```',processing_words[idx]['datum'])
                                match2 = re.search(r'`(?:[^`])+`',processing_words[idx]['datum'])
                                if mode == 'full' and not target_user:
                                    match3 = re.search(r'\|\|(?:[^\|]|\|[^\|])+\|\|',processing_words[idx]['datum'])
                                else:
                                    match3 = None
                                match4 = re.search(r'<(?:[^>\s])+>',processing_words[idx]['datum'])
                                if match1:
                                    go = True
                                    if match2 and match1.start(0) > match2.start(0):
                                        go = False
                                    if match3 and match1.start(0) > match3.start(0):
                                        go = False
                                    if match4 and match1.start(0) > match4.start(0):
                                        go = False
                                    if go:
                                        before = processing_words[0:idx] 
                                        after = processing_words[idx+1:len(processing_words)]
                                        current = [
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][0:match1.start(0)]},
                                            { 'process' : False, 'datum' : processing_words[idx]['datum'][match1.start(0):match1.end(0)]},
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][match1.end(0):len(processing_words[idx]['datum'])]}

                                        ]
                                        log.debug(before)
                                        log.debug(current)
                                        log.debug(after)
                                        processing_words = before + current + after
                                        done = False
                                        break
                                if match2:
                                    go = True
                                    if match1 and match2.start(0) > match1.start(0):
                                        go = False
                                    if match3 and match2.start(0) > match3.start(0):
                                        go = False
                                    if match4 and match2.start(0) > match4.start(0):
                                        go = False
                                    if go:
                                        before = processing_words[0:idx] 
                                        after = processing_words[idx+1:len(processing_words)]
                                        current = [
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][0:match2.start(0)]},
                                            { 'process' : False, 'datum' : processing_words[idx]['datum'][match2.start(0):match2.end(0)]},
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][match2.end(0):len(processing_words[idx]['datum'])]}

                                        ]
                                        log.debug(before)
                                        log.debug(current)
                                        log.debug(after)
                                        processing_words = before + current + after
                                        done = False
                                        break
                                if match3:
                                    go = True
                                    if match1 and match3.start(0) > match1.start(0):
                                        go = False
                                    if match2 and match3.start(0) > match2.start(0):
                                        go = False
                                    if match4 and match3.start(0) > match4.start(0):
                                        go = False
                                    if go:
                                        before = processing_words[0:idx] 
                                        after = processing_words[idx+1:len(processing_words)]
                                        current = [
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][0:match3.start(0)]},
                                            { 'process' : False, 'datum' : processing_words[idx]['datum'][match3.start(0):match3.end(0)]},
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][match3.end(0):len(processing_words[idx]['datum'])]}
                                        ]
                                        log.debug(before)
                                        log.debug(current)
                                        log.debug(after)
                                        processing_words = before + current + after
                                        done = False
                                        break
                                if match4:
                                    go = True
                                    if match1 and match4.start(0) > match1.start(0):
                                        go = False
                                    if match2 and match4.start(0) > match2.start(0):
                                        go = False
                                    if match3 and match4.start(0) > match3.start(0):
                                        go = False
                                    if go:
                                        before = processing_words[0:idx] 
                                        after = processing_words[idx+1:len(processing_words)]
                                        current = [
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][0:match4.start(0)]},
                                            { 'process' : False, 'datum' : processing_words[idx]['datum'][match4.start(0):match4.end(0)]},
                                            { 'process' : True and processing_words[idx]['process'], 'datum' : processing_words[idx]['datum'][match4.end(0):len(processing_words[idx]['datum'])]}
                                        ]
                                        log.debug(before)
                                        log.debug(current)
                                        log.debug(after)
                                        processing_words = before + current + after
                                        done = False
                                        break
                        if done:
                            break
                        if i > 100:
                            raise ValueError
                        i += 1
                    out_words = []
                    for in_words in processing_words:
                        if in_words['datum']:
                            out_words.append(in_words)
                    log.debug(out_words)
                    user_said = ''
                    for word in out_words:
                        pos = 1
                        spaces = copy.deepcopy(word['datum'])
                        processing_datum = word['datum'].lstrip()
                        if  len(spaces) != len(processing_datum):
                            for i in range(len(spaces) - len(processing_datum)):
                                user_said += ' '
                        whitespace = re.split(r'\S+',processing_datum)
                        words_array = re.split(r'\s+',processing_datum)
                        log.debug(words_array)
                        log.debug(whitespace)
                        for datum in words_array:
                            log.debug(datum)
                            if not datum:
                                pos += 1
                                continue
                            settings = ImagesafeSettings(log_data = channel_log, skip_direct = skip_direct, hide_image = no_show, enable_fimfic = do_fimfic, explicit = explicit, mode = mode, target_user = target_user)
                            if not word['process']:
                                settings.mode = 'mini'
                            word_output, embed = await self.extract_embed(copy.deepcopy(datum), settings = settings) #starts the heavy lifting
                            log.debug(word_output)
                            if embed is not None:
                                link_array.append(settings)
                            if not word['process']:
                                word_output = datum
                            user_said += word_output + whitespace[pos]      
                            pos += 1 
                else:
                    log.debug('I got words but they are boring!')
                    user_said = words
            if message.attachments: #attachments are files dragged/dropped directly into Discord. All we have to play with are some basic stats and the filename
                log.debug('Attachments? Attachments!')
                for datum in message.attachments:
                    log.debug(datum)
                    settings = ImagesafeSettings(log_data = channel_log, explicit = explicit, hide_image = no_show, primary = True, mode = mode, target_user = target_user)
                    embed = await self.extract_filename(url = datum.url, settings = settings)
                    if embed is None:
                        if 'unsourced' in self.metrics:
                            self.metrics['unsourced'] += 1
                        else:
                            self.metrics['unsourced'] = 1
                        if mode != 'full':
                            continue
                        if target_user is not None:
                            continue
                        if channel_log is not None:
                            channel_log.log = 'Dragged and dropped file not sourced!'
                            channel_log.log = '```Aborted```'
                            await self.log_to_channel(channel_log.log)
                        return
                    link_array.append(settings)
            if not link_array and target_user is not None:
                if self.regex_url.search(user_said) is not None or message.attachments:
                    await target_user.send("Couldn't find a hit for that!")
            if link_array: 
                #regular mode
                extra_messages = []
                already_spammed = []
                mentions = ""
                if target_user is None and mode == 'full':
                    if message.mentions:
                        mention_array = []
                        for mention in message.mentions:
                            mention_array.append(mention.mention)
                        mentions = " ".join(mention_array)
                    log.debug(mentions)
                    log.debug(user_said)
                    if not mode == 'full':
                        user_said = ''
                        mentions = ''
                    if user_said:
                        if mentions:
                            mentions = "\n\nAnd mentioned: " + mentions
                    log.debug(user_said)
                first_loop = True
                initial_message = None
                sent_message = None
                add_source = False
                for settings in link_array:
                    if settings.story:
                        settings.embed.type = 'story'
                    else:
                        settings.embed.type = 'image'
                    embed = settings.embed.embed
                    log.debug(already_spammed)
                    log.debug(embed.url)
                    log.debug(settings.mode)
                    embed.set_footer(text="Linked By: {0}".format(message.author.display_name))
                    if target_user is not None:
                        embed.add_field(name="Source provided by", value="[SourceBot](https://www.patreon.com/SourceBot)")
                    if target_user is None:
                        if settings.mode == 'basic':
                            log.debug('processing basic')
                            if embed.url in already_spammed:
                                continue
                            else:
                                already_spammed.append(embed.url)
                            asyncio.ensure_future(ctx.send(content='<{}>'.format(embed.url)))
                            continue
                        elif settings.mode == 'full':
                            log.debug('processing full')
                            if embed.url in already_spammed:
                                continue
                            else:
                                already_spammed.append(embed.url)
                            if user_said and first_loop:
                                first_loop = False
                                initial_message = await ctx.send("{0} said: \n{1}{2}".format(message.author.display_name,user_said, mentions))  
                                extra_messages.append(initial_message.id)
                            sent_message = await ctx.send(embed=embed)
                            asyncio.create_task(self.update_embeds(sent_message, settings, message.author.display_name))
                            asyncio.ensure_future(sent_message.add_reaction("🚫"))
                            if 'fimfic' in embed.url:
                                if '/story/' in embed.url:
                                    asyncio.ensure_future(self.add_emoji(sent_message))
                                if '/user/' in embed.url:
                                    asyncio.ensure_future(self.add_emoji(sent_message,story=False))
                        elif settings.mode == 'mini':
                            log.debug('processing minimalist')
                            add_source = True
                            continue
                    else:
                        sent_message = await target_user.send(content='<{}>'.format(embed.url), embed=embed)
                        asyncio.create_task(self.update_embeds(sent_message, settings, message.author.display_name))
                        asyncio.ensure_future(sent_message.add_reaction("🚫"))
                        if 'fimfic' in embed.url:
                            if '/story/' in embed.url:
                                asyncio.ensure_future(self.add_emoji(sent_message))
                            if '/user/' in embed.url:
                                asyncio.ensure_future(self.add_emoji(sent_message,story=False))
                        continue
                    if sent_message:
                        async with self.config.channel(ctx.channel).history() as history:
                            history[sent_message.id] = {
                                'user' : message.author.id,
                                'date' : pyrfc3339.generate(datetime.utcnow(),accept_naive=True),
                                'extra_messages' : extra_messages
                            }
                    extra_messages = []
                if add_source:
                    log.debug('adding emoji')
                    self.find_emoji()
                    if mode != 'mini':
                        if initial_message:
                            asyncio.ensure_future(initial_message.add_reaction(self.source_emoji))
                        else:
                            asyncio.ensure_future(message.add_reaction(self.source_emoji))
                    else:
                        asyncio.ensure_future(message.add_reaction(self.source_emoji))
                if channel_log is not None:
                    channel_log.log = 'Processed embed(s)!'
                    channel_log.log = '```Done```'
                    await self.log_to_channel(channel_log.log)
                if target_user is not None:
                    return
                if not sent_message or add_source:
                    return
                try:
                    await message.delete()
                except discord.Forbidden:
                    await ctx.send('Error trying to delete message: Insufficient permissions.')
                except discord.NotFound:
                    pass
            elif mode == 'mini':
                #Couldn't find anything! Why do we remove the reaction? Because it might be an edit!
                self.find_emoji()
                asyncio.ensure_future(message.remove_reaction(self.source_emoji, ctx.me))
        except FoalconException:
            log.error('Foalcon posted. Ignored.')
            if channel_log is not None:
                channel_log.log = 'Image was marked foalcon!'
                channel_log.log = '```Abort```'
                await self.log_to_channel(channel_log.log)
            if target_user is not None:
                await target_user.send("Link is foalcon (according to indexed sites).")
            else:
                self.find_emoji()
                await message.add_reaction(self.foalcon_emoji)

    async def update_emoji(self, embed, sent_message):
        try:
            i = 0
            while True:
                links = embed.links[i]
                i += 1
                await asyncio.sleep(1)
                log.debug('searching for extra data from: ' + links)
                if links.startswith('file:'):
                    links = links[5:]
                    await self.extract_filename(embed = embed, file_name = links, primary = False)
                else:
                    await self.extract_embed(links,embed = embed, primary = False)
                if len(embed.links) > i:
                    continue   
                else:
                    break
        except FoalconException:
            self.find_emoji()
            member = sent_message.channel.guild.get_member(self.bot.user.id)
            sent_message.remove_reaction(self.source_emoji, member)
            await sent_message.add_reaction(self.foalcon_emoji)
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)


    async def update_embeds(self, sent_message, settings, footer):
        try:
            settings.primary = False
            if not settings.image_data and settings.primary_image:
                log.debug(settings.primary_image)
                settings.image_data = await self.get_image_data(settings.primary_image)
            while settings.embed.links:
                links = settings.embed.links.pop(0)
                await asyncio.sleep(1)
                log.debug('searching for extra data from: ' + links)
                log.debug(str(settings.embed))
                initial_hash = settings.embed.hash
                if links.startswith('file:'):
                    links = links[5:]
                    await self.extract_filename(file_name = links, settings = settings)
                elif links.startswith('hash:'):
                    log.debug(settings)
                    links = links[5:]
                    await self.extract_from_hash(links, settings = settings)
                else:
                    await self.extract_embed(links, settings = settings)
                updated = settings.embed.hash != initial_hash
                log.debug(str(settings.embed))
                log.debug(updated)
                if updated:
                    if settings.story:
                        settings.embed.type = 'story'
                    else:
                        settings.embed.type = 'image'
                    output_embed = settings.embed.embed
                    output_embed.set_footer(text="Linked By: {0}".format(footer))
                    if isinstance(sent_message.channel, discord.DMChannel):
                        output_embed.add_field(name="Source provided by", value="[SourceBot](https://www.patreon.com/SourceBot)")
                    for field_num in range(len(output_embed.fields)):
                        log.debug(output_embed.fields[field_num])
                        if output_embed.fields[field_num].name == 'force':
                            output_embed.remove_field(field_num)
                            break
                    settings.embed.debug()
                    await sent_message.edit(content = sent_message.content, embed=output_embed)
                log.debug(settings)
        except FoalconException:
            await sent_message.edit(content = "Image was foalcon!")
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)

    async def add_emoji(self,message, story : bool = True, writer : bool = True):
        if story:
            await message.add_reaction("🌟")
            await message.add_reaction("✴")
        if writer:
            await message.add_reaction("🇦")
            await message.add_reaction("🅰")
        if isinstance(message.channel,discord.TextChannel):
            if writer or story:
                await message.add_reaction("ℹ")
    
    #A function for doing json data requests using your method of choice and support caching via etag or last-modified headers
    async def get_data(self, url : str, key : str = None, type : str = None, options : dict = {}, headers : dict = {}, method : str = "GET"):
        if key is not None and type is not None:
            datum = self.get_etag(key, type)
            log.debug(datum)
            if datum is not None:
                if 'etag' in datum:
                    if type == 'e621' and datum['etag'].startswith('W/'):
                        headers['If-None-Match'] = datum['etag'][2:]
                    else:
                        headers['If-None-Match'] = datum['etag']
                if 'last_modified' in datum:
                    headers['If-Modified-Since'] = datum['last_modified']
        loop = asyncio.get_event_loop()
        if 'User-Agent' not in headers.keys():
            headers['User-Agent'] = user_agent
        log.debug(headers)
        log.debug(url)
        async with self.session.request(method, url, params = options, headers=headers) as resp: 
            try:
                resp.raise_for_status()      
            except:
                log.debug('Code: ' + str(resp.status))
                return None
            if resp.status == 304:
                output = datum['data']
                log.debug('Etag hit!')
            else:
                log.debug('Etag miss!')
                output = await resp.json()
                if output is None:
                    return None
                if key is not None and type is not None:
                    etag = None
                    last_modified = None
                    if 'etag' in resp.headers:
                        etag = resp.headers['etag']
                    if 'last-modified' in resp.headers:
                        last_modified = resp.headers['last-modified']
                    if etag is not None or last_modified is not None:
                        self.add_to_cache( key, output, type, etag = etag, last_modified = last_modified)
                        if type == 'e621' and key.isdigit():
                            self.add_to_cache( output['md5'], output, 'e621', etag = etag, last_modified = last_modified)
                        elif type == 'e621':
                            self.add_to_cache( output['id'], output, 'e621', etag = etag, last_modified = last_modified)
        log.debug(output)
        return output
        
    async def get_website(self, url : str, options : dict = {}, headers : dict = {}, method : str = "GET", cookies : dict = {}):
        start_time = datetime.utcnow()
        if 'User-Agent' not in headers.keys():
            if 'deviantart.com' in url or 'fav.me' in url:
                headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
                cookies['userinfo'] = '__e7965738a46c0042c483%3B%7B%22username%22%3A%22%22%2C%22uniqueid%22%3A%22d1cbfb13891c1a9289cff265011d9899%22%2C%22vd%22%3A%22BdGHJj%2CBdGHJj%2CA%2Cd%2CA%2C%2CB%2CA%2CB%2CBdGU8h%2CBdGU8h%2CA%2CA%2CA%2CA%2C13%2CA%2CB%2CA%2Ctao-snt-1-a-4%2CA%2CA%2CB%2CA%2CA%2C%2CA%22%2C%22dvs9-1%22%3A1%7D'
                cookies['auth'] = '__61f29e389596b1569e12%3B%2257e5a609d9ed788a12af8c3e0dbe914e%22'
                cookies['auth_secure'] = '__e70fd13d62c96e943b60%3B%222b86cf2ffb84c0eee5e2085ea8d07035%22'
                cookies['__qca'] = 'P0-627546443-1561883235232'
                log.debug(headers)
                log.debug(cookies)
            else:
                headers['User-Agent'] = user_agent
        log.debug(headers)
        async with self.session.request(method, url, params = options, headers=headers, cookies = cookies) as resp: 
            try:
                resp.raise_for_status()      
            except:
                return None
            log.debug(resp.status)
            #log.debug(resp.headers)
            output = resp
            if output is None:
                return None
            output.website = await output.text()
            #log.debug(output)
            end_time = datetime.utcnow()
            log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
            log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
            diff_time = end_time - start_time
            log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
            return output
        
    async def get_image_data(self, url : str, headers : dict = {}, method : str = "GET"):
        start_time = datetime.utcnow()
        log.debug(url)
        cache_item = self.get_cache( url, 'image_data')
        if cache_item is not None:
            return cache_item
        datum = self.get_etag(url, 'image_data')
        log.debug(datum)
        log.debug(headers)
        if datum and 'data' in datum and 'phash' in datum['data']:
            if 'etag' in datum:
                headers['If-None-Match'] = datum['etag']
            if 'last_modified' in datum:
                headers['If-Modified-Since'] = datum['last_modified']
        headers['User-Agent'] = user_agent
        log.debug(headers)
        async with self.session.request(method, url, headers=headers) as resp: 
            try:
                resp.raise_for_status()      
            except:
                log.debug('Error: ' + str(resp.status))
                return None
            log.debug(resp.status)
            #log.debug(resp.headers)
            if resp.status == 304 or ( 'content-type' in resp.headers and resp.headers['content-type'].startswith('image/') ):
                if resp.status == 304:
                    output = datum['data']
                    log.debug('Etag hit!')
                else:
                    log.debug('Etag miss!')
                    downloaded = 0
                    buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
                    async for chunk in resp.content.iter_chunked(1024000):
                        downloaded += len(chunk)
                        buffer.write(chunk)
                    buffer.seek(0)
                    site_data = buffer.read()
                    img = Image.open(io.BytesIO(site_data))
                    width, height = img.size
                    dimensions = { 'height' : height , 'width' : width }
                    phash = str(imagehash.average_hash(img, hash_size=16))
                    img = None
                    rgb_img = None
                    buffer = None 
                    hash_sha512 = sha512()
                    hash_sha512.update(site_data)
                    hash_md5 = md5()
                    hash_md5.update(site_data)
                    output = {
                        'md5' : hash_md5.hexdigest(),
                        'sha512' : hash_sha512.hexdigest(),
                        'dimensions' : dimensions,
                        #'quads' : keyvalue
                        'phash' : phash
                    }
                    etag = None
                    last_modified = None
                    if 'etag' in resp.headers:
                        etag = resp.headers['etag']
                    if 'last-modified' in resp.headers:
                        last_modified = resp.headers['last-modified']
                    if etag is not None or last_modified is not None:
                        self.add_to_cache( url, output, 'image_data', etag = etag, last_modified = last_modified)
                self.add_to_cache( url, output, 'image_data')
            else:
                end_time = datetime.utcnow()
                log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
                log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
                diff_time = end_time - start_time
                log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
                return None
            output['url'] = url
            end_time = datetime.utcnow()
            log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
            log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
            diff_time = end_time - start_time
            log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
            return output
    
    
    
    #The following are a series of functions to get data from various services, usually (but not always) taking an ID and spitting out JSON
    
    async def get_derpi_reverse(self, url, settings : ImagesafeSettings = ImagesafeSettings()):
        try:
            log.debug('reverse: ' + url)
            log.debug(settings)
            #return None
            log.debug(settings.image_data)
            if '?' in url and 'discordapp' in url:
                url = url.rsplit('?',1)[0]
            start_time = datetime.utcnow()
            cache_item = self.get_cache( url, 'derpi_reverse')
            if cache_item is not None:       
                if settings.log_data is not None:
                    settings.log_data.log = 'Fetched derpibooru id from reverse search cache'
                return cache_item
            # headers = { 'User-Agent' : user_agent, 'Origin' : 'https://derpibooru.org', 'Host' : 'derpibooru.org', 'Connection' : 'keep-alive', 'Upgrade-Insecure-Requests' : '1', 'TE' : 'Trailers'}
            # loop = asyncio.get_event_loop()
            # response = loop.run_in_executor(None, partial(requests.request,"GET", 'https://derpibooru.org/search/reverse', headers = headers))
            # website = await response
            # if website.status_code != requests.codes.ok:
            #     if settings.log_data is not None:
            #         settings.log_data.log = 'Error: ' + str(website.status_code) + ' GET to ' + 'https://derpibooru.org/search/reverse.json'
            #     log.error('Error: ' + str(website.status_code) + ' GET to ' + 'https://derpibooru.org/search/reverse.json')
            #     return None
            # if website is None:
            #     return None
            # page_text = website.text
            # soup = BeautifulSoup(page_text, 'html.parser')
            # #authenticity_token = soup.find('input', attrs={'name' : 'authenticity_token'}).get('content')
            # csrf_token = soup.find('meta', attrs={'name' : 'csrf-token'}).get('content')
            # log.debug(csrf_token)
            # if csrf_token is None:
            #     log.debug('Can\'t find csrf_token')
            #     return None
            # headers['X-CSRF-Token'] = csrf_token
            # options = {'url' : url} #, 'authenticity_token' : authenticity_token}
            # temp_header = headers
            # temp_header['Referer'] = 'https://derpibooru.org/search/reverse'
            # response = loop.run_in_executor(None, partial(requests.request,"POST", 'https://derpibooru.org/images/scrape_url', headers = headers, params=options, cookies=website.cookies))
            # website = await response
            # if website.status_code != requests.codes.ok:
            #     if settings.log_data is not None:
            #         settings.log_data.log = 'Error: ' + str(website.status_code) + ' POST to ' + 'https://derpibooru.org/images/scrape_url'
            #     log.error('Error: ' + str(website.status_code) + ' POST to ' + 'https://derpibooru.org/images/scrape_url')
            #     return None
            # if website is None:
            #     return None
            # scrape_json = website.json()
            # if 'error' in scrape_json:
            #     if scrape_json['error']:
            #         log.debug("Found an error in scrape: " + str(scrape_json['error']))
            #         return None
            # options = {'fuzziness' : 0.25, 'utf8' : '✓', 'scraper_url' : scrape_json['source_url']} #'authenticity_token' : authenticity_token,
            # response = loop.run_in_executor(None, partial(requests.request,"POST", 'https://derpibooru.org/search/reverse.json', headers = headers, params=options, cookies=website.cookies))
            #loop = asyncio.get_event_loop()
            headers = { 'User-Agent' : user_agent, 'Content-Type' : 'application/json' }
            options = {'fuzziness' : '0.25', 'utf8' : '✓', 'scraper_url' : url}
            response = await self.get_data('https://derpibooru.org/search/reverse.json?key=GET YOUR OWN DERPIBOORU KEY', headers = headers, options=options)
            # response = await response
            # if response.status_code != requests.codes.ok:
            #     if settings.log_data is not None:
            #         settings.log_data.log = 'Error: ' + str(response.status_code) + ' POST to ' + 'https://derpibooru.org/search/reverse.json'
            #     log.error('Error: ' + str(response.status_code) + ' POST to ' + 'https://derpibooru.org/search/reverse.json')
            #     return None
            # log.debug(response)
            # response = response.json()
            if response is None or 'search' not in response or not response['search']:
                return None
            possible_hits = []
            for results in response['search']:
                if 'duplicate_of' in results or 'deletion_reason' in results:
                    continue
                if settings.image_data is not None:
                    if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(results['width']) / int(results['height']),1):
                        continue
                    log.debug('found no dupe!')
                    possible_hits.append(results)
            if len(possible_hits) != 1:
                possible_hits = []
                for results in response['search']:
                    if 'duplicate_of' in results or 'deletion_reason' in results:
                        continue
                    if settings.image_data is not None:
                        if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(results['width']) / int(results['height']),1):
                            continue
                        image_data2 = await self.get_image_data("https:" + results['representations']['full'], { 'User-Agent' : user_agent})
                        if image_data2 is not None:
                            if hamming_distance(settings.image_data['phash'],image_data2['phash']) <= 4:
                                possible_hits.append(results)
            log.debug(possible_hits)
            if len(possible_hits) != 1:
                return None
            image_id = possible_hits[0]['id']
            if image_id is None:
                log.debug('No image found!')
                return None
            self.add_to_cache( url, image_id, 'derpi_reverse') 
            end_time = datetime.utcnow()
            log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
            log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
            diff_time = end_time - start_time
            log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
            return image_id
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            return None

    async def get_derpi_data(self, image_id : str = None, settings : ImagesafeSettings = ImagesafeSettings()):
        try:
            while True:
                output = self.archive[image_id]
                if output is None:
                    break
                if 'duplicate_of' in output:
                    image_id = str(output['duplicate_of'])
                    continue
                if 'deletion_reason' in output:
                    return {}
                break  
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            output = None
        if output:
            log.debug('Archive Hit')
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched derpibooru data from archive'
            return output
        start_time = datetime.utcnow()
        cache_item = self.get_cache( image_id, 'derpi')
        if cache_item is not None:       
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched derpibooru data from memory cache'
            return cache_item
        loop = asyncio.get_event_loop()
        while True:
            output = await self.get_data('https://derpibooru.org/{}.json'.format(image_id), image_id, 'derpi')
            #datum = self.get_etag(image_id, 'derpi')
            #headers = {}
            #if datum is not None:
            #    headers['If-None-Match'] = datum['etag']
            #response = loop.run_in_executor(None, partial(requests.request,"GET", 'https://derpibooru.org/{}.json'.format(image_id), headers=headers))
            #website = await response
            #log.debug(website.status_code)
            #log.debug(website.headers)
            #if website.status_code == 304:
            #    output = datum['data']
            #    log.debug('Etag hit!')
            #else:
            #    log.debug('Etag miss!')
            #    if website.status_code != requests.codes.ok:
            #        return None
            #    output = website.json()
            #    if output is None:
            #        return {}
            #    if 'id' not in output:
            #        return {}
            #    if 'etag' in website.headers:
            #        self.add_to_cache( image_id, output, 'derpi', etag = website.headers['etag'])
            if output is None:
                return {}
            if 'duplicate_of' in output:
                image_id = str(output['duplicate_of'])
                continue
            if 'deletion_reason' in output:
                return {}
            break  
        self.add_to_cache( image_id, output, 'derpi')            
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched derpibooru data'
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return output
            
    async def get_derpi_data_from_hash(self, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.image_data is None:
            return {}
        start_time = datetime.utcnow()
        cache_item = self.get_cache( settings.image_data['sha512'], 'derpihash')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched derpibooru (hash) data from cache'
            return cache_item
        start_time = datetime.utcnow()
        log.debug('Cache miss!')
        #try:
        search_field = 'https://derpibooru.org/search.json?q=' + 'orig_sha512_hash:' + settings.image_data['sha512']
        search_field += "&filter_id=56027"
        log.debug(search_field)
        headers = {'user-agent' : user_agent}
        async with self.session.request("GET", search_field, headers=headers) as resp: 
            try:
                resp.raise_for_status()      
            except:
                log.error('Bad status: ' + str(resp.status))
                return
            website = await resp.json()
        log.debug(website)
        if website is None:
            return {}
        if 'search' not in website:
            return {}
        if int(website['total']) == 0:
            return {}
        website = website['search'][0]
        log.debug(website) 
        self.add_to_cache( settings.image_data['sha512'], website, 'derpihash')
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched derpibooru (hash) data'
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return website
        #except:
        #    return None
            
    async def get_tumblr_data(self, url, settings : ImagesafeSettings = ImagesafeSettings()):
        start_time = datetime.utcnow()
        original_url = url
        cache_item = self.get_cache( url, 'tumblr')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched tumblr data from cache'
            return cache_item

        tumblr_api_key = await self.config.tumblr_api_key() #load Tumblr API key
        if not tumblr_api_key:
            log.error('No API key installed for Tumblr!')
            return {}
        url = list(urlparse(url))
        blog_name = url[1]
        blog_id = None
        for datum in url[2].split('/'):
            if datum.isdigit():
                blog_id = datum
                break
        if blog_id is None:
            return {}
        log.debug('https://api.tumblr.com/v2/blog/{name}/posts?api_key={api_key}&id={id}'.format(name=blog_name, id=blog_id, api_key=api_key))
        headers = {'user-agent' : user_agent}
        async with self.session.request("GET", 'https://api.tumblr.com/v2/blog/{name}/posts?api_key={api_key}&id={id}'.format(name=blog_name, id=blog_id, api_key=tumblr_api_key), headers=headers) as resp: 
            try:
                resp.raise_for_status()      
            except:
                log.error('Bad status: ' + str(resp.status))
                return
            website = await resp.json()
        if website is None:
            return {}
        if website['meta']['status'] != 200:
            return {}
        self.add_to_cache( original_url, website['response'], 'tumblr')
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched tumblr data'
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return website['response']
        
    async def get_e621_data(self, image_id , settings : ImagesafeSettings = ImagesafeSettings()):
        start_time = datetime.utcnow()
        cache_item = self.get_cache( image_id, 'e621')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched e621 data from cache'
            return cache_item
        if image_id.isdigit():
            options = { 'id' : image_id}
        else:
            options = { 'md5' : image_id }
        headers = { 'User-Agent' : user_agent}
        output = await self.get_data('https://e621.net/post/show.json', image_id, 'e621', headers = { 'User-Agent' : user_agent}, options = options)
        #datum = self.get_etag(image_id, 'e621')
        #if datum is not None:
        #    headers['If-None-Match'] = datum['etag'][2:]
        #loop = asyncio.get_event_loop()
        #response = loop.run_in_executor(None, partial(requests.request,"GET", 'https://e621.net/post/show.json', params = option, headers = headers))
        #website = await response
        #log.debug(website.status_code)
        #log.debug(website.headers)
        #if website.status_code == 304:
        #    output = datum['data']
        #    log.debug('Etag hit!')
        #else:
        #    log.debug('Etag miss!')
        #    if website.status_code != requests.codes.ok:
        #        return None
        #    output = website.json()
        #    if output is None or not output:
        #        return None
        #    if 'etag' in website.headers:
        #        self.add_to_cache( output['id'], output, 'e621', etag = website.headers['etag'])
        #        self.add_to_cache( output['md5'], output, 'e621', etag = website.headers['etag'])
        if output is None or not output:
            return None
        self.add_to_cache( image_id, output, 'e621')
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched e621 data'
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return output

    async def get_deviant_data(self, url, settings : ImagesafeSettings = ImagesafeSettings()):
        start_time = datetime.utcnow()
        cache_item = self.get_cache( url, 'deviant')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched deviant data from cache'
            return cache_item
        if url.startswith('https') and 'fav.me' in url:
            url = 'http:' + url.split(':',1)[1]
        log.debug(url)
        output = await self.get_data('https://backend.deviantart.com/oembed?url={}'.format(url), url, 'deviant')
        if output is None:
            return None
        website = await self.get_website(url)
        output['description'] = ''
        output['page_url'] = url
        if website is not None:
            site_data = opengraph_py3.OpenGraph(html=website.website)
            if 'url' in site_data:
                output['page_url'] = site_data['url']
            soup = BeautifulSoup(website.website)
            description = soup.find('div',class_='legacy-journal')
            if description is None:
                description = soup.find('div',class_='dev-description')
            if description is None:
                description = ''
            output['description'] = str(description)

        #loop = asyncio.get_event_loop()
        #response = loop.run_in_executor(None, partial(requests.request,"GET", 'https://backend.deviantart.com/oembed?url={}'.format(url)))
        #website = await response
        #if website.status_code != requests.codes.ok:
        #    return None
        #website = website.json()
        #website['page_url'] = url
        #log.debug(website)
        self.add_to_cache( url, output, 'deviant')
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched deviant data'
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return output  
    
    #async def get_fanfictionnet_data(self, story_id, log_data : DiscordLog = None):
    #    cache_item = self.get_cache( story_id, 'fanficnet')
    #    if cache_item is not None:
    #        return cache_item
    #    try:
    #        log.debug(story_id)
    #        story = ff.Story(id=story_id)
    #        self.add_to_cache( story_id, story, 'fanficnet')
    #        return story  
    #    except:
    #        return None

    async def get_archiveofourown_data(self, story_id, settings : ImagesafeSettings = ImagesafeSettings()):
        start_time = datetime.utcnow()
        cache_item = self.get_cache( story_id, 'ao3')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched ao3 data from cache'
            return cache_item
        api = AO3()
        log.debug(story_id)
        story = api.work(id=story_id)
        self.add_to_cache( story_id, story, 'ao3')
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched ao3 data'
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return story  
    
    async def logon_inkbunny(self, settings : ImagesafeSettings = ImagesafeSettings()):
        inkbunny_userdata = await self.config.inkbunny_userdata() #load InkBunny userdata
        loop = asyncio.get_event_loop()
        if not inkbunny_userdata['username'] or not inkbunny_userdata['password']:
            log.error('No inkbunny login details!')
            raise ValueError('No InkBunny logon data!')
        if not self.inkbunny_sid:
            headers = {'user-agent' : user_agent}
            async with self.session.request("POST", 'https://inkbunny.net/api_login.php', data={'username' : inkbunny_userdata['username'], 'password' : inkbunny_userdata['password']}, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    raise Exception('Inkbunny failed to log on!')
                website = await resp.json()
            if 'sid' in website:
                self.inkbunny_sid = website['sid']
                if settings.log_data is not None:
                    settings.log_data.log = 'Logged into InkBunny'
            else:
                raise Exception('Inkbunny failed to log on!')
            #payload = {
            #    'sid' : self.inkbunny_sid,
            #    'tag[2]' : 'yes',
            #    'tag[3]' : 'yes',
            #    'tag[4]' : 'yes',
            #    'tag[5]' : 'yes'
            #}
            #response = loop.run_in_executor(None, partial(requests.request,"POST", 'https://inkbunny.net/api_userrating.php', payload=payload))
            #website = await response
            #if 'sid' in website.json():
            #    self.inkbunny_sid = website.json()['sid']
            #else:
            #    raise Exception('Inkbunny failed to set ratings!')
    
    async def get_inkbunny(self, url : str = None, submission_id : str = None, md5_checksum : str = None, settings : ImagesafeSettings = ImagesafeSettings()):
        start_time = datetime.utcnow()
        while True:
            if submission_id is not None:         
                cache_item = self.get_cache( submission_id, 'inkbunny_sub')
                if cache_item is not None:
                    if md5_checksum is None:
                        log.debug('Target is 0')
                        cache_item['target'] = 0
                    else:
                        for i,datum in enumerate(cache_item['files']):
                            log.debug(datum.values())
                            log.debug(md5_checksum)
                            if md5_checksum in datum.values():
                                log.debug('Setting target to: ' + str(i))
                                cache_item['target'] = i
                                break
                    if settings.log_data is not None:
                        settings.log_data.log = 'Fetched InkBunny data from cache'
                    return cache_item   
                if not self.inkbunny_sid:
                    await self.logon_inkbunny(settings = settings)
                payload = {
                    'sid' : self.inkbunny_sid,
                    'submission_ids' : submission_id
                }
                log.debug(payload)
                headers = {'user-agent' : user_agent}
                async with self.session.request("POST", 'https://inkbunny.net/api_submissions.php', data=payload, headers=headers) as resp: 
                    try:
                        resp.raise_for_status()      
                    except:
                        log.error('Bad status: ' + str(resp.status))
                        return
                    website = await resp.json()
                if 'sid' in website:
                    self.inkbunny_sid = website['sid']
                else:
                    return None
                if 'submissions' in website and website['submissions']:
                    data = website['submissions'][0]
                    log.debug(data)
                    if md5_checksum is None:
                        log.debug('Target is 0')
                        data['target'] = 0
                    else:
                        for i,datum in enumerate(data['files']):
                            log.debug(datum.values())
                            log.debug(md5_checksum)
                            if md5_checksum in datum.values():
                                log.debug('Setting target to: ' + str(i))
                                data['target'] = i
                                break
                        if 'target' not in data:
                            log.debug('Couldn\'t find target')
                            return None
                    if 'submission_id' in data:
                        self.add_to_cache( submission_id, data, 'inkbunny_sub')
                        if settings.log_data is not None:
                            settings.log_data.log = 'Fetched InkBunny data'
                        end_time = datetime.utcnow()
                        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
                        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
                        diff_time = end_time - start_time
                        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
                        return data
                    else:
                        return None
            if md5_checksum is not None:       
                cache_item = self.get_cache( md5_checksum, 'inkbunny_md5')
                if cache_item is not None:
                    submission_id = cache_item 
                    continue
                if not self.inkbunny_sid:
                    await self.logon_inkbunny()
                payload = {
                'sid' : self.inkbunny_sid,
                'text' : md5_checksum,
                'md5' : 'yes'
                }
                log.debug(payload)
                headers = {'user-agent' : user_agent}
                async with self.session.request("POST", 'https://inkbunny.net/api_search.php', data=payload, headers=headers) as resp: 
                    try:
                        resp.raise_for_status()      
                    except:
                        log.error('Bad status: ' + str(resp.status))
                        return
                    website = await resp.json()
                log.debug(website)
                if 'sid' in website:
                    self.inkbunny_sid = website['sid']
                else:
                    return None
                if 'submissions' in website and website['submissions']:
                    data = website['submissions'][0]
                    if 'submission_id' in data:
                        submission_id = data['submission_id']
                        log.debug('SID: ' + submission_id)
                        self.add_to_cache( md5_checksum, submission_id, 'inkbunny_md5')
                        continue
                    else:
                        return None
                else:
                    return None
            if url is not None:       
                cache_url = re.search(r'([0-9]+)_\S+_(?:[0-9]{4}_)?\S+',url)
                if cache_url is None:
                    return None
                cache_url = cache_url.group(1)
                cache_item = self.get_cache( cache_url, 'inkbunny_url')
                if cache_item is not None:
                    md5_checksum = cache_item
                    continue             
                hash_md5 = md5()
                headers = {'user-agent' : user_agent}
                async with self.session.request("GET", url, headers=headers) as resp: 
                    try:
                        resp.raise_for_status()      
                    except:
                        log.error('Bad status: ' + str(resp.status))
                        return
                    downloaded = 0
                    buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
                    async for chunk in resp.content.iter_chunked(1024000):
                        downloaded += len(chunk)
                        buffer.write(chunk)
                buffer.seek(0)
                hash_md5.update(buffer.read())
                md5_checksum = hash_md5.hexdigest()
                log.debug('MD5: ' + md5_checksum)
                self.add_to_cache( cache_url, md5_checksum, 'inkbunny_url')
                continue
            return None
        
    async def search_stories(self, story_title):
        start_time = datetime.utcnow()
        story_title = '"' + story_title + '"'
        stories = await self.get_stories({ 'query' : story_title , 
            'max_results' : 99, 
            'fields' : { 
                'story' : [ 'name', 'title', 'short_description', 'completion_status', 'content_rating', 'date_updated', 'date_modified', 'date_published', 'num_words', 'cover_image', 'author', 'tags', 'num_likes', 'num_dislikes', 'color' ], 
                'user' : [ 'date_joined', 'id', 'name', 'avatar', 'bio' ] 
            }, 
            'include' : 'characters,author,tags' 
        })
        log.debug(stories)
        if stories is None:
            return None
        if not stories or 'error' in stories:
            return None
        if 'stories' in stories and not stories['stories']:
            return None
        counter = 1
        story_options = {}
        best_story = { 'id' : '' , 'num_likes' : 0 }
        for story_id,story in stories['story'].items():
            if story['title'].lower() == story_title.lower():
                if story['num_likes'] > best_story['num_likes']:
                    best_story['id'] = story['id']
                    best_story['num_likes'] = story['num_likes']
        log.debug(best_story)
        if best_story['id']:
            story = stories['story'][best_story['id']]
            stories['order'].remove(story['id'])
            story_options[counter] = story
            counter += 1
        log.debug(stories['order'])
        for story_id in stories['order']:
            story = stories['story'][story_id]
            story_options[counter] = story
            counter += 1
            if counter == 6:
                break
        log.debug(story_options)
        end_time = datetime.utcnow()
        log.debug('Start: ' + pyrfc3339.generate(start_time,accept_naive=True))
        log.debug('End:   ' + pyrfc3339.generate(end_time,accept_naive=True))
        diff_time = end_time - start_time
        log.debug('Took:  ' + str(diff_time.seconds) + '.' + "{:06d}".format(diff_time.microseconds)[-6:-3])
        return story_options

    def record_limits( self, headers, endpoint ):
        if endpoint not in self.rate:
            self.rate[endpoint] = {}
        self.rate[endpoint]['data'] = { 'remaining' : str(headers['x-rate-limit-remaining']), 'maximum' : str(headers['x-rate-limit-limit']), 'reset' : pyrfc3339.generate(datetime.utcnow() + timedelta(seconds = int(headers['x-rate-limit-reset'])), accept_naive=True) }
  
    def remaining_limit( self, endpoint):
        if endpoint not in self.rate:
            return None
        if 'data' not in self.rate[endpoint]:
            return None
        if 'remaining' not in self.rate[endpoint]['data']:
            return None
        if 'reset' in self.rate[endpoint]['data']:
            log.debug(self.rate[endpoint]['data']['reset'])
            log.debug(pyrfc3339.generate(datetime.utcnow(),accept_naive=True))
            log.debug(self.rate[endpoint]['data']['reset'] > pyrfc3339.generate(datetime.utcnow(),accept_naive=True))
            if self.rate[endpoint]['data']['reset'] > pyrfc3339.generate(datetime.utcnow(),accept_naive=True):
                return int(self.rate[endpoint]['data']['remaining'])
            else:
                return 1
        else:
            return 1

    async def fimfic_header(self):
        fimfic_api_key = await self.config.fimfic_api_key() #load Fimfic API key
        fimfic_api_key = fimfic_api_key['api_key']
        return {'Authorization': "Bearer {}".format(fimfic_api_key), 'Content-Type' : 'application/vnd.api+json', 'user-agent' : user_agent}
        
    async def get_stories( self, search_list , settings : ImagesafeSettings = ImagesafeSettings()):
        if self.remaining_limit('stories') is not None and self.remaining_limit('stories') <= 0:
            return None
        filters = []
        if 'query' in search_list:
            filters.append('query={}'.format(quote(search_list['query'])))
            if 'sort' not in search_list:
                search_list['sort'] = '-relevance'
        if 'filters' in search_list:
            for filter_name, options in search_list['filters'].items():
                filters.append('filter[{0}]={1}'.format(filter_name,','.join(map(str,options))))
        if not filters:
            return None
        if 'include' in search_list:
            includes = [ 'include=' + search_list['include'] ]
        else:
            includes = [ 'include=characters,author,tags' ]
        if 'page' in search_list:
            page_size = int(search_list['page'])
            page = [ 'page[size]=' + str(search_list['page']) ]
        else:
            page = [ 'page[size]=100' ]
            page_size = 100
        fields = []
        if 'fields' in search_list:
            for field_name, options in search_list['fields'].items():
                fields.append('fields[{0}]={1}'.format(field_name,','.join(options)))
        if not fields:
            fields = [ 'fields[story]=chapters,name,title,short_description,completion_status,content_rating,date_updated,date_modified,date_published,num_words,cover_image,author,tags,num_likes,num_dislikes,color' , 'fields[user]=date_joined,id,name,avatar,bio' ] 
        if 'sort' in search_list:
            sorts = [ 'sort={}'.format(search_list['sort']) ]
        else:   
            sorts = [ 'sort=-date_updated' ]
        if 'max_results' in search_list:
            max_results = search_list['max_results']
        else:
            max_results = None #page_size - 1
        url = 'https://www.fimfiction.net/api/v2/stories?'
        cursor = ''
        website = {}
        order = []
        loop = asyncio.get_event_loop()
        newest_datetime = None
        amount_returned = 0
        while True:
            log.debug(url + '&'.join( filters + fields + sorts + includes + page) + cursor)
            headers = await self.fimfic_header()
            async with self.session.get( URL(url + '&'.join( filters + fields + sorts + includes + page) + cursor, encoded = True), headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    log.error(resp.url)
                    return None
                output_returned = await resp.json()
                self.record_limits(resp.headers, 'stories')
            if 'error' in output_returned:
                return None
            else:
                if 'meta' in output_returned:
                    if 'num_stories' in output_returned['meta']:
                        if max_results is None:
                            max_results = int(output_returned['meta']['num_stories'])
                if 'data' in output_returned:
                    if len(output_returned['data']) == 0:
                        break
                    else:
                        log.debug(output_returned['meta'])
                        log.debug(output_returned['links'])
                        cursor = re.findall(r'&page\[cursor\]=([^&]+)', unquote(output_returned['links']['next']))[-1]
                        #cursor = { 'page' , cursor.pattern(1)}
                        for stories in output_returned['data']:
                            order.append(stories['id'])
                        if not website:
                            log.debug('First loop, adding directly to website')
                            website = jsonapi_unflatten(output_returned)
                            output = { 'story' : {} }
                            for story in website['story'].values():
                                story['simple'] = False
                                output['story'][story['id']] = story
                                self.add_to_cache( story['id'], story, 'fimfic_story') #Wooo cache!
                            website = output
                            break   #because fimfic still doesn't support paging through results
                        else:
                            log.debug('Additional loop')
                            for story in jsonapi_unflatten(output_returned)['story'].values():
                                log.debug('Looping a story:' + story['id'])
                                website['story'][story['id']] = story
                        amount_returned += len(output_returned['data'])
                        if max_results is not None and max_results:
                            if max_results <= amount_returned:
                                break
                        if len(output_returned['data']) < page_size:
                            break
                        else:
                            continue
                else:
                    return None
            break
        website['order'] = order
        if 'story' in website:
            log.debug(len(website['story']))
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic search data'
            return website
        else:
            return None
        
    async def get_story_quick(self, story_id, settings : ImagesafeSettings = ImagesafeSettings()):
        cache_item = self.get_cache( story_id, 'fimfic')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic story embed data from cache'
            return cache_item
        try:
            log.debug( story_id )
            loop = asyncio.get_event_loop()
            response = loop.run_in_executor(None, partial(PyEmbed().embed, url='https://www.fimfiction.net/story/{id}/stuff'.format(id=story_id)))
            Embed = await response
            soup = BeautifulSoup(Embed, 'html.parser')
            log.debug(soup.prettify())
            story = {}
            story['name'] = soup.find(class_='story_link').get('title')
            story['url'] = fixup_fimfic_urls(soup.find(class_='story_link').get('href'))
            story['content_rating'] = soup.find(class_=re.compile("content_rating_[a-z]+")).get('title').split(' ')[1]
            author = {}
            author['name'] = soup.find(class_='story-card__author').string
            author['id'] = soup.find(class_='story-card__author').get('href').split('/')[2]
            author = [ author ]
            story['author'] = author
            story['short_description'] = soup.find('span',class_='short_description').contents[2].strip()
            story['cover_image'] = {}
            story['cover_image']['full'] = soup.find('img')
            if story['cover_image']['full'] is not None:
                story['cover_image']['full'] = story['cover_image']['full'].get('data-fullsize')
            if story['cover_image']['full'] is None:
                del story['cover_image']
            story['num_words'] = soup.find('span',class_='story-card__info').contents[4].split(' ')[0]
            story['num_words'] = re.sub(r"\s+", "", story['num_words'], flags=re.UNICODE)
            multi = {'k' : 1000, 'm' : 1000000, 'b' : 1000000000 }
            story['num_words'] = int(float(story['num_words'][:-1]) * multi[story['num_words'][-1]])
            tags = []
            for datum in soup.find_all(class_=re.compile("tag-[a-z]+")):
                log.debug(datum)
                type = datum.get('class')[0].split('-')[1]
                tag = datum.string
                id = datum.get('data-tag-id')
                tags.append({'id' : id, 'type' : type, 'name' : tag})
            story['tags'] = tags
            log.debug(story)
            story['simple'] = True
            self.add_to_cache(story_id, story, 'fimfic')
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic story embed data'
            return story            
        except:
            return None

    async def get_story( self, story_id , settings : ImagesafeSettings = ImagesafeSettings()):
        log.debug( story_id )
        cache_item = self.get_cache( story_id, 'fimfic_story')
        if cache_item is not None:
            return cache_item
        try:
            if self.remaining_limit('stories') is not None and self.remaining_limit('stories') <= 0:
                return await self.get_story_quick(story_id, log_data = log_data)
            search = 'include=characters,author,tags&fields[story]=chapters,name,title,short_description,completion_status,content_rating,date_updated,date_modified,date_published,num_words,cover_image,author,tags,num_likes,num_dislikes,color&fields[user]=date_joined,id,name,avatar,bio'
            base_url = 'https://www.fimfiction.net/api/v2/stories/{}?'.format( story_id )
            headers = await self.fimfic_header()
            async with self.session.request("GET", base_url + search, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    return None
                website = await resp.json()
                self.record_limits(resp.headers, 'stories')
            if 'error' in website:
                return None
            if 'data' in website:
                if len(website['data']) == 0:
                    return None
                website = jsonapi_unflatten(website)
                website['story'][story_id]['simple'] = False
                self.add_to_cache( story_id, website['story'][story_id], 'fimfic_story')
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic story data'
            return website['story'][story_id]
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            return None

    async def get_group( self, group_id , settings : ImagesafeSettings = ImagesafeSettings()):
        log.debug( group_id )
        cache_item = self.get_cache( group_id, 'fimfic_group')
        if cache_item is not None:
            return cache_item
        try:
            if self.remaining_limit('groups') is not None and self.remaining_limit('groups') <= 0:
                return None
            search = 'include=founder&fields[group]=name,description_html,num_members,num_stories,nsfw,open,hidden,date_created,icon&fields[user]=id,name,avatar'
            base_url = 'https://www.fimfiction.net/api/v2/groups/{}?'.format( group_id )
            headers = await self.fimfic_header()
            async with self.session.request("GET", base_url + search, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    return None
                website = await resp.json()
                self.record_limits(resp.headers, 'groups')
            if 'error' in website:
                return None
            if 'data' in website:
                if len(website['data']) == 0:
                    return None
                website = jsonapi_unflatten(website)
                self.add_to_cache( group_id, website['group'][group_id], 'fimfic_group')
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic group data'
            return website['group'][group_id]
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
            log.exception(e)
            return None

    async def get_bookshelf_ids( self, bookshelf_id , settings : ImagesafeSettings = ImagesafeSettings(), page : int = 1):
        log.debug( bookshelf_id )
        try:
            if self.remaining_limit('bookshelves') is not None and self.remaining_limit('bookshelves') <= 0:
                log.debug('Out of bookshelf API calls.')
                return None
            search = '?include=author&page[size]=100&page[page]=' + str(page)
            base_url = 'https://www.fimfiction.net/api/v2/bookshelves/{}/items'.format( bookshelf_id )
            log.debug('URL: ' + base_url)
            headers = await self.fimfic_header()
            async with self.session.request("GET", base_url + search, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    return None
                website = await resp.json()
                self.record_limits(resp.headers, 'bookshelves')
            if 'error' in website:
                log.debug('Error in website: ' + str(website))
                return None
            if 'data' in website:
                if len(website['data']) == 0:
                    log.debug('No data in website.')
                    return None
                output = []
                for bookshelf_item in website['data']:
                    if not bookshelf_item:
                        continue
                    if 'relationships' in bookshelf_item:
                        if 'story' in bookshelf_item['relationships']:
                            output.append(bookshelf_item['relationships']['story']['data']['id'])
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic group data'
            return output
        except:
            raise
            return None

    async def get_blogs( self, search_list , settings : ImagesafeSettings = ImagesafeSettings()):
        if self.remaining_limit('blog-posts') is not None and self.remaining_limit('blog-posts') <= 0:
            return None
        filters = []
        if 'query' in search_list:
            filters.append('query={}'.format(search_list['query']))
            if 'sort' not in search_list:
                search_list['sort'] = '-relevance'
        if 'filters' in search_list:
            for filter_name, options in search_list['filters'].items():
                filters.append('filter[{0}]={1}'.format(filter_name,','.join(map(str,options))))
        if not filters:
            return None
        if 'include' in search_list:
            includes = [ 'include=' + search_list['include'] ]
        else:
            includes = [ 'include=author' ]
        if 'page' in search_list:
            page_size = int(search_list['page'])
            page = [ 'page[size]=' + str(search_list['page']) ]
        else:
            page = [ 'page[size]=100' ]
            page_size = 100
        fields = []
        if 'fields' in search_list:
            for field_name, options in search_list['fields'].items():
                fields.append('fields[{0}]={1}'.format(field_name,','.join(options)))
        if not fields:
            fields = [ 'fields[user]=date_joined,id,name,avatar,bio', 'fields[blog_post]=title,date_posted,content_html,num_views,tags,tagged_story,author' ] 
        if 'sort' in search_list:
            sorts = [ 'sort={}'.format(search_list['sort']) ]
        else:   
            sorts = [ 'sort=-date_posted' ]
        if 'max_results' in search_list:
            max_results = search_list['max_results']
        else:
            max_results = None #page_size - 1
        url = 'https://www.fimfiction.net/api/v2/blog-posts?' + '&'.join( filters + fields + sorts + includes + page)
        cursor = ''
        website = {}
        order = []
        loop = asyncio.get_event_loop()
        newest_datetime = None
        amount_returned = 0
        while True:
            log.debug(url + cursor)
            headers = await self.fimfic_header()
            async with self.session.request("GET", url + cursor, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    return None
                output_returned = await resp.json()
                self.record_limits(resp.headers, 'blog-posts')
            if 'error' in output_returned:
                return None
            else:
                if 'data' in output_returned:
                    if len(output_returned['data']) == 0:
                        break
                    else:
                        log.debug(output_returned['meta'])
                        log.debug(output_returned['links'])
                        cursor = re.findall(r'&page\[cursor\]=[^&]+', unquote(output_returned['links']['next']))[-1]
                        for blogs in output_returned['data']:
                            order.append(blogs['id'])
                        if not website:
                            log.debug('First loop, adding directly to website')
                            website = jsonapi_unflatten(output_returned)
                            output = { 'blog_post' : {} }
                            for blog in website['blog_post'].values():
                                blog['simple'] = False
                                output['blog_post'][blog['id']] = blog
                                self.add_to_cache( blog['id'], blog, 'fimfic_blog') #Wooo cache!
                            website = output
                            break   #because fimfic still doesn't support paging through results
                        else:
                            log.debug('Additional loop')
                            for blog in jsonapi_unflatten(output_returned)['blog_post'].values():
                                log.debug('Looping a story:' + blog['id'])
                                website['blog_post'][blog['id']] = blog
                        amount_returned += len(output_returned['data'])
                        if max_results is not None and max_results:
                            if max_results <= amount_returned:
                                break
                        if len(output_returned['data']) < page_size:
                            break
                        else:
                            continue
                else:
                    return None
            break
        website['order'] = order
        if 'blog_post' in website:
            log.debug(len(website['blog_post']))
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic search data'
            return website
        else:
            return None

    async def get_blog( self, blog_id , settings : ImagesafeSettings = ImagesafeSettings()):
        log.debug( blog_id )
        cache_item = self.get_cache( blog_id, 'fimfic_blog')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic blog data from cache'
            return cache_item
        try:
            if self.remaining_limit('blog-posts') is not None and self.remaining_limit('blog-posts') <= 0:
                return None
            search = 'include=author&fields[blog_post]=title,date_posted,content_html,num_views,tags,tagged_story,author&fields[user]=date_joined,id,name,avatar,bio'
            base_url = 'https://www.fimfiction.net/api/v2/blog-posts/{}?'.format( blog_id )
            headers = await self.fimfic_header()
            async with self.session.request("GET", base_url + search, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    return None
                website = await resp.json()
                self.record_limits(resp.headers, 'blog-posts')
            if 'error' in website:
                return None
            if 'data' in website:
                if len(website['data']) == 0:
                    return None
                website = jsonapi_unflatten(website)
                log.debug(website)
                self.add_to_cache( blog_id, website['blog_post'][blog_id], 'fimfic_blog')
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic blog data'
            return website['blog_post'][blog_id]
        except:
            return None

    async def get_user(self, user_id, settings : ImagesafeSettings = ImagesafeSettings()):
        log.debug( user_id )
        cache_item = self.get_cache( user_id, 'fimfic_user')
        if cache_item is not None:
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic user data from cache'
            return cache_item
        try:
            if self.remaining_limit('users') is not None and self.remaining_limit('users') <= 0:
                return None
            fields = [ 'fields[user]=date_joined,id,name,avatar,bio,bio_html,color' ]
            base_url = 'https://www.fimfiction.net/api/v2/users/{user}?'.format(user=user_id)
            loop = asyncio.get_event_loop()
            newest_datetime = None
            url = base_url + '&'.join( fields )
            headers = await self.fimfic_header()
            async with self.session.request("GET", url, headers=headers) as resp: 
                try:
                    resp.raise_for_status()      
                except:
                    log.error('Bad status: ' + str(resp.status))
                    return None
                website = await resp.json()
                self.record_limits(resp.headers, 'users')
            log.debug( website )
            if 'error' in website:
                log.debug( 'error' )
                return None
            if 'data' in website:
                if len(website['data']) == 0:
                    log.debug( 'No data' )
                    return None 
            else:
                log.debug( 'Panic' )
                return None
            website = jsonapi_unflatten(website)
            log.debug(website)
            self.add_to_cache( user_id, website['user'][user_id], 'fimfic_user')
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched Fimfic user data'
            return website['user'][user_id]
        except:
            return None

    async def get_furaffinity(self, post_id : str = None, artist : str = None, settings : ImagesafeSettings = ImagesafeSettings()):
        fa_cookies = await self.config.fa_cookies() #load FurAffinity cookies
        log.debug( post_id )
        log.debug( artist )
        if not fa_cookies:
            log.error( 'No FurAffinity cookies' )
            return None, None
        headers = {'user-agent' : user_agent}
        loop = asyncio.get_event_loop()
        target_id = None
        if artist is None and post_id is not None:
            target_id = post_id
            log.debug(target_id)
        else:
            cache_item = self.get_cache( '{} - {}'.format(post_id,artist), 'fasearch')
            if cache_item is not None:
                target_id = cache_item
            else:
                userpage = 'https://www.furaffinity.net/gallery/{artist}/'.format(artist=artist) + '{page_num}'
                #log.debug( userpage )
                post_page = None
                page_num = 1
                breaker = False
                while not breaker:
                    async with self.session.request("GET", userpage.format(page_num=page_num), headers=headers, cookies=fa_cookies) as resp: 
                        try:
                            resp.raise_for_status()      
                        except:
                            log.error('Bad status: ' + str(resp.status))
                            return None
                        text_data = await resp.text()
                        soup = BeautifulSoup(text_data, 'html.parser')
                    for img in soup.find_all('img'):
                        if img is None:
                            continue
                        data = re.search(r'([0-9]+)@[24]00-([0-9]+)\.',img.get('src'))
                        #log.debug(data)
                        #log.debug(img)
                        if data is not None:
                            self.add_to_cache( '{} - {}'.format(data.group(2),artist), data.group(1), 'fasearch')
                        if post_id in img.get('src'):
                            #log.debug(img)
                            log.debug('Found hit!')
                            pattern = self.regex_fasmallimage.fullmatch('https:' + img.get('src'))
                            #log.debug(pattern)
                            if pattern is None:
                                continue
                            target_id = pattern.group(1)
                            log.debug(target_id)
                            breaker = True
                    if post_id not in text_data:
                        page_num += 1
                        if soup.find('div', id="no-images") or page_num > 50:
                            break
                        continue
                    else:
                        break
                if not breaker or target_id is None:
                    userpage = 'https://www.furaffinity.net/scraps/{artist}/'.format(artist=artist) + '{page_num}'
                    #log.debug( userpage )
                    post_page = None
                    page_num = 1
                    breaker = False
                    while not breaker:
                        async with self.session.request("GET", userpage.format(page_num=page_num), headers=headers, cookies=fa_cookies) as resp: 
                            try:
                                resp.raise_for_status()      
                            except:
                                log.error('Bad status: ' + str(resp.status))
                                return None
                            text_data = await resp.text()
                            soup = BeautifulSoup(text_data, 'html.parser')
                        for img in soup.find_all('img'):
                            if img is None:
                                continue
                            data = re.search(r'([0-9]+)@[24]00-([0-9]+)\.',img.get('src'))
                            log.debug(data)
                            log.debug(img)
                            if data is not None:
                                self.add_to_cache( '{} - {}'.format(data.group(2),artist), data.group(1), 'fasearch')
                            if post_id in img.get('src'):
                                #log.debug(img)
                                log.debug('Found hit!')
                                pattern = self.regex_fasmallimage.fullmatch('https:' + img.get('src'))
                                #log.debug(pattern)
                                if pattern is None:
                                    continue
                                target_id = pattern.group(1)
                                log.debug(target_id)
                                breaker = True
                        if post_id not in text_data:
                            page_num += 1
                            if soup.find('div', id="no-images") or page_num > 50:
                                break
                            continue
                        else:
                            break
                    if not breaker or target_id is None:
                        return None, None
                self.add_to_cache( '{} - {}'.format(post_id,artist), target_id, 'fasearch')
        cache_item = self.get_cache( target_id, 'furaffinity')
        if cache_item is not None:
            #log.debug(cache_item)
            if settings.log_data is not None:
                settings.log_data.log = 'Fetched furaffinity data from cache'
            return ( cache_item['soup'], cache_item['url'] )
        text_data = await self.get_website('https://www.furaffinity.net/full/{target_id}/'.format(target_id=target_id), cookies=fa_cookies)
        if not text_data.website or 'fatal system error' in text_data.website.lower():
            return None, None
        soup = BeautifulSoup(text_data.website, 'html.parser')
        self.add_to_cache( target_id, { 'soup' : soup, 'url' : 'https://www.furaffinity.net/full/{target_id}/'.format(target_id=target_id) }, 'furaffinity')
        if settings.log_data is not None:
            settings.log_data.log = 'Fetched furaffinity data'
        return ( soup, 'https://www.furaffinity.net/full/{target_id}/'.format(target_id=target_id) )

    async def get_saucenao(self, url : str, settings : ImagesafeSettings = ImagesafeSettings()):
        assert url
        cache_item = self.get_cache( 'url', 'saucenao')
        log.debug(settings)
        log.debug(cache_item)
        if cache_item is not None:
            return cache_item['source']
        api_key = await self.config.saucenao_api_key()
        if not api_key:
            return None
        search = 'https://saucenao.com/search.php?db=999&output_type=2&api_key={api_key}&url={url}&numres=1'
        url_mod = quote_plus(url)
        log.debug(search.format(api_key = api_key, url = url_mod))
        search = URL(search.format(api_key = api_key, url = url_mod), encoded = True)
        result = await self.get_data(search)
        log.debug(result)
        if not result:
            return url
        if 'header' not in result:
            return None
        self.metrics['saucenao'] = str( result['header']['long_remaining'] ) + '/' + str(result['header']['long_limit'])
        if int(result['header']['status']) != 0:
            return None
        if int(result['header']['results_returned']) != 1:
            return None
        if 'results' not in result:
            return None
        if not result['results']:
            return None
        result = result['results'][0]
        if float(result['header']['similarity']) < 90.0:
            return None
        if 'da_id' in result['data']:
            source = 'http://fav.me/{}'.format(result['data']['da_id'])
        else:
            source = result['data']['ext_urls'][0]
        self.add_to_cache( url, { 'source' : source }, 'saucenao')
        return source

    #The following functions add data to an ImageImbed object given image data (usually gained from data sources)    
    async def add_furaffinity(self, soup : BeautifulSoup, url : str, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        main_data = soup.find('table',class_='maintable')
        #log.debug(main_data)
        log.debug(url)
        img_url = 'http:' + main_data.find('img',id='submissionImg').get('src')
        if settings.image_data is not None:
            image_data2 = await self.get_image_data(img_url, { 'User-Agent' : user_agent})
            if image_data2 is not None:
                if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                    raise ValueError('Images don\'t match (AR)')
                if hamming_distance(settings.image_data['phash'],image_data2['phash']) > 4:
                    raise ValueError('Images don\'t match (pHash)')
                if self.hash_storage is not None:
                    data = dict(image_data2)
                    data['source'] = 'https://www.furaffinity.net/full/{id}/'
                    data['identifier'] = url.rsplit('/',2)[1]
                    self.hash_storage.add(data)
        if url in settings.embed.sources:
            return None
        settings.embed.sources = url
        if settings.log_data is not None:
            settings.log_data.log = 'Added furaffinity data at: ' + str(confidence)
        log.debug(confidence)
        #log.debug(soup.prettify())
        #log.debug(main_data)
        settings.embed.set_title_url(url, confidence)
        settings.embed.set_title(main_data.find('img',class_='imgresizer').get('alt'), 10)
        settings.embed.set_artist(main_data.find('img',class_='avatar').get('alt'),confidence)
        settings.embed.set_artist_url('https://www.furaffinity.net/user/{user}'.format(user=main_data.find('img',class_='avatar').get('alt')),confidence)
        image_data = main_data.find('table',class_='maintable')
        settings.embed.set_artist_image('http:' + image_data.find('img',class_='avatar').get('src'),confidence)
        for td in image_data.find_all('td',class_='alt1'):
            if 'Submission Information' in str(td):
                continue
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True  
            log.debug(td)   
            if td.code:
                td.code.name = 'blockquote'
            description = td.prettify()
            settings.embed.set_description(text_maker.handle(description), confidence)
        image_url = 'http:' + main_data.find('img',id='submissionImg').get('src')
        settings.embed.set_image(image_url,confidence)
        if image_url and settings.primary:
            settings.primary_image = image_url
            settings.embed.links = "hash:" + image_url
        if 'general' not in soup.find('img',alt=re.compile(r'\S+ [rR]ating')).get('alt').lower():
            settings.embed.set_explicit('explicit', confidence)
        if confidence < 5:
            settings.embed.add_link('FurAffinity', url)
        else:
            settings.image = True
        return True

           
    async def add_e621(self, image_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if settings.image_data is not None:
            image_data2 = await self.get_image_data(image_json['file_url'], { 'User-Agent' : user_agent})
            if image_data2 is not None:
                if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                    raise ValueError('Images don\'t match (AR)')
                if hamming_distance(settings.image_data['phash'],image_data2['phash']) > 4:
                    raise ValueError('Images don\'t match (pHash)')
                if self.hash_storage is not None:
                    data = dict(image_data2)
                    data['source'] = 'https://e621.net/post/show/{id}/'
                    data['identifier'] = image_json['id']
                    self.hash_storage.add(data)
        if 'https://e621.net/post/show/{}/'.format(image_json['id']) in settings.embed.sources:
            return None
        settings.embed.sources = 'https://e621.net/post/show/{}/'.format(image_json['id'])
        if settings.log_data is not None:
            settings.log_data.log = 'Added e621 data at: ' + str(confidence)
        if settings.explicit:
            settings.embed.set_title_url('https://e926.net/post/show/{}/'.format(image_json['id']), confidence)
        else:
            settings.embed.set_title_url('https://e621.net/post/show/{}/'.format(image_json['id']), confidence)
        if 'artist' in image_json and image_json['artist']:
            settings.embed.set_artist(image_json['artist'][0], confidence)
            settings.embed.set_artist_url('https://e621.net/post/index/1/' + image_json['artist'][0], confidence)
        settings.embed.set_title(image_json['id'], 0)
        settings.embed.set_image(image_json['file_url'], confidence)
        log.debug(image_json['rating'])
        if image_json['rating'].startswith('e'):
            if re.search(r'(?:\s|^)(cub|foalcon|young)(?:\s|$)', image_json['tags']) is not None:
                raise FoalconException('https://e621.net/post/show/{}/'.format(image_json['id']))
            settings.embed.set_explicit('explicit', confidence)
        if image_json['file_url'] and settings.primary:
            settings.primary_image = image_json['file_url']
            settings.embed.links = "hash:" + image_json['file_url']
        if 'sources' in image_json:
            if image_json['sources']:
                log.debug(image_json['sources'])
                for source in image_json['sources']:
                    if 'tumblr' in source: #This is to stop links to potential porn blogs
                        continue
                    settings.embed.links = source
                    settings.embed.set_source('Source',source,0)
        if confidence < 5:
            if settings.explicit:
                settings.embed.add_link('e621', 'https://e926.net/post/show/{}/'.format(image_json['id']))
            else:
                settings.embed.add_link('e621', 'https://e621.net/post/show/{}/'.format(image_json['id']))
        else:
            settings.image = True
        return True

    async def add_deviantart(self, image_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if settings.image_data is not None:
            image_data2 = await self.get_image_data(image_json['url'], { 'User-Agent' : user_agent})
            if image_data2 is not None:
                if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                    raise ValueError('Images don\'t match (AR)')
                if hamming_distance(settings.image_data['phash'],image_data2['phash']) > 4:
                    raise ValueError('Images don\'t match (pHash)')
                if self.hash_storage is not None:
                    data = dict(image_data2)
                    if 'fav.me' in image_json['page_url']:
                        data['source'] = 'http://fav.me/{id}'
                        data['identifier'] = image_json['page_url'].rsplit('/',1)[1]
                    else:
                        data['source'] = 'http://fav.me/{id}'
                        data['identifier'] = image_json['page_url'].rsplit('-',1)[1]
                    self.hash_storage.add(data)
        log.debug (image_json['url'])
        log.debug (settings.embed.sources)

        if image_json['url'] in settings.embed.sources:
            log.debug('Skipped adding DA')
            log.debug(str(settings.embed))
            return None
        log.debug('Added DA')
        settings.embed.sources = image_json['url']
        if settings.log_data is not None:
            settings.log_data.log = 'Added deviantart data at: ' + str(confidence)
        settings.embed.set_title_url(image_json['page_url'], confidence)
        settings.embed.set_artist(image_json['author_name'], 8)
        settings.embed.set_artist_url(image_json['author_url'], confidence)
        settings.embed.set_title(image_json['title'], 8)
        settings.embed.set_image(image_json['url'], confidence)
        if 'description' in image_json:
            content = image_json['description']
            log.debug(content)
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True
            content = content.replace('https://www.deviantart.com/users/outgoing?','')
            content = text_maker.handle(content)
            if len(content) > 400:
                log.debug( content )
                content = content[:400]
                log.debug( content )
                content = content.rsplit('\n', maxsplit=1)[0].rstrip() + '\n\nContinued at source...'
                log.debug( content )
            log.debug(content)
            settings.embed.set_long_description(content, confidence)
        if image_json['url'] and settings.primary:
            settings.primary_image = image_json['url']
            settings.embed.links = "hash:" + image_json['url']
        if image_json['safety'] == 'adult':
            settings.embed.set_explicit('explicit', confidence)
        if confidence < 5:
            settings.embed.add_link('DeviantArt', image_json['page_url'])
        else:
            settings.image = True
        return True
            
    async def add_derpibooru(self, image_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.primary:
            log.debug("Primary == True")
            confidence = 5
        else:
            log.debug("Primary == False")
            confidence = 2
        log.debug(settings.image_data)
        if settings.image_data is not None:
            image_data2 = await self.get_image_data("https:" + image_json['representations']['full'], { 'User-Agent' : user_agent})
            log.debug(image_data2)
            if image_data2 is not None:
                if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                    raise ValueError('Images don\'t match (AR)')
                if hamming_distance(settings.image_data['phash'],image_data2['phash']) > 4:
                    raise ValueError('Images don\'t match (pHash)')
                if self.hash_storage is not None:
                    data = dict(image_data2)
                    data['source'] = 'https://derpibooru.org/{id}'
                    data['identifier'] = image_json['id']
                    self.hash_storage.add(data)
        if "https://derpibooru.org/{}".format(image_json['id']) in settings.embed.sources:
            return None
        settings.embed.sources = "https://derpibooru.org/{}".format(image_json['id'])
        if settings.log_data is not None:
            settings.log_data.log = 'Added derpibooru data at: ' + str(confidence)
        log.debug(confidence)
        if not image_json['file_name'] or len(image_json['file_name'].rsplit('.',1)[0]) < 4:
            image_json['file_name'] = 'Unknown.blarg'
        settings.embed.set_title(image_json['file_name'].split('.')[0], 0)
        settings.embed.set_title_url("https://derpibooru.org/{}".format(image_json['id']), confidence)
        r = re.compile('artist:.+')
        tags = image_json['tags'].split(',')
        artist_list = list(filter(r.search, tags))
        if artist_list:
            artist = artist_list[0].strip()[7:]
            settings.embed.set_artist(artist, confidence)
            artist_url = "https://derpibooru.org/search?q=artist%3a{}".format(quote_plus(artist))
            settings.embed.set_artist_url(artist_url, confidence)
        else:
            settings.embed.set_artist('Unknown', 0)
        settings.embed.set_description(derpi_markup_2_markdown(image_json['description']), confidence)
        settings.embed.set_image("https:" + image_json['representations']['large'], confidence)
        tag_array = image_json['tags'].split(',')
        explicit_image = re.search(r'(?:,|^)\s?(explicit|questionable)\s?(?:,|$)', image_json['tags'])
        log.debug(explicit_image)
        if explicit_image is not None:
            log.debug('Flagged as: ' + explicit_image.group(1))
            settings.embed.set_explicit(explicit_image.group(1), confidence)

        if re.search(r'(?:,|^)\s?(foalcon)\s?(?:,|$)', image_json['tags']) is not None:
            raise FoalconException("https://derpibooru.org/{}".format(image_json['id']))

        """Note: all these have a "confidence < 5" if block.
        This is used specifically when they are not the primary
        source."""
        #Because Derpibooru allows source citation, we can possibly get extra info from just the source filename OR from the source URL
        
        if image_json['file_name']:
            settings.embed.links = "file:" + image_json['file_name']
        if settings.primary:
            settings.primary_image = "https:" + image_json['representations']['full']
            settings.embed.links = "hash:" + "https:" + image_json['representations']['full']
            settings.image = True

            #await self.extract_filename(embed = embed, file_name = image_json['file_name'], primary = False, log_data = log_data, explicit = explicit)
        #if re.fullmatch(r'(?:[-a-zA-Z0-9@:%_\+.~#?&//=]*).+?by.+?(?:-|_)(d[a-z0-9]+)(?:-pre|-fullview)?\.[0-9a-zA-Z]{1,9}' ,image_json['file_name']) is not None:
        #    code = re.findall(r'(?:[-a-zA-Z0-9@:%_\+.~#?&//=]*).+?by.+?(?:-|_)(d[a-z0-9]+)(?:-pre|-fullview)?\.[0-9a-zA-Z]{1,9}', image_json['file_name'])[0]
        #    url = decode_deviant_filename(code)
        #    if url is not None:
        #        data = await self.get_deviant_data(url, log_data = log_data)
        #        if data:
        #            log.debug(data)
        #            self.add_deviantart(data, embed, primary = False, log_data = log_data)
        #            return
        if image_json['source_url']:
            if 'tumblr' not in image_json['source_url']: #This is to stop links to potential porn blogs
                settings.embed.links = image_json['source_url']
                #await self.extract_embed(image_json['source_url'],embed = embed, primary = False, log_data = log_data, explicit = explicit)
                settings.embed.set_source('Source', image_json['source_url'].replace('trixiebooru','derpibooru'), 0)
        if confidence < 5:
            settings.embed.add_link('Derpibooru', "https://derpibooru.org/{}".format(image_json['id']))
        return True

    def add_archiveofourown(self, story, settings : ImagesafeSettings = ImagesafeSettings()):
        try:
            if settings.primary:
                confidence = 5
            else:
                confidence = 2
            if settings.log_data is not None:
                settings.log_data.log = 'Added ao3 data at: ' + str(confidence)
            log.debug(story.json())
            settings.embed.set_title_url(story.url, confidence)
            settings.embed.set_artist(story.author, 8)
            modified_author = re.match(r'(\S+) \((\S+)\)',story.author)
            if modified_author is not None:
                settings.embed.set_artist_url("https://archiveofourown.org/users/{0}/pseuds/{1}".format(modified_author.group(2), modified_author.group(1)), 8)
            else:
                settings.embed.set_artist_url("https://archiveofourown.org/users/{0}/pseuds/{0}".format(story.author), 8)
            tags = {}
            tags['character'] = story.characters
            tags['cataory'] = story.category
            tags['rating'] = story.rating
            tags['content'] = story.relationship
            tags['language'] = [story.language]
            tags['warning'] = story.warnings
            tags['fandom'] = story.fandoms
            for tag,tag_list in tags.items():
                if not tag_list:
                    continue
                tag_names = []
                for this_tag in tag_list:
                    tag_names.append('[{}]'.format(this_tag))
                while len(' '.join(tag_names)) > 1024:
                    tag_names.pop
                settings.embed.add_misc(tag.capitalize(), ' '.join(tag_names))
            settings.embed.set_title("{0} - ({1:,} words)".format(story.title.capitalize(),story.words), 8)
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True
            content = story.summary
            if len(content) > 400:
                log.debug( content )
                content = content[:400]
                log.debug( content )
                content = content.rsplit('</p>', maxsplit=1)[0].rstrip() + '<p>Continued at source...</p>'
                log.debug( content )
            settings.embed.set_long_description(text_maker.handle(content), confidence)
            for datum in story.rating:
                if datum.lower() == 'mature' or datum.lower() == 'explicit':
                    settings.embed.set_explicit('explicit', confidence)
                    break
            if confidence < 5:
                settings.embed.add_link('Ao3', story.url)
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            
            log.error( sys.exc_info())
            log.exception(e)
            raise
         
    async def add_fimfiction(self, story_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.image_data is not None:
            if 'cover_image' in story_json:
                image_data2 = await self.get_image_data(story_json['cover_image']['full'], { 'User-Agent' : user_agent})
                if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                    raise ValueError('Images don\'t match (AR)')
        if fixup_fimfic_urls(story_json['url']) in settings.embed.sources:
            return None
        settings.embed.sources = fixup_fimfic_urls(story_json['url'])
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if settings.log_data is not None:
            settings.log_data.log = 'Added fimfic story data at: ' + str(confidence)
        if confidence == 5:
            log.debug(story_json)
            settings.embed.set_title_url(fixup_fimfic_urls(story_json['url']), confidence)
            author = story_json['author'][0]  
            settings.embed.set_artist(author['name'], 8)
            settings.embed.set_artist_url("https://www.fimfiction.net/user/{0}/{1}".format(author['id'],quote_plus(author['name'])), 8)
            if not story_json['simple']:
                if 'avatar' in author:
                    avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                    for size in avatars:
                        if size in author['avatar']:
                            settings.embed.set_artist_image( author['avatar'][size], 8)
                            break
                if 'color' in story_json:
                    settings.embed.set_color(str(story_json['color']['hex']), confidence)
                if 'completion_status' in story_json:
                    settings.embed.add_misc("Status", story_json['completion_status'].capitalize())
                settings.embed.add_misc("Rating", story_json['content_rating'].capitalize())
                if 'tags' in story_json:
                    tags = {}
                    tags['character'] = {}
                    tags['genre'] = {}
                    tags['rating'] = {}
                    tags['content'] = {}
                    tags['series'] = {}
                    tags['warning'] = {}
                    tags['universe'] = {}
                    tags['unknown'] = {}
                    for story_tag in story_json['tags']:
                        if story_tag['type'] in tags:
                            tags[story_tag['type']][story_tag['id']] = story_tag
                        else:
                            tags['unknown'][story_tag['id']] = story_tag
                    for tag,tag_list in tags.items():
                        if not tag_list:
                            continue
                        tag_names = []
                        for tag_id,this_tag in tag_list.items():
                            if this_tag['name'].lower() in fimfic_tags:
                                this_tag['name'] = fimfic_tags[this_tag['name'].lower()]
                            tag_names.append('[{}]'.format(this_tag['name']))
                        settings.embed.add_misc(tag.capitalize(), ' '.join(tag_names))
            if 'name' in story_json:
                title = story_json['name']
            else:
                #this is a fix until name is the standard
                title = story_json['title']
            settings.embed.set_title("{0} - ({1:,} words)".format(title,story_json['num_words']), 8)
            settings.embed.set_description(story_json['short_description'], confidence)
            if 'cover_image' in story_json:
                settings.primary_image = story_json['cover_image']['full']
                settings.embed.set_image(story_json['cover_image']['full'], confidence)
                if settings.primary:
                    settings.embed.links = "hash:" + story_json['cover_image']['full']
            settings.story = True
        else:
            settings.embed.add_link('FimFiction', fixup_fimfic_urls(story_json['url'])) 
        return True      

    def add_fimfiction_blog(self, blog_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if fixup_fimfic_urls(blog_json['url']) in settings.embed.sources:
            return None
        settings.embed.sources = fixup_fimfic_urls(blog_json['url'])
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if settings.log_data is not None:
            settings.log_data.log = 'Added fimfic blog data at: ' + str(confidence)
        if confidence == 5:
            log.debug( blog_json )
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True
            url = fixup_fimfic_urls(blog_json['url'])
            if 'color' in blog_json:
                settings.embed.set_color(str(blog_json['color']['hex']), confidence)
            settings.embed.set_title_url(url, confidence)
            settings.embed.set_title( blog_json['title'], confidence)
            content = blog_json['content_html']
            if len(content) > 1600:
                log.debug( content )
                content = content[:1600]
                log.debug( content )
                content = content.rsplit('</p>', maxsplit=1)[0] + '<p>And more...</p>'
                log.debug( content )
            if 'spoiler' in content:
                soup = BeautifulSoup(content)
                while True:
                    spoiler = soup.find('span', class_ = 'spoiler')
                    log.debug(str(soup))
                    if spoiler is None:
                        break
                    new_spoiler = '||' + spoiler.get_text() + '||'
                    spoiler.replace_with(new_spoiler)
                content = str(soup)
            settings.embed.set_long_description(text_maker.handle(content.split('[page_break]',maxsplit=1)[0]), 8)
            if 'avatar' in blog_json['author'][0]:
                avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                for size in avatars:
                    if size in blog_json['author'][0]['avatar']:
                        settings.embed.set_artist_image( blog_json['author'][0]['avatar'][size], 8)
                        break
            settings.embed.set_artist(blog_json['author'][0]['name'], 8)
            settings.embed.set_artist_url(blog_json['author'][0]['url'], 8)
        else:
            settings.embed.add_link('FimFiction', fixup_fimfic_urls(blog_json['url']))  
        return True

    async def add_fimfiction_group(self, group_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if fixup_fimfic_urls(group_json['url']) in settings.embed.sources:
            return None
        settings.embed.sources = fixup_fimfic_urls(group_json['url'])
        if settings.image_data is not None:
            if 'icon' in group_json:
                avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                for size in avatars:
                    if size in group_json['icon']:
                        if group_json['icon'][size] is not None:
                            image_data2 = await self.get_image_data(group_json['icon'][size], { 'User-Agent' : user_agent})
                            if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                                raise ValueError('Images don\'t match (AR)')
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if settings.log_data is not None:
            settings.log_data.log = 'Added fimfic group data at: ' + str(confidence)
        if confidence == 5:
            log.debug( group_json )
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True
            url = fixup_fimfic_urls(group_json['url'])
            if 'color' in group_json:
                settings.embed.set_color(str(group_json['color']['hex']), confidence)
            settings.embed.set_title_url(url, confidence)
            settings.embed.set_title( group_json['name'], confidence)
            content = group_json['description_html']
            if len(content) > 1000:
                log.debug( content )
                content = content[:1000]
                log.debug( content )
                content = content.rsplit('</p>', maxsplit=1)[0] + '<p>...</p>'
                log.debug( content )
            if 'spoiler' in content:
                soup = BeautifulSoup(content)
                while True:
                    spoiler = soup.find('span', class_ = 'spoiler')
                    log.debug(str(soup))
                    if spoiler is None:
                        break
                    new_spoiler = '||' + spoiler.get_text() + '||'
                    spoiler.replace_with(new_spoiler)
                content = str(soup)
            settings.embed.set_long_description(text_maker.handle(content).replace('__',''), 8)
            if 'founder' in group_json:
                if 'avatar' in group_json['founder'][0]:
                    avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                    for size in avatars:
                        if size in group_json['founder'][0]['avatar']:
                            settings.embed.set_artist_image( group_json['founder'][0]['avatar'][size], 8)
                            break
                settings.embed.set_artist(group_json['founder'][0]['name'], 8)
                settings.embed.set_artist_url(group_json['founder'][0]['url'], 8)
            if 'icon' in group_json:
                avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                for size in avatars:
                    if size in group_json['icon']:
                        if group_json['icon'][size] is not None:
                            settings.embed.set_image( group_json['icon'][size], 8)
                        break
            if 'num_members' in group_json:
                settings.embed.add_misc('Members', '{0:,}'.format(group_json['num_members']))
            if 'num_stories' in group_json:
                settings.embed.add_misc('Stories', '{0:,}'.format(group_json['num_stories']))
            if 'nsfw' in group_json and group_json['nsfw']:
                settings.embed.add_misc('Rating','NSFW')
            else:
                settings.embed.add_misc('Rating','SFW')
            others = []
            if 'hidden' in group_json and group_json['hidden']:
                others.append('Hidden')
            if 'open' in group_json and group_json['open']:
                others.append('Taking Members')
            if others:
                settings.embed.add_misc('Info','\n'.join(others))
            settings.story = True
        else:
            settings.embed.add_link('FimFiction', fixup_fimfic_urls(group_json['url']))  
        return True
            
    async def add_fimfiction_user(self, user_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if fixup_fimfic_urls(user_json['url']) in settings.embed.sources:
            return None
        settings.embed.sources = fixup_fimfic_urls(user_json['url'])
        if settings.image_data is not None:
            if 'avatar' in user_json:
                avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                for size in avatars:
                    if size in user_json['avatar']:
                        image_data2 = await self.get_image_data(user_json['avatar'][size], { 'User-Agent' : user_agent})
                        if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                            raise ValueError('Images don\'t match (AR)')
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if settings.log_data is not None:
            settings.log_data.log = 'Added fimfic user data at: ' + str(confidence)
        if confidence == 5:
            log.debug( user_json )
            text_maker = html2text.HTML2Text()
            text_maker.body_width = 0
            text_maker.ignore_images = True
            url = fixup_fimfic_urls(user_json['url'])
            if 'color' in user_json:
                settings.embed.set_color(user_json['color']['hex'], confidence)
            settings.embed.set_title_url(url, confidence)
            settings.embed.set_title( user_json['name'], confidence)
            settings.embed.set_long_description(text_maker.handle(user_json['bio_html']), confidence)
            if 'avatar' in user_json:
                avatars = [ "512" , "384" , "256" , "192" , "128" , "96" , "64" , "48" , "32" , "16" ] 
                for size in avatars:
                    if size in user_json['avatar']:
                        settings.embed.set_image( user_json['avatar'][size], confidence)
                        if settings.primary:
                            settings.primary_image = user_json['avatar'][size]
                            settings.embed.links = "hash:" + user_json['avatar'][size]
                        break
        else:
            settings.embed.add_link('FimFiction', fixup_fimfic_urls(user_json['url'])) 
        return True
            
    async def add_inkbunny(self, image_json, settings : ImagesafeSettings = ImagesafeSettings()):
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if 'target' in image_json:
            target = image_json['target']
        else:
            target = 0
        if settings.image_data is not None:
            image_data2 = await self.get_image_data(image_json['files'][target]['file_url_full'], { 'User-Agent' : user_agent})
            if image_data2 is not None:
                if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['width']),1):
                    raise ValueError('Images don\'t match (AR)')
                if hamming_distance(settings.image_data['phash'],image_data2['phash']) > 4:
                    raise ValueError('Images don\'t match (pHash)')
                if self.hash_storage is not None:
                    data = dict(image_data2)
                    data['source'] = 'https://inkbunny.net/s/{id}/'
                    data['identifier'] = image_json['submission_id']
                    self.hash_storage.add(data)
        if 'https://inkbunny.net/s/{}/'.format(image_json['submission_id']) in settings.embed.sources:
            return None
        settings.embed.sources = 'https://inkbunny.net/s/{}/'.format(image_json['submission_id'])
        if settings.log_data is not None:
            settings.log_data.log = 'Added inkbunny data at: ' + str(confidence)
        log.debug(target)
        settings.embed.set_title_url('https://inkbunny.net/s/{}/'.format(image_json['submission_id']), confidence)
        settings.embed.set_artist(image_json['username'], confidence)
        settings.embed.set_artist_url('https://inkbunny.net/' + image_json['username'], confidence)
        settings.embed.set_title(image_json['title'], 8)
        settings.embed.set_image(image_json['files'][target]['file_url_full'], confidence)
        if settings.primary:
            if image_json['files'][target]['file_url_full']:
                settings.primary_image = image_json['files'][target]['file_url_full']
                settings.embed.links = "hash:" + image_json['files'][target]['file_url_full']
        settings.embed.set_artist_image(image_json['user_icon_url_large'],confidence)
        if image_json['rating_name'] != 'General':
            settings.embed.set_explicit('explicit', confidence)
        if confidence < 5:
            settings.embed.add_link('InkBunny', 'https://inkbunny.net/s/{}/'.format(image_json['submission_id']))
        else:
            settings.image = True
        return True
    


    async def add_opengraph(self, site_data : opengraph_py3.OpenGraph, settings : ImagesafeSettings = ImagesafeSettings()):
        if 'url' in site_data:
            log.debug(site_data['url'])
            log.debug(settings.embed.sources)
            if site_data['url'] in settings.embed.sources:
                log.debug('Skipped adding OpenGraph')
                log.debug(str(settings.embed))
                return None
            log.debug('Adding OpenGraph')
            settings.embed.sources = site_data['url']
        if not site_data.is_valid():
            return None
        if settings.primary:
            confidence = 5
        else:
            confidence = 2
        if 'image' not in site_data:
            return None
        log.debug(site_data['image'])
        log.debug(settings.embed.sources)
        if site_data['image'] in settings.embed.sources:
            log.debug('Skipped adding OpenGraph')
            log.debug(str(settings.embed))
            return None
        log.debug('Adding OpenGraph')
        settings.embed.sources = site_data['image']
        if 'http' not in site_data['image']:
            return None
        image_data2 = await self.get_image_data(site_data['image'], { 'User-Agent' : user_agent})
        if settings.image_data is not None and image_data2 is not None:
            if round(int(settings.image_data['dimensions']['width']) / int(settings.image_data['dimensions']['height']),1) != round(int(image_data2['dimensions']['width']) / int(image_data2['dimensions']['height']),1):
                raise ValueError('Images don\'t match (AR)')
            if hamming_distance(settings.image_data['phash'],image_data2['phash']) > 4:
                raise ValueError('Images don\'t match (pHash)')
        if settings.log_data is not None:
            settings.log_data.log = 'Added opengraph data at: ' + str(confidence)
        if 'url' in site_data:
            settings.embed.set_title_url(site_data['url'], confidence)
        if 'title' in site_data:
            settings.embed.set_title(site_data['title'], confidence)
        if 'description' in site_data:
            if not site_data['description'].startswith('DeviantArt is the world'):
                settings.embed.set_description(site_data['description'], confidence)
        settings.embed.set_image(site_data['image'], confidence)
        if settings.primary:
            settings.primary_image = site_data['image']
            settings.embed.links = "hash:" + site_data['image']
        if confidence < 5 and 'site_name' in site_data and 'url' in site_data:
            settings.embed.add_link(site_data['site_name'], site_data['url'])
        elif settings.primary:
            settings.image = True
        return settings.embed

    async def extract_filename(self, file_name : str = None, url : str = None, settings : ImagesafeSettings = ImagesafeSettings()):
        if file_name is None and url is not None:
            file_name = url.rsplit('/')[-1]
        elif file_name is None and url is None:
            raise ValueError("Must have either file_name or url")
        log.debug("Filename: " + str(file_name))
        log.debug("URL: " + str(url))
        if url is not None:
            header = { 'User-Agent' : user_agent}
            settings.image_data = await self.get_image_data(url, header)
        if settings.log_data is not None:
            if url is not None:
                settings.log_data.log = 'Looking for url: <' + str(url) + '>'
            else:
                settings.log_data.log = 'Looking for file named: ' + str(file_name)
        pattern = re.fullmatch(r'([0-9]+)(?:__\S+)?\.\S+' ,file_name)
        if pattern is not None and settings.image_data is not None:
            # Derpibooru
            log.debug('Derpi')
            image_id = pattern.group(1)
            output = await self.get_derpi_data(image_id, settings = settings)
            log.debug("Got output")
            if output:
                if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                    log.debug("Made embed")
                    return settings.embed
        pattern = re.fullmatch(r'\S+(?:-|_)(d[a-z0-9]+)(?:-pre|-fullview)?\.\S+', file_name)
        if pattern is None:
            pattern = re.fullmatch(r'(d[a-z0-9]+)[^a-z0-9].+\.[0-9a-zA-Z]+', file_name)
        if pattern is not None:
            # DeviantArt
            log.debug('DA')
            code = pattern.group(1)
            log.debug(code)
            url = 'http://fav.me/{code}'.format(code=code)
            log.debug(url)
            output = await self.get_deviant_data(url, settings = settings)
            if output:
                pattern = self.regex_deviantart_raw1.search(output['url'])
                if pattern is None:
                    pattern = self.regex_deviantart_raw2.search(output['url'])
                if pattern is not None:
                    url = decode_deviant_filename(pattern.group(1))
                log.debug('da url: ' + url)
                log.debug("Got output")
                if await self.add_data(self.add_deviantart(output, settings = settings), settings):
                    log.debug("Made embed")
                    return settings.embed
        pattern =  re.fullmatch(r'\w{4}-[0-9]+-([0-9]+)-(?:tiny|medium|large|full)\.\S+', file_name)
        if pattern is not None:
            # Fimfiction story image
            log.debug('FimStory')
            output = await self.get_story(pattern.group(1), settings = settings)
            log.debug(output)
            if output:
                log.debug("Got output")
                abort = False
                log.debug(output['cover_image']['full'].rsplit('/',1)[1].rsplit('-',1)[0])
                log.debug(file_name.rsplit('-',1)[0])
                if output['cover_image']['full'].rsplit('/',1)[1].rsplit('-',1)[0] != file_name.rsplit('-',1)[0]:
                    abort = True
                if not abort:
                    if await self.add_data(self.add_fimfiction(output, settings = settings), settings):
                        log.debug("Made embed")
                        return settings.embed
        pattern = re.fullmatch(r'\w{4}-[0-9]+-([0-9]+)-[0-9]{1,3}\.\S+', file_name)
        if pattern is not None:
            # Fimfiction user/group image
            log.debug('Fimuser/group')
            unknown_id = pattern.group(1)
            log.debug(unknown_id)
            output = None
            loop = asyncio.get_event_loop()
            log.debug('Image: ' + file_name.split('.',1)[0])
            response = loop.run_in_executor(None, partial(requests.request,"HEAD", 'https://cdn-img.fimfiction.net/user/' + file_name.split('.',1)[0]))
            website = await response
            if website.status_code == requests.codes.ok:
                type = 'user'
                output = await self.get_user(unknown_id, settings = settings)
            else:
                response = loop.run_in_executor(None, partial(requests.request,"HEAD", 'https://cdn-img.fimfiction.net/group/' + file_name.split('.',1)[0]))
                website = await response
                if website.status_code == requests.codes.ok:
                    type = 'group'
                    output = await self.get_group(unknown_id, settings = settings)
            log.debug(type)
            log.debug(output)
            if output:
                log.debug("Got output")
                abort = False
                if type == 'user':
                    log.debug(output['avatar'][file_name.split('.',1)[0].rsplit('-',1)[1]])
                    log.debug(file_name.split('.',1)[0])
                    if output['avatar'][file_name.split('.',1)[0].rsplit('-',1)[1]].rsplit('/',1)[1] != file_name.split('.',1)[0]:
                        abort = True
                if type == 'group':
                    log.debug(output['icon'][file_name.split('.',1)[0].rsplit('-',1)[1]])
                    log.debug(file_name.split('.',1)[0])
                    if output['icon'][file_name.split('.',1)[0].rsplit('-',1)[1]].rsplit('/',1)[1] != file_name.split('.',1)[0]:
                        abort = True
                if not abort:
                    if type == 'user':
                        success = await self.add_data(self.add_fimfiction_user(output, settings = settings), settings)
                    elif type == 'group':
                        success = await self.add_data(self.add_fimfiction_group(output, settings = settings), settings)
                    if success:
                        if type == 'user':
                            pass
                        elif type == 'group':
                            pass
                            #embed.type = 'story'
                        log.debug("Made embed")
                        return settings.embed
        pattern = re.fullmatch(r'([a-fA-F\d]{32})\.\S+' ,file_name)
        if pattern is not None:
            #e621
            log.debug('e621')
            image_md5 = pattern.group(1)
            log.debug(image_md5)
            output = await self.get_e621_data(image_md5, settings = settings)
            log.debug(output)
            if output:
                image_id = output['id']
                if output['status'].lower() != 'deleted':
                    log.debug("Got output")
                    if await self.add_data(self.add_e621(output, settings = settings), settings):
                        log.debug("Made embed")
                        return settings.embed
        if url is not None:
            pattern = re.fullmatch(r'([0-9]+)_\S+_(?:[0-9]{4}_)?\S+',file_name)
            log.debug(pattern)
            if pattern is not None:
                #Inkbunny images
                log.debug('inkbunny')
                #url = 'https://tx.ib.metapix.net/files/full/{first_four}/{filename}'
                #url = url.format(filename=datum['filename'],first_four=pattern.group(1)[:4])
                log.debug(url)
                output = await self.get_inkbunny(url=url, settings = settings)
                if output:
                    image_id = output['submission_id']
                    log.debug(output)
                    if await self.add_data(self.add_inkbunny(output, settings = settings), settings):
                        log.debug("Made embed")   
                        return settings.embed
                
                
            
            log.debug(settings.image_data)
            if settings.image_data is not None:
                output = await self.extract_from_hash(url = url, settings = settings)
                if output is not None:
                    return output
        return None
    
    async def search_hashes(self, settings : ImagesafeSettings = ImagesafeSettings()):
        if self.hash_storage is None:
            return False
        source = ''
        total_results = []
        result = self.hash_storage.sha512(settings.image_data['sha512'])
        log.debug('Checking sha512')
        log.debug(settings.image_data['sha512'])
        if result:
            log.debug('Hit on sha512!')
            source += 'sha512 '
            total_results += result
        log.debug('Checking md5')
        log.debug(settings.image_data['md5'])
        result = self.hash_storage.md5(settings.image_data['md5'])
        if result:
            log.debug('Hit on md5!')
            source += 'md5 '
            total_results += result    
        log.debug('Checking phash')
        log.debug(settings.image_data['phash'])
        result = self.hash_storage.phash(settings.image_data['phash'])
        if result:
            log.debug('Hit on phash!')
            source += 'pHash '
            total_results += result
        if not result:
            return False
        log.debug(total_results)
        hash_check = settings.embed.hash
        settings.skip_direct = False
        settings.enable_fimfic = True
        for datum in total_results:
            url = datum['source'].format(id=datum['identifier'])
            log.debug(url)
            if url in settings.embed.sources:
                log.debug('Already discovered')
                continue
            url, output = await self.extract_embed(url, settings = settings)
            if output:
                log.debug('Got output from search!')
                settings.embed.sources = url
        if settings.embed.hash != hash_check:
            log.debug('Got output from hash storage!')
            if settings.log_data is not None:
                settings.log_data.log = 'Retreived image data from local hash DB: ' + source
            return settings.embed
        else:
            log.debug('Failed to get output from search.')
            return False

    async def extract_from_hash(self, url : str = '', settings : ImagesafeSettings = ImagesafeSettings()):
        """Whose porn is this porn?"""
        log.debug(url)
        #search internal hashes
        embed_output = None
        if settings.image_data:
            output = await self.search_hashes(settings = settings)
            if output:            
                embed_output = output
            #search derpibooru for the sha512
            output = await self.get_derpi_data_from_hash(settings = settings)
            log.debug(output)
            if output: #Is this your porn? Or... 
                image_id = output['id'] #... did it become your porn?
                log.debug("Got output")
                if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                    log.debug("Made embed") 
                    embed_output = settings.embed #at least you got some nice porn out of the deal
                                        
            #search InkBunny for the md5  
            output = await self.get_inkbunny(md5_checksum = settings.image_data['md5'], settings = settings)
            if output: #That porn is wiped—out
                image_id = output['submission_id'] #Or not. Wouldn't want to masturbate to it
                log.debug("Got output")
                if await self.add_data(self.add_inkbunny(output, settings = settings), settings):
                    log.debug("Made embed")
                    log.debug("Made embed")   
                    embed_output = settings.embed #this porn just came here
                    
            #search e621 for the md5 
            output = await self.get_e621_data(settings.image_data['md5'], settings = settings)
            if output: #do you know whose porn that is?
                log.debug(output)
                if 'id' in output:
                    image_id = output['id']
                    if output['status'] != 'deleted':
                        log.debug("Got output")
                        if await self.add_data(self.add_e621(output, settings = settings), settings):
                            log.debug("Made embed")   
                            embed_output = settings.embed   #have a "good" time ;) ;) ;)  
        if url and not settings.hash_source:
            if not any('https://derpibooru.org' in x for x in settings.embed.sources):
                image_id = await self.get_derpi_reverse(url, settings = settings)
                if image_id is not None:
                    output = await self.get_derpi_data(image_id, settings = settings)
                    if output:
                        if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                            log.debug("Made embed")   
                            embed_output = settings.embed
                            settings.hash_source = True
            # if settings.primary and not embed_output:
            #     url = await self.get_saucenao(url, settings = settings)
            #     if url:
            #         # settings.skip_direct = False
            #         # settings.enable_fimfic = True
            #         # settings.primary = False
            #         url, output = await self.extract_embed(url, settings = settings(skip_direct = False, enable_fimfic = True, primary = False, hash_source = True))
            #         if output:
            #             embed_output = output
        
        return embed_output #they don't know whose porn that is :( 
            
    async def extract_embed(self, input : str = None, settings : ImagesafeSettings = ImagesafeSettings() ):
        """Heavy lifting function! This takes individual words from a message
        and checks them for being a trigger, a verifyable source URL, or
        a changeling. Only two of the afforementioned checks are actually done."""
        if input is None:
            raise ValueError #Bork bork bork!
        log.debug(input)
        #Matching derpi/da/e621=#### results
        pattern = self.regex_equals.findall(input)
        if pattern:
            if settings.log_data is not None:
                settings.log_data.log = 'Looking for trigger: ' + str(input)
            pattern = pattern[0]
            log.debug(pattern)
            image_id = pattern[2]
            log.debug(image_id)
            if pattern[0] == 'derpi':
                output = await self.get_derpi_data(image_id, settings = settings)
                if not output:
                    log.debug("No output")
                    return '<image {} not found>'.format(image_id), None
                image_id = output['id']
                log.debug("Got output")
                if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                    settings.embed.force = True
                    log.debug("Made embed")
                    return '<{}>'.format(settings.embed.url), settings.embed
            elif pattern[0] == 'e621':
                output = await self.get_e621_data(image_id, settings = settings)
                if not output:
                    log.debug("No output")
                    return '<image {} not found>'.format(image_id), None
                log.debug(output)
                image_id = output['id']
                if output['status'] is 'deleted':
                    log.debug("Image has been deleted")
                    return '<image {} has been deleted>'.format(image_id), None
                log.debug("Got output")
                if await self.add_data(self.add_e621(output, settings = settings),settings):
                    settings.embed.force = True
                    log.debug("Made embed")
                    return '<{}>'.format(settings.embed.url), settings.embed
            elif pattern[0] == 'da':
                url = 'http://fav.me/{}'.format(image_id)
                output = await self.get_deviant_data(url, settings = settings)
                if not output:
                    log.debug("No output")
                    return input, None
                log.debug("Got output")
                if await self.add_data(self.add_deviantart(output, settings = settings), settings):
                    settings.embed.force = True
                    log.debug("Made embed")
                    return '<{}>'.format(settings.embed.url), settings.embed
            elif pattern[0] == 'story':            
                output = await self.get_story(image_id, settings = settings)
                if output is None:
                    log.debug("No output")
                    return input, None
                log.debug(output)
                if await self.add_data(self.add_fimfiction(output, settings = settings), settings):
                    settings.embed.force = True
                    log.debug("Made embed")   
                    return '<{input}>'.format(input = settings.embed.url), settings.embed
        pattern = self.regex_derpiarrows.findall(input)
        if pattern:
            if settings.log_data is not None:
                settings.log_data.log = 'Looking for trigger: ' + str(input)
            image_id = pattern[0]
            log.debug(image_id)
            output = await self.get_derpi_data(image_id, settings = settings)
            if not output:
                log.debug("No output")
                return '<image {} not found>'.format(image_id), None
            image_id = output['id']
            log.debug("Got output")
            if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                settings.embed.force = True
                log.debug("Made embed")
                return '<{}>'.format(settings.embed.url), settings.embed


        log.debug(input)
        pattern = self.regex_url.search(input)
        if pattern is None: #if it's not a URL, don't bother going further
            log.debug('Not a URL')
            return input, None

        # if input.startswith('<') and input.endswith('>'):
        #     settings.mode = 'mini'
            
        input_x = unquote(input)
        log.debug(input_x)
        pattern_x = self.regex_discordcamo.search(input_x)
        if pattern_x is not None:
            log.debug(pattern_x)
            input = pattern_x.group(1)
            input = input.split('/',1)[0] + '://' + input.split('/',1)[1]
            if '?' in input:
                input = input.rsplit('?',1)[0]
            log.debug(input)
            #if '?' in url:
            #    url = url.split('?',1)[0]
            #log.debug(url)
            #input = url.split('/',1)[0] + '://' + url.split('/',1)[1]
        input_x = unquote(input)
        pattern_x = self.regex_fimficcamo.search(input_x)
        if pattern_x is not None:
            log.debug(pattern_x)
            input = pattern_x.group(1)
            #log.debug(url)
            #if '=' in url:
            #    input = url.split('=',1)[1]
            log.debug(input)
        pattern = re.search(r'(https?://media\.discordapp\.net/attachments/\S+)\?\S+', input) 
        if pattern is not None:
            log.debug(input)
            input = pattern.group(1)
            log.debug(input)
        loop = asyncio.get_event_loop()
        header = {}
        #if 'e621' in pattern.group(0):
        pattern = self.regex_url.search(input)
        log.debug('URL: ' + pattern.group(0))
        url = pattern.group(0).rstrip('`|>')
        log.debug('URL: ' + url)
        try:
            website = await self.get_website(url, method = "HEAD")
        except:
            log.debug('URL is bad: ' + url)
            return input, None
        if website is None:
            log.debug('Website failure!')
            return input, None
        if website.status >= 400:
            log.debug('URL is bad: ' + url + 'CODE: ' + str(website.status_code))
            return input, None
        if 'content-type' in website.headers and ( website.headers['content-type'].startswith('image/') or website.headers['content-type'].startswith('video/') ):
            is_image = True
            is_html = False
        else:
            is_image = False
            if 'content-type' in website.headers and 'text/html' in website.headers['content-type'].lower():
                is_html = True
            else:
                is_html = False
        website = None
        image_process = None
        html_process = None
        if settings.primary:
            if is_image:
                image_process = asyncio.ensure_future(self.get_image_data(url, header))
            
        if is_html:
            html_process = asyncio.ensure_future(self.get_website(url, header))
            
        
        if settings.log_data is not None:
            settings.log_data.log = 'Looking for url: <' + str(input) + '>'
        log.debug(settings)
        #Note: nothing in here retruns unless it has a valid match and can build an embed. I should probably wrap everything in try blocks...
        if not settings.skip_direct: #Direct sites are only to be matched in full or mini modes
            log.debug('Looking for direct site links')
            #e621
            pattern = self.regex_e621.search(input)
            if pattern is not None:
                image_id = pattern.group(1)
                log.debug(image_id)
                output = await self.get_e621_data(image_id, settings = settings)
                if output:
                    log.debug(output)
                    image_id = output['id']
                    if output['status'] is not 'deleted':
                        log.debug("Got output")
                        if await self.add_data(self.add_e621(output, settings = settings),settings):
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            if settings.explicit:
                                url = 'https://e926.net/post/show/{}/'.format(image_id)
                            else:
                                url = 'https://e621.net/post/show/{}/'.format(image_id)
                            return '{input}'.format(input = pre_input + '<' + url + '>' + post_input), settings.embed 
                
                
            #DeviantArt
            pattern = self.regex_deviantart.search(input)
            if pattern is None:
                pattern = self.regex_faveme.search(input)
            if pattern is not None:
                url = pattern.group(1)
                log.debug(url)
                if url.startswith('http:') and 'fav.me' not in url:
                    url = 'https:{}'.format(url.split(':',1)[1])
                output = await self.get_deviant_data(url, settings = settings)
                if output:
                    pattern2 = self.regex_deviantart_raw1.search(output['url'])
                    if pattern2 is None:
                        pattern2 = self.regex_deviantart_raw2.search(output['url'])
                    if pattern2 is not None:
                        url = decode_deviant_filename(pattern2.group(1))
                    log.debug('da url: ' + url)
                    log.debug("Got output")
                    if await self.add_data(self.add_deviantart(output, settings = settings),settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        output_url = pattern.group(1)
                        if 'fav.me' in output_url:
                            output_url = settings.embed.url
                        return '{input}'.format(input = pre_input + '<' + output_url + '>' + post_input), settings.embed 
                
            
            #Derpibooru/Trixiebooru
            pattern = self.regex_derpibooru.search(input)
            if pattern is not None:
                image_id = pattern.group(1)
                log.debug(image_id)
                output = await self.get_derpi_data(image_id, settings = settings)
                log.debug(output)
                if output:
                    if 'comment' not in input:
                        url = 'https://derpibooru.org/{}'.format(image_id)
                    else:
                        url = pattern.group(0)
                    image_id = output['id']
                    if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + url + '>' + post_input), settings.embed 
                    
            #Inkbunny
            pattern = self.regex_inkbunny_page.search(input)
            if pattern is not None:
                log.debug(pattern)
                log.debug(pattern.group(1))
                output = await self.get_inkbunny(submission_id = pattern.group(1), settings = settings)
                if output is not None:
                    if await self.add_data(self.add_inkbunny(output, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 

            #furaffinity
            pattern = self.regex_faurls.search(input)
            if pattern is not None:
                log.debug(pattern)
                output, url = await self.get_furaffinity(pattern.group(1), settings = settings)
                if output is not None:
                    if await self.add_data(self.add_furaffinity(output, url, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 
            
            #archive of our own story
            # pattern = self.regex_archiveofourown.search(input)
            # if pattern is not None:
            #     log.debug(pattern)
            #     output = await self.get_archiveofourown_data(pattern.group(1), log_data = log_data)
            #     if output is not None:
            #         if pattern.group(1) not in embed.sources:
            #             embed.sources = pattern.group(1)
            #             log.debug(output.json())
            #             self.add_archiveofourown(output, embed, primary, log_data = log_data)
            #             if explicit and embed.is_explicit:
            #                 embed.set_image(explicit,10)
            #             embed.type = 'story'
            #             output = embed
            #             log.debug("Made embed")   
            #             pre_input = input[:pattern.start(0)]
            #             if pre_input:
            #                 pre_input = pre_input + ' '
            #             post_input = input[pattern.end(0):]
            #             if post_input:
            #                 post_input = ' ' + post_input
            #             if '/chapters/' in input:
            #                 return '<{input}>'.format(input = input), output
            #             else:
            #                 return '{input}'.format(input = pre_input + '<' + pattern.group(0) + '>' + post_input), output 
            
            
            #fimfic stories
            if settings.enable_fimfic:
                pattern = self.regex_fimficurl_chapters.search(input)
                if pattern is None:
                    pattern = self.regex_fimficurl.search(input)
                if pattern is not None:
                    log.debug(pattern)
                    output = await self.get_story(pattern.group(1), settings = settings)
                    if output is not None:
                        log.debug(output)
                        if await self.add_data(self.add_fimfiction(output, settings = settings), settings):
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            return '{input}'.format(input = pre_input + '<' + pattern.group(0) + '>' + post_input), settings.embed 
            
                pattern = self.regex_fimficblogurl.search(input)
                if pattern is not None:
                    log.debug(pattern)
                    output = await self.get_blog(pattern.group(1), settings = settings)
                    if output is not None:
                        log.debug(output)
                        success = self.add_fimfiction_blog(output, settings = settings)
                        if success:
                            if settings.explicit and settings.embed.is_explicit:
                                settings.embed.set_image(settings.explicit,10)
                            if settings.hide_image:
                                settings.embed.rem_image()
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            return '{input}'.format(input = pre_input + '<' + pattern.group(0) + '>' + post_input), settings.embed 
            
                pattern = self.regex_fimficuserurl.search(input)
                if pattern is not None:
                    log.debug(pattern)
                    output = await self.get_user(pattern.group(1), settings = settings)
                    if output is not None:
                        log.debug(output)
                        if await self.add_data(self.add_fimfiction_user(output, settings = settings), settings):
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            return '{input}'.format(input = pre_input + '<' + pattern.group(0) + '>' + post_input), settings.embed 
            
                pattern = self.regex_fimficgroupurl.search(input)
                if pattern is not None:
                    log.debug(pattern)
                    output = await self.get_group(pattern.group(1), settings = settings)
                    if output is not None:
                        log.debug(output)
                        if await self.add_data(self.add_fimfiction_group(output, settings = settings), settings):
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            return '{input}'.format(input = pre_input + '<' + pattern.group(0) + '>' + post_input), settings.embed 
        if is_image:
            if image_process is not None:
                settings.image_data = await image_process
                log.debug(settings.image_data)
            
            #Inkbunny images
            pattern = self.regex_inkbunny_image.search(input)
            if pattern is not None:
                log.debug(pattern)
                output = await self.get_inkbunny(url=pattern.group(0), settings = settings)
                log.debug(output)
                if output is not None:
                    log.debug(output)
                    if await self.add_data(self.add_inkbunny(output, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 
                        
            #furaffinity images
            pattern = self.regex_fafullimage.search(input)
            if pattern is not None:
                log.debug(pattern)
                output, url = await self.get_furaffinity(pattern.group(2), pattern.group(1), settings = settings)
                if output is not None:
                    if await self.add_data(self.add_furaffinity(output, url, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 
                    
            #pattern = self.regex_fafullimage_partial.fullmatch(input)
            #if pattern is not None:
            #    log.debug(pattern)
            #    output, url = await self.get_furaffinity(pattern.group(1), pattern.group(2), log_data = log_data)
            #    if output is not None:
            #        self.add_furaffinity(output, url, embed, primary, log_data = log_data)
            #        if explicit and embed.is_explicit:
            #            embed.set_image(explicit,10)
            #        if hide_image:
            #            embed.rem_image()
            #        output = embed
            #        log.debug("Made embed")   
            #        return '{input}'.format(input = output.url), output 
                    
            #furaffinity small images
            pattern = self.regex_fasmallimage.search(input)
            if pattern is not None:
                log.debug(pattern)
                output, url = await self.get_furaffinity(pattern.group(1), settings = settings)
                if output is not None:
                    if await self.add_data(self.add_furaffinity(output, url, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed                   
                    
            #fimfiction story images
            pattern = self.regex_fimficstory.search(input)
            if pattern is not None:
                log.debug(pattern)
                output = await self.get_story(pattern.group(1), settings = settings)
                if output is not None:
                    log.debug(output)
                    if await self.add_data(self.add_fimfiction(output, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 
                    
            #fimfiction group images
            pattern = self.regex_fimficgroup.search(input)
            if pattern is not None:
                log.debug(pattern)
                output = await self.get_group(pattern.group(1), settings = settings)
                if output is not None:
                    log.debug(output)
                    if await self.add_data(self.add_fimfiction_group(output, settings = settings), settings):
                        #embed.type = 'story'
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed  
                    
            #fimfiction user images
            pattern = self.regex_fimficuser.search(input)
            if pattern is not None:
                log.debug(pattern)
                output = await self.get_user(pattern.group(1), settings = settings)
                if output is not None:
                    log.debug(output)
                    if await self.add_data(self.add_fimfiction_user(output, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 

            ##tumblr raw
            #pattern = self.regex_tumblr_raw.findall(input)
            #if pattern:
            #    pattern = pattern[0]
            #    log.debug(pattern)
            #    url = decode_tumblr_filename(pattern[0])
            #    log.debug(url)
            #    log.debug("url:        " + url)
            #    log.debug("pattern[0]: " + pattern[0])
            #    if url not in pattern[0] or skip_direct:
            #        self.add_raw_tumblr(url, embed, primary, log_data = log_data)
            #        if explicit and embed.is_explicit:
            #            embed.set_image(explicit,10)
            #        if hide_image:
            #            embed.rem_image()
            #        output = embed
            #        log.debug("Made embed")   
            #        return '{input}'.format(input = output.url), output  


            #e621 raw
            pattern = self.regex_e621_raw.search(input)
            if pattern is not None:
                log.debug(pattern)
                image_md5 = pattern.group(1)
                log.debug(image_md5)
                output = await self.get_e621_data(image_md5, settings = settings)
                if output:
                    log.debug(output)
                    image_id = output['id']
                    if output['status'] != 'deleted':
                        log.debug("Got output")
                        if await self.add_data(self.add_e621(output, settings = settings), settings):
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            if settings.explicit:
                                url = 'https://e926.net/post/show/{}/'.format(image_id)
                            else:
                                url = settings.embed.url
                            return '{input}'.format(input = pre_input + '<' + url + '>' + post_input), settings.embed 


            #deviantart raw
            pattern = self.regex_deviantart_raw1.search(input)
            if pattern is None:
                pattern = self.regex_deviantart_raw2.search(input)
            if pattern is not None:
                url = decode_deviant_filename(pattern.group(1))
                log.debug(url)
                output = await self.get_deviant_data(url, settings = settings)
                if output:
                    pattern = self.regex_deviantart_raw1.search(output['url'])
                    if pattern is None:
                        pattern = self.regex_deviantart_raw2.search(output['url'])
                    if pattern is not None:
                        url = decode_deviant_filename(pattern.group(1))
                    log.debug('da url: ' + url)
                    log.debug("Got output")
                    if await self.add_data(self.add_deviantart(output, settings = settings), settings):
                        log.debug("Made embed")   
                        pre_input = input[:pattern.start(0)]
                        if pre_input:
                            pre_input = pre_input + ' '
                        post_input = input[pattern.end(0):]
                        if post_input:
                            post_input = ' ' + post_input
                        return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 
                
                
            #derpicdn/other derpi raw images
            if settings.primary:
                pattern = self.regex_derpicdn1.search(input)
                if pattern is None:
                    pattern = self.regex_derpicdn2.search(input)
                if pattern is not None:
                    log.debug(pattern)
                    image_id = pattern.group(1)
                    log.debug(image_id)
                    output = await self.get_derpi_data(image_id, settings = settings)
                    if output:
                        log.debug("Got output")
                        if await self.add_data(self.add_derpibooru(output, settings = settings), settings):
                            log.debug("Made embed")   
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            return '{input}'.format(input = pre_input + '<' + settings.embed.url + '>' + post_input), settings.embed 
                        
                pattern = self.regex_tumblr_raw.search(input)
                if pattern is not None:
                    log.debug(pattern)
                    pre_input = input[:pattern.start(0)]
                    if pre_input:
                        pre_input = pre_input + ' '
                    post_input = input[pattern.end(0):]
                    if post_input:
                        post_input = ' ' + post_input
                    input = pre_input + decode_tumblr_filename(pattern.group(0)) + post_input
                
                pattern = self.regex_url.search(input)
                if pattern is not None:
                    if settings.image_data is not None:    
                        output = await self.extract_from_hash(pattern.group(0), settings = settings)
                        if output is not None:
                            pre_input = input[:pattern.start(0)]
                            if pre_input:
                                pre_input = pre_input + ' '
                            post_input = input[pattern.end(0):]
                            if post_input:
                                post_input = ' ' + post_input
                            return '{input}'.format(input = pre_input + '<' + output.url + '>' + post_input), output

        if not settings.primary and is_html:
            log.debug(bool(html_process))
            if html_process is not None:
                website = await html_process
                site_data = opengraph_py3.OpenGraph(html=website.website)
                log.debug(site_data.to_json())
                if await self.add_data(self.add_opengraph(site_data, settings = settings), settings):
                    return '{}'.format('<' + input + '>'), settings.embed

        log.debug('Nothing found, trying to source OpenGraph: ' + str(is_html and ( settings.mode == 'full' or settings.mode == 'mini')))
        if is_html and ( settings.mode == 'full' or settings.mode == 'mini'):
            pattern1 = self.regex_e621.search(input)
            pattern2 = self.regex_derpibooru.search(input)
            pattern3 = self.regex_inkbunny_page.search(input)
            pattern4 = self.regex_faurls.search(input)
            pattern5 = self.regex_deviantart.search(input)
            pattern6 = self.regex_fimficurl_chapters.search(input)
            if pattern6 is None:
                pattern6 = self.regex_fimficurl.search(input)
            if pattern5 is None:
                pattern5 = self.regex_faveme.search(input)
            pattern7 = self.regex_fimficblogurl.search(input)
            pattern8 = self.regex_fimficuserurl.search(input)
            pattern9 = self.regex_fimficgroupurl.search(input)

            if pattern1 or pattern2 or pattern3 or pattern4 or pattern5 or pattern6 or pattern7 or pattern8 or pattern9:
                log.debug('HTML file, skipping!')
            else:
                log.debug('HTML file, searching for OpenGraph')
                if html_process is not None:
                    image_url = None
                    try:
                        website = await html_process
                        site_data = opengraph_py3.OpenGraph(html=website.website)
                        log.debug(site_data)
                        if 'type' in site_data and settings.mode != 'mini' and settings.target_user is None:
                            assert 'video' not in str(site_data['type'])
                        assert 'http' in site_data['image']
                        image_url = site_data['image']
                    except:
                        pass
                    if image_url:
                        if await self.add_data(self.add_opengraph(site_data, settings = settings), settings):
                            settings.image_data = await self.get_image_data( image_url )
                            settings.primary = False
                            output = await self.extract_from_hash(image_url, settings = settings)
                            if output is not None:
                                return '{}'.format('<' + input + '>'), output

        if settings.primary and is_image:
            if 'unsourced' in self.metrics:
                self.metrics['unsourced'] += 1
            else:
                self.metrics['unsourced'] = 1
        #embed.set_source('Source', input, 0)
        return input, None #finally, if we can't find anything, pass the original word back and None.


    async def add_data(self, func = None, settings : ImagesafeSettings = ImagesafeSettings()):
        assert func
        abort = False
        try:
            success = await func
        except ValueError as e:
            abort = True
            if settings.log_data is not None:
                settings.log_data.log = e
            log.debug(e)
        if not abort and success:
            if settings.explicit and settings.embed.is_explicit:
                settings.embed.set_image(settings.explicit,10)
            if settings.hide_image:
                settings.embed.rem_image()
            return True
        return False

class Simple2DAnalyser(object):
    def __init__(self, imagename):
        im = cv2.imdecode(imagename, cv2.IMREAD_COLOR)
        # Now let's get four quadrants and our total
        rows, cols, channels = im.shape 
        halfX = int(round(cols/2))
        halfY = int(round(rows/2))
        #self.total = self.sumArea(im)
        self.nw = self.sumArea(im[0:halfY, 0:halfX])
        self.ne = self.sumArea(im[0:halfY, halfX+1:cols])
        self.sw = self.sumArea(im[halfY+1:rows, 0:halfX])
        self.se = self.sumArea(im[halfY+1:rows, halfX+1:cols])
        self.height = rows
        self.width = cols

    def sumArea(self, im):
        rows, cols, channels = im.shape 
        sums = cv2.sumElems(im)
        self.r = (sums[2]/(rows*cols))#*0.2126
        self.g = (sums[1]/(rows*cols))#*0.7152
        self.b = (sums[0]/(rows*cols))#*0.0772
        return (self.r,self.g,self.b)
        #return ((self.b+self.g+self.r)/3)
    def getValues(self):
        return ("%f %f %f %f" %
                (self.ne, self.nw, self.se, self.sw))


def hamming_distance(s1, s2):
    assert len(s1) == len(s2)
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))
    
def compare_average_colors(image1, image2):
    if image1 and image2:
        for blocks in ['ne','nw','se','sw']:
            for i in range(3):
                if abs(image1[blocks][i] - image2[blocks][i]) > 6:
                    return False
    return True

def get_average_color(start, end, image):
    # duplicate_image = image.crop((start['x'],start['y'],end['x'],end['y']))
    # stats = ImageStat.Stat(duplicate_image)
    # mean_values = stats.mean
    # r = mean_values[0]*0.2126
    # g = mean_values[1]*0.7152
    # b = mean_values[2]*0.0772
    # output = (r+g+b)/3
    # return output


    duplicate_image = image.crop((start['x'],start['y'],end['x'],end['y']))
    stats = ImageStat.Stat(duplicate_image)
    return stats.mean



    duplicate_image = image.copy()
    duplicate_image = duplicate_image.crop((start['x'],start['y'],end['x'],end['y']))
    duplicate_image = duplicate_image.resize((1, 1), Image.BOX) #, Image.ANTIALIAS
    R, G, B = duplicate_image.getpixel((0,0))
    log.debug(R)
    log.debug(G)
    log.debug(B)
    return (R,G,B)
    
    
    """ Returns a 3-tuple containing the RGB value of the average color of the
    given square bounded area of length = n whose origin (top left corner) 
    is (x, y) in the given image"""
    r, g, b = 0, 0, 0
    count = 0
    px = image.load()
    for s in range(start['x'], end['x'],2):
        for t in range(start['y'], end['y'],2):
            pixlr, pixlg, pixlb = px[s, t]
            r += pixlr
            g += pixlg
            b += pixlb
            count += 1
    return ((r/count), (g/count), (b/count))

async def get_key( client_id, client_secret):   
    """Helper function to fetch and verify a fimfiction API key"""
    if not client_id or not client_secret:
        return None
    base_url = 'https://www.fimfiction.net/api/v2/token'
    loop = asyncio.get_event_loop()
    headers = {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache"
        }
    payload = "client_id=" + client_id + "&client_secret=" + client_secret +  "&grant_type=client_credentials"
    response = loop.run_in_executor(None, partial(requests.request,"POST", base_url, data=payload, headers=headers))
    website = await response
    website = website.json()
    return website
        
#custom unflatten code for fimfic flat-json
def jsonapi_unflatten( data ):
    if 'error' in data:
        return data
    output_data = {}
    included_dict = {}
    if 'included' in data:
        for datum in data['included']:
            if datum['type'] not in included_dict:
                included_dict[datum['type']] = {}
            included_dict[datum['type']][datum['id']] = datum
    if not isinstance(data['data'],list):
        data['data'] = [ data['data'] ]
    for datum in data['data']:
        datum = unflatten( datum, included_dict)
        if datum['type'] not in output_data:
            output_data[datum['type']] = {}
        output_data[datum['type']][datum['id']] = datum
    return output_data

def unflatten( data, data_included ):
    if 'relationships' in data:
        for relationship_name, relationship_data in data['relationships'].items():
            if isinstance(relationship_data['data'], dict):
                relationship_data['data'] = [ relationship_data['data'] ]
            for datum in relationship_data['data']:
                if datum['type'] not in data_included:
                    continue
                else:
                    if relationship_name not in data:
                        data[relationship_name] = []
                    r_type = datum['type']
                    r_id = datum['id']            
                    extra_data = {}
                    if 'relationships' in data_included[r_type][r_id]:
                        extra_data = unflatten(data_included[r_type][r_id],data_included)
                    if 'meta' in data_included[r_type][r_id]:
                        if 'url' in data_included[r_type][r_id]['meta']:
                            extra_data['url'] = data_included[r_type][r_id]['meta']['url']
                    if 'attributes' in data_included[r_type][r_id]:
                        data[relationship_name].append({**data_included[r_type][r_id]['attributes'], **{ 'id' : r_id,}, **extra_data})
                    else:
                        data[relationship_name].append({**{ 'id' : r_id}, **extra_data})
                        #'type' : r_type,
        del data['relationships']
    if 'links' in data:
        del data['links']
    if 'attributes' in data:
        for key,value in data['attributes'].items():
            data[key] = value
        del data['attributes']
    if 'meta' in data:
        if 'url' in data['meta']:
            data['url'] = data['meta']['url']
        del data['meta']
    return data        
        
def derpi_markup_2_markdown(input):
    """Because derpibooru won't expose an endpoint
    to give me HTML like EVERYWHERE ELSE DOES"""
    processing = re.split(r'^\[==(.*?)==\]', input, flags=re.DOTALL)
    output = ''
    for datum in processing:
        if re.fullmatch(r'^\[==(.*?)==\]', datum, flags=re.DOTALL) is not None:
            output += datum
            continue
        datum = datum.replace('*', '**')
        #datum = re.sub(r'\+([^\+]+?)\+', '\\1', datum)
        #datum = re.sub(r'@([^@]+?)@', '```\\1```', datum)
        #datum = re.sub(r'-([^\-]+?)-', '\\1', datum)
        #datum = re.sub(r'~([^~]+?)~', '\\1', datum)
        #datum = re.sub(r'\^([^\^]+?)\^', '\\1', datum)
        log.debug(datum)
        datum = re.sub(r'"([^"]+?)":((?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+))', '[\\1](\\2)', datum)
        datum = re.sub(r'"([^"]+?)":([\S]+)', '[\\1](https://derpibooru.org\\2)', datum)
        log.debug(datum)
        datum = re.sub(r'>>(\d+)(?:t|p|s)?', '[>>\\1](https://derpibooru.org/\\1)', datum)
        log.debug(datum)
        datum = re.sub(r'!((?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+))!:((?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+))', '[IMAGE](\\1) + [LINK](\\2)', datum)
        log.debug(datum)
        datum = re.sub(r'!((?:(?:(?:ftp|http)[s]*:\/\/|www\.)[^\.]+\.[^ \n]+))!', '[IMAGE](\\1)', datum)
        log.debug(datum)
        datum = re.sub(r'\[spoiler\](.*?)\[/spoiler\]', '||\\1||', datum, flags=re.DOTALL)
        log.debug(datum)
        datum_out = ''
        log.debug(re.split(r'(\[bq\].*\[/bq\])', datum, flags=re.DOTALL))
        for blockquotes in re.split(r'(\[bq\].*\[/bq\])', datum, flags=re.DOTALL):
            log.debug(datum_out)
            log.debug(blockquotes)
            if re.fullmatch(r'\[bq\].*?\[/bq\]', blockquotes, flags=re.DOTALL) is None:
                datum_out += blockquotes
                continue
            blockquotes = blockquotes.replace('[bq]', '')
            blockquotes = blockquotes.replace('[/bq]', '')
            log.debug(datum_out)
            log.debug(blockquotes)
            datum_out += re.sub(r'(.*?\r?\n)', '> \\1', blockquotes, flags=re.MULTILINE)
        datum = datum_out
        log.debug(datum)
        output += datum
    if 'tumblr.com' in output:
        output = ''
    return output

    
#Various helper functions follow
def fixup_fimfic_urls( url ):
    """Because API2 is in beta"""
    broken_url = list(urlparse(url))
    if broken_url[2].startswith('/api/v2'):
        broken_url[2] = broken_url[2][7:]
        url = urlunparse(broken_url)
    if url.startswith('//'):
        return 'https:' + url
    if not url.startswith('https://www.fimfiction.net'.lower()):
        return 'https://www.fimfiction.net' + url
    return url    
        
def decode_tumblr_filename( url ):
    """This is only a little complicated"""
    log.debug(url.split('/'))
    log.debug(url.split('/')[-1].rsplit('_',1))
    log.debug(url.split('/')[-1].split('.',1))
    return '{url}_1280.{ext}'.format(url=url.rsplit('_',1)[0], ext=url.rsplit('.',1)[1])
    #return 'http://data.tumblr.com/{md5}/{first}_raw.{ext}'. format(md5=url.split('/')[3], first=url.split('/')[4].rsplit('_',1)[0], ext=url.split('/')[4].split('.',1)[1])
    
def decode_deviant_filename( data ):
    """This actually became really easy."""
    #if code and artist and title:
    return 'http://fav.me/{code}'.format(code=data)
    #else:
    #    return None

def decode_stash_filename( data ):
    """This doesn't work. I'm pretty sure it will crash
    most of the time. Don't do Stash, Stash is bad, 
    mmmkay?"""
    title = data.split('/')[-1].rsplit('_by_', maxsplit=1)[0].replace('_','-')
    artist = data.split('/')[-1].rsplit('_by_', maxsplit=1)[1].rsplit('-d')[0].replace('_','-')
    code = data.split('/')[-1].rsplit('_by_', maxsplit=1)[1].rsplit('-d')[1].rsplit('.', maxsplit=1)[0]
    extension = data.split('/')[-1].rsplit('_by_', maxsplit=1)[1].rsplit('-d')[1].split('.', maxsplit=1)[1]
    code = int(code, 36)
    if code and artist and title:
        return 'http://sta.sh/????'.format(artist=artist, code=code, title=title)
    else:
        return None