import discord
import asyncio
import logging

log = logging.getLogger("red.nolinks")
log.setLevel(logging.INFO)

class Nolinks:
    def __init__(self, bot):
        self.bot = bot
    
    async def listener2(self, skub, message):
        await self.listener(message)
    
    async def listener(self, message):
        channel = message.channel
        server = message.server
        author = message.author
        if message.author.id == self.bot.user.id:
            log.debug("First abort")
            return
        if message.author.bot:
            log.debug("Second abort")
            return
        if channel.permissions_for(message.author). manage_messages:
            log.debug("Third abort")
            return
        found_discord_link = False
        while True:
            if 'discord.gg/' in message.content.lower():
                found_discord_link = True
                log.debug("Link in content")
                break
            for embeds in message.embeds:
                if found_discord_link:
                    break
                for title,data in embed.to_dict().items():
                    if found_discord_link:
                        break
                    if 'discord.gg/' in title.lower():
                        found_discord_link = True
                        log.debug("Link in embed title")
                        break
                    if 'discord.gg/' in data.lower():
                        found_discord_link = True
                        log.debug("Link in embed data")
                        break
            for attachments in message.attachments:
                if found_discord_link:
                    break
                for title,data in attachments.items():
                    if found_discord_link:
                        break
                    if 'discord.gg/' in title.lower():
                        found_discord_link = True
                        log.debug("Link in attachment title")
                        break
                    if 'discord.gg/' in data.lower():
                        found_discord_link = True
                        log.debug("Link in attachment data")
                        break
            break
        if found_discord_link:
            log.debug("Actioning...")
            await self.bot.delete_message(message)
            await self.bot.send_message(author, "Please ask a member of staff to post any discord server links on {}. Thank you! :bat:".format(server.name))
        
        
def setup(bot):
    n = Nolinks(bot)
    bot.add_listener(n.listener, 'on_message')
    bot.add_listener(n.listener2, 'on_message_edit')
    bot.add_cog(n)