import discord
from discord.ext import commands
import os
from time import gmtime, strftime
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
import asyncio

# Some magic values

DATA_PATH = "data/notes/"
JSON_PATH = DATA_PATH + "notes.json"

class Notes:
    """Allows staff to leave notes about users"""
    # Data format:
    # {
    #  serverid (str): {
    #   userid (str): [
    #    {
    #     note : note (str)
    #     authorid : author (str)
    #     time : creation date (str)
    #     deleted : deleted by (str)
    #    }
    #   ]
    #  }
    # }
    
    def __init__(self, bot):
        self.bot = bot
        self.notefile = fileIO("data/notes/notes.json", "load")
        
    @checks.mod()
    @commands.group(pass_context = True, name = 'notes')
    async def notes(self, ctx):
        """Note taking cog for moderators
        Admin can see exactly who deleted a note, and when they delete them they are fully removed."""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @notes.command(pass_context=True, name='list')
    async def _list(self, ctx, username : discord.Member):
        """Lists notes for a user
        Usage:
        notes list @user
        """
        server = ctx.message.server
        if server.id not in self.notefile:
            await self.bot.say('No notes on any users on this server!')
        elif username.id not in self.notefile[server.id]:
            await self.bot.say('No notes on this user on this server!')
        else:
            server = ctx.message.server
            if username.id not in self.notefile[server.id]:
                await self.bot.whisper("No notes found")
            else:
                msg = '```'
                for a in range(len(self.notefile[server.id][username.id])):
                    if self.notefile[server.id][username.id][a]['deleted'] is not False:
                        staffer = discord.utils.find(lambda m: m.id == self.notefile[server.id][username.id][a]['deleted'], ctx.message.server.members)
                        if staffer is None:
                            staffer = 'by: an ex-staff member'
                        elif checks.admin():
                            staffer = 'by: ' + staffer.name
                        else:
                            staffer = ''
                        msg += '%s - %s %s\n' % (a,'Message deleted',staffer)
                    else:
                        msg += '%s - %s\n' % (a,self.notefile[server.id][username.id][a]['note'])
                msg += '```'
                await self.bot.whisper(msg)  
                await self.bot.say('Done.')                

    @notes.command(pass_context=True, name='add')
    async def _add(self, ctx, username : discord.Member, *, data):
        """Adds a note to a user
        Usage:
        notes add @user <note>
        """
        staffer = ctx.message.author
        server = ctx.message.server
        note = { 'note' : data , 'authorid' : staffer.id , 'time' : strftime("%a, %d %b %Y", gmtime()), 'deleted' : False }
        if server.id not in self.notefile:
            self.notefile[server.id] = {}
        if username.id not in self.notefile[server.id]:
            self.notefile[server.id][username.id] = [ note ]
        else:
            self.notefile[server.id][username.id].append(note)
        fileIO(JSON_PATH, "save", self.notefile)
        await self.bot.say('Done.')
                
    @notes.command(pass_context=True, name='detail', aliases=['details'])
    async def _details(self, ctx, username : discord.Member, target_note : int):
        """Gives extra detail on a note
        Usage:
        notes detail @user #"""
        server = ctx.message.server
        if target_note not in range(len(self.notefile[server.id][username.id])):
            await self.bot.whisper("No note found")
        else:
            author = discord.utils.find(lambda m: m.id == self.notefile[server.id][username.id][target_note]['authorid'], ctx.message.server.members)
            if author is None:
                author = 'an ex-staff member'
            else:
                author = author.name
            staffer = discord.utils.find(lambda m: m.id == self.notefile[server.id][username.id][target_note]['deleted'], ctx.message.server.members)
            if staffer is not None:
                staffer = staffer.name
            msg =  '```'
            msg += '%s: %s\n' % ('Author', author)
            msg += '%s: %s\n' % ('Date', self.notefile[server.id][username.id][target_note]['time'])
            msg += 'Notes:\n'
            if checks.admin() and staffer is not None:
                msg += '    %s\n' % (self.notefile[server.id][username.id][target_note]['note'])
                msg += '    Deleted by: %s\n' % (staffer)
            elif staffer is not None:
                msg += '    Deleted\n'
            else:
                msg += '    %s\n' % (self.notefile[server.id][username.id][target_note]['note'])
            await self.bot.whisper(msg + '```')
            await self.bot.say('Done.')
            
    @notes.command(pass_context=True, name='del', aliases=['delete', 'rem', 'remove'])
    async def _del(self, ctx, username : discord.Member, target_note : int):
        '''Removes a note from the archive
        Usage:
        notes remove #
        '''        
        server = ctx.message.server
        if target_note not in range(len(self.notefile[server.id][username.id])):
            await self.bot.whisper("No note found")
        else:
            if checks.admin():
                await self.bot.say('Type **delete** to actually delete the message, or **mark** to just mark deleted (anything else aborts).')
                message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
                if message.content.lower() == 'delete':
                    await self.bot.say('Deleting')
                    self.notefile[server.id][username.id].remove( self.notefile[server.id][username.id][target_note] )
                elif message is None or message.content.lower() != 'mark':
                    await self.bot.say('Aborting')
                else:
                    await self.bot.say('Marking as deleted')
                    self.notefile[server.id][username.id][target_note]['deleted'] = ctx.message.author.id
            else:
                self.notefile[server.id][username.id][target_note]['deleted'] = ctx.message.author.id
            if not self.notefile[server.id][username.id]:
                del self.notefile[server.id][username.id]
            if not self.notefile[server.id]:
                del self.notefile[server.id]
            fileIO(JSON_PATH, "save", self.notefile)
            await self.bot.say('Done.')
        


def check_folder():
    if not os.path.exists(DATA_PATH):
        os.makedirs(DATA_PATH)


def check_file():
    if not fileIO(JSON_PATH, "check"):
        print("Creating notes.json...")
        fileIO(JSON_PATH, "save", {})        
        
def setup(bot):
    check_folder()
    check_file()
    n = Notes(bot)
    bot.add_cog(n)