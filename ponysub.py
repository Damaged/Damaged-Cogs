import discord
from discord.ext import commands
from .utils.chat_formatting import *
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
from urllib import parse
import aiohttp
import os
import asyncio
from datetime import datetime, timedelta
import pyrfc3339 
import logging
import requests
import json
from functools import partial


log = logging.getLogger("red.ponysub")
if os.path.exists("data/istest"):
    log.setLevel(logging.DEBUG)
    log.debug('Debug Logging On')
else:
    log.setLevel(logging.ERROR)
    log.error('Error Logging On')

try:   # check if pony cog is installed
    from .special_pony import Pony
    ponyAvailable = True 
except ImportError:
    ponyAvailable = False
    
try:   # check if imagesafe cog is installed
    from .imagesafe import Imagesafe, ImageEmbed
    imagesafe_instance = Imagesafe(None)
    derpiAvailable = True 
except ImportError:
    derpiAvailable = False

    
    
class Ponysub:
    def __init__(self, bot):
        self.bot = bot
        self.data = fileIO("data/ponysub/data.json", "load")
        self.history = fileIO("data/ponysub/history.json", "load")
        self.sub_task = bot.loop.create_task(self.ponysub_service())


    @commands.group(name='ponysub',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def ponysub(self, ctx):
        """Manages subscription to a search string on derpibooru"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @ponysub.command(name='test', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _test(self, ctx):
        log.setLevel(logging.DEBUG)
        await self.spam_looper()
        if os.path.exists("data/istest"):
            log.setLevel(logging.DEBUG)
            log.debug('Debug Logging On')
        else:
            log.setLevel(logging.ERROR)
            log.error('Error Logging On')
        
    @ponysub.command(name='add', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _add(self, ctx, sub_name : str, channel : discord.Channel, * , search_string : str=""):
        """Adds a new search string to check for updates:
        
        [p]ponysub add <name> #<channel> <search string>"""
        server = ctx.message.server
        sub_name = sub_name.lower().capitalize()
        new_sub = { 'name' : sub_name, 'channel' : channel.id, 'search string' : search_string, 'enabled' : False, 'latest image' : "1980" }
        if server.id not in self.data:
            self.data[server.id] = []
        else:
            if find_sub(self, server, sub_name):
                await self.bot.say("Name %s is already used!" % ( sub_name ))
                return
        self.data[server.id].append( new_sub )
        fileIO("data/ponysub/data.json", "save", self.data)
        await self.bot.say("Added %s to subscriptions! Don't forget to enable it!" % ( sub_name ))
    
    @ponysub.command(name='list', pass_context=True)
    async def _list(self, ctx, channel : discord.Channel=False):
        """Lists all subscriptions:
        
        [p]ponysub list"""
        server = ctx.message.server
        msg = "Listing:\n"
        if server.id not in self.data:
            msg += "No subscriptions in server!\n"
            await self.bot.say(msg)
        else:
            for a in range(len(self.data[server.id])):
                if channel:
                    if channel.id != self.data[server.id][a]['channel']:
                        continue
                msg += "```Markdown\n"
                if self.data[server.id][a]['enabled']:
                    msg += "# %s: Enabled\n" % (self.data[server.id][a]['name'])
                else:
                    msg += "# %s: Disabled\n" % (self.data[server.id][a]['name'])
                msg += "Channel: #%s\n" % ( find_channel( self, server, self.data[server.id][a]['channel'] ).name )
                msg += "Search String: %s\n" % (self.data[server.id][a]['search string'])
                msg += "```\n"
                await self.bot.say(msg)
                msg = ''

    @ponysub.command(name='toggle', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _toggle(self, ctx, sub_name):
        """Enable or disable a subscription
        
        [p]ponysub toggle <name>"""
        server = ctx.message.server
        sub_number = find_sub(self, server, sub_name)
        if type(sub_number) is bool:
            await self.bot.say("Can't find %s!" % (sub_name))
            return
        if self.data[server.id][sub_number]['enabled']:
            self.data[server.id][sub_number]['enabled'] = False
            fileIO("data/ponysub/data.json", "save", self.data)
            await self.bot.say( "Subscription %s now disabled!" % (self.data[server.id][sub_number]['name']))
            return
        else:
            search = "https://derpibooru.org/search.json?q="
            search += parse.quote_plus(self.data[server.id][sub_number]['search string'])
            search += "&filter_id=56027"
            try:
                async with aiohttp.get(search) as r:
                    website = await r.json()
            except:
                return await self.bot.say("Error.")
            if website['total'] == 0:
                await self.bot.say("There are no images matching this subscription!")
                return
            self.data[server.id][sub_number]['latest image'] = website['search'][0]['created_at']
            self.data[server.id][sub_number]['enabled'] = True
            fileIO("data/ponysub/data.json", "save", self.data)
            await self.bot.say( "Subscription %s now enabled!" % (self.data[server.id][sub_number]['name']))

    @ponysub.command(name='refresh', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes)
        
        [p]ponysub refresh <time>"""
        server = ctx.message.server
        if refresh_time < 2:
            await self.bot.say("You can only enter a time over 2 minutes.")
            return
        self.data['refresh'] = refresh_time
        await self.bot.say( "Refresh time set to %i minutes." % (refresh_time))
        fileIO("data/ponysub/data.json", "save", self.data)
        
    @ponysub.command(name='del', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _delete(self, ctx, sub_name):
        """Deletes a subscription
        
        [p]ponysub del <name>"""
        server = ctx.message.server
        sub_number = find_sub(self, server, sub_name)
        if type(sub_number) is bool:
            await self.bot.say("Can't find %s!" % (sub_name))
            return
        if server.id not in self.data:
            await self.bot.say("No subscriptions for this server!")
            return
        await self.bot.say('Type **delete** to actually delete the subscription.')
        message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
        if not message:
            return await self.bot.say("Aborted.")
        if message.content.lower() != 'delete':
            return await self.bot.say("Aborted.")
        self.data[server.id].remove( self.data[server.id][sub_number] )
        if not self.data[server.id]:
            del self.data[server.id]
        fileIO("data/ponysub/data.json", "save", self.data)
        await self.bot.say( "Deleted %s subscription." % (sub_name))
        
    async def ponysub_service(self):
        """Trigger loop"""
        #return
        if os.path.exists("data/istest"):
            return #comment out when not testing
        await self.bot.wait_until_ready()
        try:
            while True:
                try:
                    await self.spam_looper()
                except asyncio.CancelledError:
                    raise
                except:
                    pass
                if type(self.data['refresh']) is int:
                    if self.data['refresh'] < 2:
                        self.data['refresh'] = 2
                        fileIO("data/ponysub/data.json", "save", self.data)
                else:
                    self.data['refresh'] = 2
                    fileIO("data/ponysub/data.json", "save", self.data)
                await asyncio.sleep(self.data['refresh'] * 60)
                print("Ponysub service tick.")
        except asyncio.CancelledError:
            pass

    def __unload(self):
        self.sub_task.cancel()
            
    async def spam_looper(self):
        if os.path.exists("data/pony/filters.json"):
            self.filters = fileIO("data/pony/filters.json", "load")
        else:
            self.filters = {}
            self.filters['default'] = [ '*' ]
        if not self.data:
            return
        for server in self.data.keys():
            server = self.bot.get_server(server)
            if server is None:
                continue
            newest = pyrfc3339.generate(datetime.utcnow() - timedelta(seconds = 60),accept_naive=True)
            channel_list = []
            for datum in self.data[server.id]:
                if datum['channel'] in channel_list:
                    continue
                channel_list.append(datum['channel'])
            log.debug(channel_list)
            for channel in channel_list:
                channel = self.bot.get_channel(channel)
                if channel is None:
                    continue
                if 'latest image' not in self.data:
                    self.data['latest image'] = {}
                if server.id not in self.data['latest image']:
                    self.data['latest image'][server.id] = {}
                if channel.id not in self.data['latest image'][server.id] or not self.data['latest image'][server.id][channel.id]:
                    self.data['latest image'][server.id][channel.id] = newest
                    fileIO("data/ponysub/data.json", "save", self.data)
                    continue
                log.debug('Processing Channel: {}'.format(channel.name))
                spammed = []
                search_field = []
                if channel.id not in self.history:
                    self.history[channel.id] = {
                        'hash' : [],
                        'values' : []
                    }
                    fileIO("data/ponysub/history.json", "save", self.history)
                for sub_number in range(len(self.data[server.id])):
                    if not self.data[server.id][sub_number]['enabled']:
                        continue
                    if self.data[server.id][sub_number]['channel'] != channel.id:
                        log.debug('Ignoring {}'.format(self.data[server.id][sub_number]['name']))
                        continue
                    search_field.append(self.data[server.id][sub_number]['search string'])
                log.debug(search_field)
                if not search_field:
                    continue
                search_field = '( (' + ') OR ('.join(search_field) + ') )'
                search_field += ", created_at.gt:{oldest}, created_at.lt:{newest}".format(oldest=self.data['latest image'][server.id][channel.id], newest=newest)
                if server.id in self.filters and self.filters[server.id]:
                    search_field += ", " + ", ".join(self.filters[server.id])
                else:
                    search_field += ", " + ", ".join(self.filters["default"])
                #search_field += parse.quote_plus(tagSearch)
                search_field = 'https://derpibooru.org/search.json?q=' + parse.quote_plus(search_field)
                search_field += "&filter_id=56027"
                log.debug(search_field)
                page_counter = 1
                image_dict = {}
                loop = asyncio.get_event_loop()
                while True:
                    response = loop.run_in_executor(None, partial(requests.request,"GET", search_field + '&page=' + str(page_counter)))
                    website = await response
                    website = website.json()
                    log.debug(website)
                    if 'search' not in website or not website['search']:
                        break
                    for data in website['search']:
                        image_dict[data['id']] = data
                    page_counter += 1
                log.debug(image_dict)
                actioned = False
                for image_id in sorted(list(image_dict.keys())):
                    log.debug(image_id)
                    log.debug(image_dict[image_id])
                    if image_id in spammed:
                        log.debug('I already spammed: ' + image_id)
                        continue
                    image_data = image_dict[image_id]
                    while "duplicate_of" in image_data:
                        log.debug('Duplicate detected: ' + image_data["duplicate_of"])
                        search = "https://derpibooru.org/{}.json".format(image_data["duplicate_of"])
                        async with aiohttp.get(search) as r:
                            image_data = await r.json()
                    if not image_data:
                        continue
                    log.debug(image_data)
                    if image_data['orig_sha512_hash'] in self.history[channel.id]['hash']:
                        log.info('Skipping image due to dupe detection (HASH)')
                        continue
                    self.history[channel.id]['hash'].append(image_data['orig_sha512_hash'])
                    while len(self.history[channel.id]['hash']) > 100:
                        self.history[channel.id]['hash'].pop(0)
                    fileIO("data/ponysub/history.json", "save", self.history)
                    if derpiAvailable:
                        image_metadata = await imagesafe_instance.get_image_data('https:' + image_data['representations']['medium'])
                        if image_metadata is not None:
                            if image_metadata['matchvalues']:
                                for key,data in enumerate(list(image_metadata['matchvalues'])):
                                    image_metadata['matchvalues'][key] = [int(i) for i in data]
                                if image_metadata['matchvalues'] in self.history[channel.id]['values']:
                                    log.info('Skipping image due to dupe detection (RGB)')
                                    continue
                                self.history[channel.id]['values'].append(image_metadata['matchvalues'])
                                while len(self.history[channel.id]['values']) > 100:
                                    self.history[channel.id]['values'].pop(0)
                                fileIO("data/ponysub/history.json", "save", self.history)
                        actioned = True
                        log.debug('Processing and building embed')
                        embed_object = ImageEmbed()
                        await imagesafe_instance.add_derpibooru(image_data, embed_object, True)
                        log.debug('Added derpibooru data to embed')
                        embed = embed_object.embed
                        log.debug('Spamming')
                        asyncio.ensure_future(self.bot.send_message(channel, "Image found: <https://derpibooru.org/{}>".format(image_data["id"]), embed=embed))
                    else:
                        actioned = True
                        log.debug('Where\'s imagesafe?! ' + str(derpiAvailable))
                        asyncio.ensure_future(self.bot.send_message(channel, "Image found: https://derpibooru.org/{}".format(image_data["id"])))
                    spammed.append(image_data['id'])
                log.debug('Newest: ' + newest)
                if actioned:
                    log.debug('Wrote newest')
                    self.data['latest image'][server.id][channel.id] = newest
                    fileIO("data/ponysub/data.json", "save", self.data)

def find_sub(self, server, sub_name):
    sub_name = sub_name.lower().capitalize()
    if server.id not in self.data:
        return False
    for a in range(len(self.data[server.id])):
        if self.data[server.id][a]['name'] == sub_name:
            return a
    return False
    
def check_folder():
    if not os.path.exists("data/ponysub"):
        print("Creating data/ponysub folder...")
        os.makedirs("data/ponysub")

def check_files():
    data_default = {"refresh":5}
    if not fileIO("data/ponysub/data.json", "check"):
        print("Creating default ponysub data.json...")
        fileIO("data/ponysub/data.json", "save", data_default)
    else:
        data = fileIO("data/ponysub/data.json", "load")
    if not fileIO("data/ponysub/history.json", "check"):
        print("Creating default ponysub history.json...")
        fileIO("data/ponysub/history.json", "save", {})



def setup(bot):
    check_folder()
    check_files()
    bot.add_cog(Ponysub(bot))
