import discord
import asyncio
from discord.ext import commands
import logging
import re
import os
from .utils.dataIO import fileIO
from datetime import datetime, timedelta
import pyrfc3339 

log = logging.getLogger("red.emojifier")
log.setLevel(logging.ERROR)
 
class Quoteplus:
    def __init__(self, bot):
        self.bot = bot
        self.history = fileIO("data/imagesafe/history.json", "load")
        self.cleanup_history()
        
    @commands.command(name='quotetest',no_pm=True, pass_context=True)
    async def _test(self, ctx):
        """Test emojifier"""
        quotes = re.findall(r'```(.+?)```', ctx.message.clean_content)
        #log.debug("quotes found: " + quotes)
        for quote in quotes:
            log.debug(quote)
        
    async def listener2(self, skub, message):
        await self.listener(message)
    
    async def listener(self, message):
        if message.author.bot:
            log.debug("First abort: " + message.clean_content)
            return
        log.debug("content: " + message.clean_content)
        if '```' in message.clean_content:
            for emoji in self.bot.get_server('316422538592911360').emojis:
                if 'quote' in emoji.name:
                    await self.bot.add_reaction(message, emoji)

    async def reaction(self, reaction,member):
        if member.bot:
            log.debug("First abort")
            return
        if not reaction.custom_emoji:
            if not reaction.emoji == "ðŸš«":
                log.debug("Second abort")
                return
        else:
            log.debug("Second abort pt2")
            return
        if not reaction.message.author.bot:
            log.debug("Third abort")
            return
        message = reaction.message
        channel = message.channel
        server = message.server
        if server.id not in self.history:
            log.debug("Fourth abort")
            return
        if channel.id not in self.history[server.id]:
            log.debug("Fifth abort")
            return
        if message.id not in self.history[server.id][channel.id]:
            log.debug("Sixth abort")
            return
        if not self.history[server.id][channel.id][message.id]:
            log.debug("Seventh abort")
            return
        data = self.history[server.id][channel.id][message.id]
        if not channel.permissions_for(member).manage_messages and member.id is not data['user']:
            log.debug("Eighth abort")
            return
        await self.bot.delete_message(message)
        del self.history[server.id][channel.id][message.id]
        fileIO("data/quoteplus/history.json", "save", self.history)
                
    def cleanup_history(self):
        cleanup_datetime = pyrfc3339.generate(datetime.utcnow() - timedelta(days = 120),accept_naive=True)
        for server_id in list(self.history.keys()):
            server = self.bot.get_server(server_id)
            if server is None:
                del self.history[server_id]
                fileIO("data/imagesafe/history.json", "save", self.history)
                continue
            channel_data = self.history[server.id]
            for channel_id in list(channel_data.keys()):
                message_data = channel_data[channel_id]
                for message_id in list(message_data.keys()):
                    data = message_data[message_id]
                    if data['date'] < cleanup_datetime:
                        del self.history[server_id][channel_id][message_id]
                        fileIO("data/imagesafe/history.json", "save", self.history)
                        continue
                
    async def _reaction(self, reaction, member):
        if member.bot:
            log.debug("First abort")
            return
        if not reaction.custom_emoji:
            log.debug("Second abort")
            return
        message = reaction.message
        channel = message.channel
        server = message.server
        if not message.author.id == member.id:
            log.debug("Third abort")
            return
        log.debug("emoji! " + reaction.emoji.name)
        if reaction.emoji.name.lower() == 'quote':
            #Do quote block
            log.debug("Doing quote thingy")
            quotes = re.findall(r'```(.+?)```', message.clean_content, flags=re.S)
            #log.debug("quotes found: " + quotes)
            for quote in quotes:
                log.debug(quote)
                sent_message = await self.bot.send_message(channel,"```Quote by {}```\n".format(message.author.display_name) + quote)
                if message.server.id not in self.history:
                    self.history[message.server.id] = {}
                if message.channel.id not in self.history[message.server.id]:
                    self.history[message.server.id][message.channel.id] = {}
                self.history[message.server.id][message.channel.id][sent_message.id] = {
                    'user' : message.author.id,
                    'date' : pyrfc3339.generate(datetime.utcnow(),accept_naive=True)
                }
                fileIO("data/quoteplus/history.json", "save", self.history)
                await self.bot.add_reaction(sent_message, "ðŸš«")
        
        
def check_folder():
    if not os.path.exists("data/quoteplus"):
        print("Creating data/quoteplus folder...")
        os.makedirs("data/quoteplus")

def check_files():
    if not fileIO("data/quoteplus/history.json", "check"):
        print("Creating default history.json...")
        fileIO("data/quoteplus/history.json", "save", {})
        
def setup(bot):
    check_folder()
    check_files()
    n = Quoteplus(bot)
    bot.add_listener(n._reaction, 'on_reaction_add')
    bot.add_listener(n.reaction, 'on_reaction_add')
    bot.add_listener(n.listener, 'on_message')
    bot.add_listener(n.listener2, 'on_message_edit')
    bot.add_cog(n)