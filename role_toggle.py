import discord
from discord.ext import commands
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
import os
import asyncio
import logging

log = logging.getLogger("red.role_toggle")
log.setLevel(logging.DEBUG)

class Role_Toggle:
    """Allows users to toggle on/off particular roles set by admins"""

    def __init__(self, bot):
        self.bot = bot
        self.settings = fileIO("data/role_toggle/settings.json", "load")
        for servers in self.bot.servers:
            self.clean_roles(servers)

    def clean_roles(self, server):
        if server.id in self.settings:
            if 'roles' in self.settings[server.id]:
                server_roles = []
                for roles in server.roles:
                    server_roles.append(roles.id)
                for role_id in self.settings[server.id]['roles']:
                    if role_id not in server_roles:
                        self.settings[server.id]['roles'].remove(role_id)
                        fileIO("data/role_toggle/settings.json","save",self.settings)
        
    @commands.group(pass_context=True, no_pm=True)
    @checks.admin_or_permissions(manage_server=True)
    async def toggleset(self, ctx):
        """Sets role_toggle module settings"""
        server = ctx.message.server
        self.clean_roles(server)
        if server.id not in self.settings:
            self.settings[server.id] = {}
            self.settings[server.id]['roles'] = []
            fileIO("data/role_toggle/settings.json","save",self.settings)
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)
            await self.list_roles(server)

    async def list_roles(self, server):
        msg = "```\n"
        msg += "Roles that can be toggled:\n"
        has_roles = False
        for role in server.roles:
            if role.id not in self.settings[server.id]['roles']:
                continue
            msg += role.name + "\n"
            has_roles = True
        if not has_roles:
            msg += "No Roles Found!\n"
        msg += "```"
        await self.bot.say(msg)
            
    @toggleset.command(pass_context=True, no_pm=True)
    async def role(self, ctx, role_name):
        """Sets a role that can be added by users."""
        server = ctx.message.server
        self.clean_roles(server)
        role_id = None
        for role in server.roles:
            if role.name.lower() != role_name.lower():
                continue
            else:
                role_id = role.id
                break
        if role_id is None:
            await self.bot.say("I don't recognize that role name!")
            return
        if role_id not in self.settings[server.id]['roles']:
            self.settings[server.id]['roles'].append(role_id)
            fileIO("data/role_toggle/settings.json","save",self.settings)
            await self.bot.say("Role added.")
        else:
            self.settings[server.id]['roles'].remove(role_id)
            fileIO("data/role_toggle/settings.json","save",self.settings)
            await self.bot.say("Role removed.")
            
    @commands.command(name='role', pass_context=True, no_pm=True)
    async def togglerole(self, ctx, role_name : str = None):
        server = ctx.message.server
        self.clean_roles(server)
        if server.id not in self.settings:
            await self.bot.say("This feature hasn't been configured!")
            return
        if role_name is None or role_name.lower() is 'list':
            await self.list_roles(server)
            return
        role_id = None
        for role in server.roles:
            if role.name.lower() != role_name.lower():
                continue
            else:
                role_id = role.id
                role_name = role.name
                user_role = role
                break
        if role_id is None:
            await self.bot.say("I couldn't find a role named: `{}`".format(role_name))
            return
        if role_id not in self.settings[server.id]['roles']:
            await self.bot.say("I cannot toggle: `{}`".format(role_name))
            return
        if user_role in ctx.message.author.roles:
            try:
                log.debug( "Welcome: Trying to remove role:" + str(role_name) )
                await self.bot.remove_roles(ctx.message.author, user_role)
                await self.bot.say("Removed role: `{}`".format(role_name))
            except discord.Forbidden:
                await self.bot.say("Oh noez! I can't remove that role! Danger! Danger!")
                return
            except discord.HTTPException:
                await self.bot.say("EXPLOSIONS!")
                return
        else:
            try:
                log.debug( "Welcome: Trying to add role:" + str(role_name) )
                await self.bot.add_roles(ctx.message.author, user_role)
                await self.bot.say("Added role: `{}`".format(role_name))
            except discord.Forbidden:
                await self.bot.say("Oh noez! I can't add that role! Danger! Danger!")
                return
            except discord.HTTPException:
                await self.bot.say("EXPLOSIONS!")
                return
    

def check_folders():
    if not os.path.exists("data/role_toggle"):
        print("Creating data/role_toggle folder...")
        os.makedirs("data/role_toggle")

def check_files():
    f = "data/role_toggle/settings.json"
    if not fileIO(f, "check"):
        print("Creating welcome settings.json...")
        fileIO(f, "save", {})

def setup(bot):
    check_folders()
    check_files()
    n = Role_Toggle(bot)
    bot.add_cog(n)

