import discord
import feedparser
from discord.ext import commands
from .utils.dataIO import fileIO
from .utils import checks
from __main__ import send_cmd_help
from urllib import parse
import os
import re
import asyncio
import aiohttp
import logging

try:   # check if feedparser exists
    import feedparser 
except ImportError:
    feedparser = None

log = logging.getLogger("red.youtubesub")
if os.path.exists("data/istest"):
    log.setLevel(logging.DEBUG)
    log.debug('Debug Logging On')
else:
    log.setLevel(logging.INFO)
    log.info('Info Logging On')
    
class YoutubeSub:
    def __init__(self, bot):
        self.bot = bot
        self.data = fileIO("data/youtubesub/data.json", "load")
        self.sub_task = bot.loop.create_task(self.youtubesub_service())

    @commands.group(name='youtubesub',no_pm=True, pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _youtubesub(self, ctx):
        """Manages YoutubeSub"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)
            
    @_youtubesub.command(name='refresh', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)
    async def _refresh(self, ctx, refresh_time : int):        
        """Set the refresh time (how often to check the server in minutes)
        
        [p]youtubesub refresh <time>"""
        server = ctx.message.server
        if refresh_time < 5:
            await self.bot.say("You can only enter a time over 15 minutes.")
            return
        self.data['refresh'] = refresh_time
        await self.bot.say( "Refresh time set to %i minutes." % (refresh_time))
        fileIO("data/youtubesub/data.json", "save", self.data)

    @_youtubesub.command(name='add', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _add(self, ctx, channel : discord.Channel = None, url : str = None):
        """Add a subscription to a youtube channel
        
        [p]youtubesub add #<channel> channel_url"""
        if channel is None or url is None:
            await send_cmd_help(ctx)
            return
        await self.add_channel(channel, url, ctx.channel)
        
        
    async def add_channel(self, channel, url, comms_channel):
        server = channel.server
        youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/(channel|user)/(\S+)', url)
        if not youtube_id:
            return await self.bot.send_message(comms_channel,'Channel not found!')
        log.debug(youtube_id)
        if youtube_id[0][0] == 'user':
            async with  aiohttp.get('https://www.youtube.com/user/{}'.format(youtube_id[0][1])) as response:
                website = await response.text()
            log.debug(website)
            youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/feeds/videos\.xml\?channel_id=(\S+)"', website)
            log.debug(youtube_id)
            if not youtube_id:
                return await self.bot.send_message(comms_channel,'Channel not valid!')
            youtube_id = youtube_id[0]
        else:
            youtube_id = youtube_id[0][1]
        if server.id not in self.data['subs']:
            self.data['subs'][server.id] = {}
        if channel.id not in self.data['subs'][server.id]:
            self.data['subs'][server.id][channel.id] = {}
        if youtube_id in self.data['subs'][server.id][channel.id]:
            return await self.bot.send_message(comms_channel,'That channel is already subscribed!')
        youtube_data = self.get_channel(youtube_id)
        if youtube_data is None:
            return await self.bot.send_message(comms_channel,'That channel doesn\'t exist!')
        self.data['subs'][server.id][channel.id][youtube_id] = {
            'latest' : youtube_data['latest'],
            'prefix' : 'Youtube Bot: {}',
            'whitelist' : None,
            'blacklist' : None,
            'spoiler' : {
                'filter' : None,
                'prefix' : 'Youtube Bot: {}'
            }
        }
        fileIO("data/youtubesub/data.json", "save", self.data)
        await self.bot.send_message(comms_channel,"Added <https://www.youtube.com/channel/{youtube_id}> to {channel}. Latest video found: {latest}".format(youtube_id = youtube_id, channel = channel.mention, latest = youtube_data['latest']))
        
    @_youtubesub.command(name='del', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _del(self, ctx, channel : discord.Channel = None, url : str = None):
        """Del a subscription
        
        [p]youtubesub del #<channel> channel_url"""
        if channel is None or url is None:
            await send_cmd_help(ctx)
            return
        await self.del_channel(channel, url, ctx.channel)
            
            
    async def del_channel(self, channel, url, comms_channel):
        server = channel.server
        youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/(channel|user)/(\S+)', url)
        if not youtube_id:
            return await self.bot.send_message(comms_channel,'Channel not found!')
        if youtube_id[0][0] == 'user':
            async with  aiohttp.get('https://www.youtube.com/user/{}'.format(youtube_id[0][1])) as response:
                website = await response.text()
            youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/feeds/videos\.xml\?channel_id=(\S+)"', website)
            if not youtube_id:
                return await self.bot.send_message(comms_channel,'Channel not valid!')
            youtube_id = youtube_id[0]
        else:
            youtube_id = youtube_id[0][1]
        log.debug( youtube_id)
        if server.id not in self.data['subs']:
            return await self.bot.send_message(comms_channel,'No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.send_message(comms_channel,'No subs on this server!')
        if youtube_id not in self.data['subs'][server.id][channel.id]:
            return await self.bot.send_message(comms_channel,'That channel is not subscribed!')
        del self.data['subs'][server.id][channel.id][youtube_id]
        fileIO("data/youtubesub/data.json", "save", self.data)
        await self.bot.send_message(comms_channel,"Removed <https://www.youtube.com/channel/{youtube_id}> from {channel}.".format(youtube_id = youtube_id, channel = channel.mention))
        
    @_youtubesub.command(name='set_prefix', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _set_prefix(self, ctx, channel : discord.Channel = None, url : str = None):
        """Set a prefix for spam
        
        [p]youtubesub set_prefix #<channel> channel_url"""
        server = ctx.message.server
        if channel is None or url is None:
            await send_cmd_help(ctx)
            return
        await self.bot.send_message(ctx.message.channel, "Please enter the prefix you wish to use, with {} where you want the URL to be (\"None\"  to reset, or let timeout to abort)")
        message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
        if not message:
            return await self.bot.send_message(ctx.message.author,"Aborted.")
        prefix = message.content
        if 'None' in prefix:
            prefix = None
        if prefix is not None and '{}' not in prefix:
            return await self.bot.say('You need to put {} in the prefix where you want the URL to be.')
        youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/(channel|user)/(\S+)', url)
        if not youtube_id:
            return await self.bot.say('Channel not found!')
        if youtube_id[0][0] == 'user':
            async with  aiohttp.get('https://www.youtube.com/user/{}'.format(youtube_id[0][1])) as response:
                website = await response.text()
            youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/feeds/videos\.xml\?channel_id=(\S+)"', website)
            if not youtube_id:
                return await self.bot.say('Channel not valid!')
            youtube_id = youtube_id[0]
        else:
            youtube_id = youtube_id[0][1]
        if server.id not in self.data['subs']:
            return await self.bot.say('No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.say('No subs on this server!')
        if youtube_id not in self.data['subs'][server.id][channel.id]:
            return await self.bot.say('That channel is not subscribed!')
        if prefix is None:
            self.data['subs'][server.id][channel.id][youtube_id]['prefix'] = 'Youtube Bot: {}'
        else:
            self.data['subs'][server.id][channel.id][youtube_id]['prefix'] = prefix
        fileIO("data/youtubesub/data.json", "save", self.data)
        await self.bot.say("Done.")
        
    @_youtubesub.command(name='set_whitelist', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _set_whitelist(self, ctx, channel : discord.Channel = None, url : str = None):
        """Set a whitelist for spam (regular expression)
        
        If whitelist is missing, it is reset.
        
        [p]youtubesub set_whitelist #<channel> channel_url"""
        server = ctx.message.server
        if channel is None or url is None:
            await send_cmd_help(ctx)
            return
        youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/(channel|user)/(\S+)', url)
        if not youtube_id:
            return await self.bot.say('Channel not found!')
        if youtube_id[0][0] == 'user':
            async with  aiohttp.get('https://www.youtube.com/user/{}'.format(youtube_id[0][1])) as response:
                website = await response.text()
            youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/feeds/videos\.xml\?channel_id=(\S+)"', website)
            if not youtube_id:
                return await self.bot.say('Channel not valid!')
            youtube_id = youtube_id[0]
        else:
            youtube_id = youtube_id[0][1]
        if server.id not in self.data['subs']:
            return await self.bot.say('No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.say('No subs on this server!')
        if youtube_id not in self.data['subs'][server.id][channel.id]:
            return await self.bot.say('That channel is not subscribed!')
        await self.bot.send_message(ctx.message.channel, "Please enter the whitelist you wish to use (\"None\"  to reset, or let timeout to abort)")
        message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
        if not message:
            return await self.bot.send_message(ctx.message.author,"Aborted.")
        whitelist = message.content
        if 'None' in whitelist:
            whitelist = None
        if whitelist is None:
            self.data['subs'][server.id][channel.id][youtube_id]['whitelist'] = None
        else:
            try:
                re.match(whitelist, url)
            except:
                return await self.bot.say('Error in regular expression!')
            self.data['subs'][server.id][channel.id][youtube_id]['whitelist'] = whitelist
        fileIO("data/youtubesub/data.json", "save", self.data)
        await self.bot.say("Done.")

    @_youtubesub.command(name='set_blacklist', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def _set_blacklist(self, ctx, channel : discord.Channel = None, url : str = None):
        """Set a blacklist for spam (regular expression)
                
        [p]youtubesub set_blacklist #<channel> channel_url"""
        server = ctx.message.server
        if channel is None or url is None:
            await send_cmd_help(ctx)
            return
        youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/(channel|user)/(\S+)', url)
        if not youtube_id:
            return await self.bot.say('Channel not found!')
        if youtube_id[0][0] == 'user':
            async with  aiohttp.get('https://www.youtube.com/user/{}'.format(youtube_id[0][1])) as response:
                website = await response.text()
            youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/feeds/videos\.xml\?channel_id=(\S+)"', website)
            if not youtube_id:
                return await self.bot.say('Channel not valid!')
            youtube_id = youtube_id[0]
        else:
            youtube_id = youtube_id[0][1]
        if server.id not in self.data['subs']:
            return await self.bot.say('No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.say('No subs on this server!')
        if youtube_id not in self.data['subs'][server.id][channel.id]:
            return await self.bot.say('That channel is not subscribed!')
        await self.bot.send_message(ctx.message.channel, "Please enter the blacklist you wish to use (\"None\"  to reset, or let timeout to abort)")
        message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
        if not message:
            return await self.bot.send_message(ctx.message.author,"Aborted.")
        blacklist = message.content
        if 'None' in blacklist:
            blacklist = None
        if blacklist is None:
            self.data['subs'][server.id][channel.id][youtube_id]['blacklist'] = None
        else:
            try:
                re.match(blacklist, url)
            except:
                return await self.bot.say('Error in regular expression!')
            self.data['subs'][server.id][channel.id][youtube_id]['blacklist'] = blacklist
        fileIO("data/youtubesub/data.json", "save", self.data)
        await self.bot.say("Done.")        
        
    @_youtubesub.command(name='set_spoiler', pass_context=True)
    @checks.admin_or_permissions(manage_server=True)        
    async def set_spoiler(self, ctx, channel : discord.Channel = None, url : str = None):
        """Set a spoiler for spam (regular expression)
                
        [p]youtubesub set_spoiler #<channel> channel_url"""
        server = ctx.message.server
        if channel is None or url is None:
            await send_cmd_help(ctx)
            return
        youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/(channel|user)/(\S+)', url)
        if not youtube_id:
            return await self.bot.say('Channel not found!')
        if youtube_id[0][0] == 'user':
            async with  aiohttp.get('https://www.youtube.com/user/{}'.format(youtube_id[0][1])) as response:
                website = await response.text()
            youtube_id = re.findall(r'https?://(?:www\.)?youtube\.com/feeds/videos\.xml\?channel_id=(\S+)"', website)
            if not youtube_id:
                return await self.bot.say('Channel not valid!')
            youtube_id = youtube_id[0]
        else:
            youtube_id = youtube_id[0][1]
        if server.id not in self.data['subs']:
            return await self.bot.say('No subs on record!')
        if channel.id not in self.data['subs'][server.id]:
            return await self.bot.say('No subs on this server!')
        if youtube_id not in self.data['subs'][server.id][channel.id]:
            return await self.bot.say('That channel is not subscribed!')
        await self.bot.send_message(ctx.message.channel, "Please enter the spoiler you wish to use (\"None\"  to reset, or let timeout to abort)")
        message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
        if not message:
            return await self.bot.send_message(ctx.message.author,"Aborted.")
        spoiler = message.content
        if 'None' in spoiler:
            spoiler = None
        await self.bot.send_message(ctx.message.channel, "Please enter the prefix you wish to use, with {} where you want the URL to be (\"None\"  to reset, or let timeout to abort)")
        message = await self.bot.wait_for_message(timeout=60, author=ctx.message.author)
        if not message:
            return await self.bot.send_message(ctx.message.author,"Aborted.")
        prefix = message.content
        if 'None' in prefix:
            prefix = None
        if spoiler is None:
            self.data['subs'][server.id][channel.id][youtube_id]['spoiler'] = { 'filter' : None, 'prefix' : 'Youtube Bot: {}' }
        else:
            try:
                re.match(spoiler, url)
            except:
                return await self.bot.say('Error in regular expression!')
            self.data['subs'][server.id][channel.id][youtube_id]['spoiler'] = { 'filter' : spoiler, 'prefix' : prefix }
        fileIO("data/youtubesub/data.json", "save", self.data)
        await self.bot.say("Done.")      
        
    def get_channel(self, youtube_id : str = None, values : dict = None):
        if youtube_id is None:
            raise
        feed = feedparser.parse( 'https://www.youtube.com/feeds/videos.xml?channel_id={}'.format(youtube_id) )
        if not feed:
            log.debug('No feed found!')
            return None
        if 'entries' not in feed:
            log.debug('No entries found!')
            return None
        log.debug(feed)
        if values is not None:
            log.debug(values)
        feed_data = {}
        latest_time = ''
        for data in feed['entries']:
            log.debug(data)
            if values is not None:
                if data['published'] <= values['latest']:
                    log.debug(data['published'] + ' is older than ' + values['latest'])
                    continue
                if values['whitelist'] is not None:
                    if re.search(values['whitelist'], data['summary']) is None and re.search(values['whitelist'], data['title']) is None:
                        log.debug('Whitelist regex not matched')
                        continue
                if values['blacklist'] is not None:
                    if re.search(values['blacklist'], data['summary']) is not None or re.search(values['blacklist'], data['title']) is not None:
                        log.debug('Blacklist regex matched')
                        continue
                if values['spoiler']['filter'] is not None:
                    if re.search(values['spoiler']['filter'], data['summary']) is not None or re.search(values['spoiler']['filter'], data['title']) is not None:
                        log.debug('Spoiler regex matched')
                        feed_data[values['spoiler']['prefix'].replace('{}', '<{}>').format(data['link'])] = data
                        if data['published'] > latest_time:
                            latest_time = data['published']
                        continue
                feed_data[values['prefix'].format(data['link'])] = data['published']
            else:
                feed_data['Youtube Bot: {}'.format(data['link'])] = data['published']
            if data['published'] > latest_time:
                latest_time = data['published']
        return { 'latest' : latest_time, 'feed' : feed_data }

    @_youtubesub.command(name='test', pass_context=True)
    async def _test(self, ctx):
        self.data = fileIO("data/youtubesub/data.json", "load")
        await self.spam_looper()        
        
    async def youtubesub_service(self):
        """Trigger loop"""
        if os.path.exists("data/istest"):
            return #comment out when not testing
        await self.bot.wait_until_ready()
        try:
            log.debug( "Entering try block, starting loop." )
            while True:
                try:
                    await self.spam_looper()
                except:
                    e = sys.exc_info()[0]
                    log.error( "Error: %s" % e )
                    
                    log.error( sys.exc_info())
                    log.exception(e)
                log.info( "Youtubesub: Task done, sleeping." )
                if type(self.data['refresh']) is int:
                    if self.data['refresh'] < 5:
                        self.data['refresh'] = 5
                        fileIO("data/youtubesub/data.json", "save", self.data)
                else:
                    self.data['refresh'] = 5
                    fileIO("data/youtubesub/data.json", "save", self.data)
                await asyncio.sleep(self.data['refresh'] * 60)
                log.info("Youtubesub service tick.")
        except asyncio.CancelledError:
            pass
        
    def __unload(self):
        self.sub_task.cancel()
        pass
        
    async def spam_looper(self):
        if not self.data:
            log.debug( "Program data empty? Bail out!" )
            return None
        if 'subs' not in self.data:
            log.debug( "Program data (almost) empty? Bail out!" )
            return None
        for server in self.bot.servers:
            if server.id not in self.data['subs'].keys():
                log.debug("Server without data: " + server.name + ' : ' + server.id)
                continue
            log.debug('Found server: ' + server.name)
            for channel, data in self.data['subs'][server.id].items():
                log.debug('Channel: ' + str(channel))
                channel = self.bot.get_channel(channel)
                if channel is None:
                    log.debug('Not found!')
                    continue
                log.debug('Found!')
                log.debug( "Looking for a channel to spam, found: {}".format(channel.name) )
                for youtube_id, values in data.items():
                    feed = self.get_channel(youtube_id, values)
                    log.debug(feed)
                    if feed is None:
                        log.debug('Channel misconfigured!')
                        continue
                    if not feed['feed']:
                        log.debug('Nothing new!')
                        continue
                    for datum in feed['feed'].keys():
                        asyncio.ensure_future(self.bot.send_message( channel, datum))
                    self.data['subs'][server.id][channel.id][youtube_id]['latest'] = feed['latest']
                    fileIO("data/youtubesub/data.json", "save", self.data)
                    
    async def reaction(self, reaction, member):
        try:
            if member.bot:
                log.debug("First abort")
                return
            if reaction.custom_emoji:
                log.debug("Second abort")
                return
    #        if not reaction.message.author.bot:
    #            log.debug("Third abort")
    #            return
            message = reaction.message
            channel = message.channel
            server = message.server
            log.debug(reaction.emoji)
            looper = True
            if not checks.admin_or_permissions(manage_messages=True):
                return
            while looper:
                looper = False
                url = re.search(r'https?://(?:www\.)?youtube\.com/(?:channel|user)/\S+',message.clean_content)
                if url is None:
                    return
                else:
                    url = url.group(0)
                if reaction.emoji == "ℹ": #Info emoji
                    await self.bot.add_reaction(message, "➕")
                    await self.bot.add_reaction(message, "➖")
                    break
                if reaction.emoji == "➕": #Plus
                    await self.add_channel(channel, url, channel)
                    break
                if reaction.emoji == "➖": #Minus
                    await self.del_channel(channel, url, channel)
                    break
                break
        except:
            e = sys.exc_info()[0]
            log.error( "Error: %s" % e )
            log.error( sys.exc_info())
                     
    async def listener(self,message):
        if message.server is None:
            return
        if message.author.bot:
            return
        if re.search(r'https?://(?:www\.)?youtube\.com/(?:channel|user)/\S+',message.clean_content) is None:
            return
        if isinstance(message.channel,discord.Channel):
            log.debug('Adding emoji!')
            await self.bot.add_reaction(message, "ℹ")
            return
                        
def check_folder():
    if not os.path.exists("data/youtubesub"):
        print("Creating data/youtubesub folder...")
        os.makedirs("data/youtubesub")

def check_files():
    data_default = {"refresh":15, 'subs' : {}}
    if not fileIO("data/youtubesub/data.json", "check") or not fileIO("data/youtubesub/data.json", "load"):
        print("Creating default youtubesub data.json...")
        fileIO("data/youtubesub/data.json", "save", data_default)
    else:
        data = fileIO("data/youtubesub/data.json", "load")

def setup(bot):
    check_folder()
    check_files()
    if feedparser is None:
        raise RuntimeError("You need to run `pip install feedparser`")
    n = YoutubeSub(bot)
    bot.add_listener(n.listener, 'on_message')
    bot.add_listener(n.reaction, 'on_reaction_add')
    bot.add_cog(n)